<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\auth
 */
namespace GH\auth;

use GH\DB;

function user()
{
	$query =
		'select
			usr.id,
			usr.nombreusuario as username,
			usr.nombre as name,
			usr.apellido_paterno as lastname,
			usr.apellido_materno as surname,
			upper(concat_ws(" ", usr.nombre, usr.apellido_paterno, usr.apellido_materno)) as fullname
		from usuario as usr
		where usr.id = %i';

	$_COOKIE['uid'] = 2;

	if (is_null($_COOKIE['uid']))
		\GH\halt(401, array(array
		(
			'id'			=> 1102
		)));

	$user = DB::$gen->queryFirstRow($query, $_COOKIE['uid']);
	if (is_null($user))
		\GH\halt(401, array(array
		(
			'id'			=> 1103
		)));

	return $user;
}

function permissionMap(&$rowList)
{
	$permissionMap = array();

	for ($i = 0, $size = count($rowList); $size > $i; ++$i)
		$permissionMap[$rowList[$i]['permissionName']] = true;

	return $permissionMap;
}

/**
 * @param mixed $nodeMap
 * @param mixed $permissionMap
 * @return mixed
 */
function filterNodeMap(&$nodeMap, &$permissionMap)
{
	if (array_key_exists('*', $permissionMap))
		return;

	foreach ($nodeMap as $node => $allowed)
	{
		if (!array_key_exists($node, $permissionMap))
			unset($nodeMap[$node]);
		else if (array_key_exists('actionMap', $nodeMap[$node]))
		{
			foreach ($nodeMap[$node]['actionMap'] as $action => $config)
			{
				if (!array_key_exists("$node:$action", $permissionMap))
					unset($nodeMap[$node]['actionMap'][$action]);
			}
		}
	}

	return $nodeMap;
}

/**
 * @api
 */
function profile($uId)
{
	$query =
		'select
			pms.name as permissionName
		from gh_user_role urr
		inner join gh_role_permission as rpm
			on urr.role_id = rpm.role_id
		inner join gh_permission as pms
			on rpm.permission_id = pms.id
		where urr.user_id = %i';

	$rowList = DB::$gen->query($query, $uId);
	$bounds = permissionMap($rowList);

	if (0 == count($bounds))
		\GH\halt(401, array(array
		(
			'id'			=> 1101
		)));

	$query =
		'select
			usr.id,
			usr.nombreusuario as username,
			usr.nombre as name,
			concat_ws(" ", usr.apellido_paterno, usr.apellido_materno) as lastname
		from usuario as usr
		where usr.id = %i';

	$user = DB::$gen->queryFirstRow($query, $uId);

	$profile = array
	(
		'base.prefix.url'	=> '/gestionhospitales',
		'base.service.url'	=> '/gestionhospitales/service',
		'base.client.url'	=> '/gestionhospitales/gh',
		'base.system.list'	=> array
		(
			array
			(
				'name'	=> 'exequiel',
				'title'	=> 'Hospital de Niños Exequiel González Cortés',
				'logo'	=> 'logo-exequiel.png'
			)
		),
		'user' => $user,
		'bounds' => $bounds,
		'template.pool.url'	=> 'gh/assets',
		'node.map'			=> array
		(
			'home' => array
			(
				'title'			=> 'Inicio'
			),
			'home.menu' => array
			(
				'title'			=> 'Menú Inicio',
				'mainMenu'		=> array
				(
					'surgicalAct'
				)
			),
			'operativeProtocol' => array
			(
				'title'			=> 'Protocolo',
				'redirectTo'	=> 'operativeProtocol.pendings'
			),
			'operativeProtocol.cancel' => array
			(
				'title'			=> 'Cancelar Protocolo'
			),
			'operativeProtocol.check' => array
			(
				'title'			=> 'Verificación de Protocolo',
				'actionMap'		=> array
				(
					'validate'		=> array
					(
						'title'		=> 'Validar'
					),
					'viewPatient'	=> array
					(
						'title'		=> 'Ver Paciente'
					)
				),
				'relatedMenu'	=> array
				(
					':viewPatient',
					':validate'
				)
			),
			'operativeProtocol.fill' => array
			(
				'title'			=> 'Editar Protocolo Operatorio',
				'actionMap'		=> array
				(
					'viewPatient' => array
					(
						'title'		=> 'Ver Paciente'
					)
				),
				'relatedMenu'	=> array
				(
					':viewPatient'
				)
			),
			'operativeProtocol.issued' => array
			(
				'title'			=> 'Protocolos Emitidos',
				'actionMap'		=> array
				(
					'cancel'	=> array
					(
						'title'	=> 'Cancelar'
					),
					'check'	=> array
					(
						'title'	=> 'Validar'
					),
					'view'	=> array
					(
						'title'	=> 'Ver'
					)
				)
			),
			'operativeProtocol.pendings' => array
			(
				'title'			=> 'Protocolos Pendientes',
				'actionMap'		=> array
				(
					'cancel'	=> array
					(
						'title'	=> 'Cancelar protocolo'
					),
					'check'	=> array
					(
						'title'	=> 'Validar protocolo'
					),
					'fill'		=> array
					(
						'title'	=> 'Completar protocolo'
					),
					'suspend'	=> array
					(
						'title'	=> 'Suspender orden'
					)
				)
			),
			'operativeProtocol.reports' => array
			(
				'title'			=> 'Reportes'
			),
			'operativeProtocol.view' => array
			(
				'title'			=> 'Vista de Protocolo Operatorio',
				'actionMap'		=> array
				(
					'viewPatient'	=> array
					(
						'title'		=> 'Ver Paciente'
					),
					'print'			=> array
					(
						'title'		=> 'Imprimir'
					)
				),
				'relatedMenu'	=> array
				(
					':viewPatient',
					':print'
				)
			),
			'surgicalAct' => array
			(
				'title'			=> 'Acto Quirúrgico',
				'redirectTo'	=> 'operativeProtocol.pendings',
				'mainMenu'		=> array
				(
					'home',
					'operativeProtocol.pendings',
					'operativeProtocol.issued',
					'operativeProtocol.reports'
				)
			),
			'surgicalOrder' => array
			(
				'title'			=> 'Orden Quirúrgica'
			),
			'surgicalOrder.suspend' => array
			(
				'title'			=> 'Suspender Orden Quirúrgica'
			)
		)
	);
	filterNodeMap($profile['node.map'], $bounds);

	\GH\respond($profile);
}
