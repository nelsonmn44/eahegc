<?php
/**
 * Servicios para gestion-hospitales
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH
 */
namespace GH\auth;

header('Access-Control-Allow-Origin: *');
require_once(__DIR__ . '/../config.php');
require_once('app.php');

\GH\dbConnect();

$ghApp = \GH\getNewApp();
\Slim\Route::setDefaultConditions(array(
	'opId'		=> '[0-9]+',
	'soId'		=> '[0-9]+'
));

require_once('auth.php');

$ghApp->get(
	'/:uId/profile',
	'\GH\auth\profile'
);

$ghApp->run();
