<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		-
 */
namespace GH\util;

/**
 * @param mixed ...
 * @return string
 */
function concatWs()
{
	$result = '';

	$argList = func_get_args();
	for ($i = 0, $size = count($argList); $size > $i; ++$i)
	{
		if (!empty($argList[$i]))
			$result .= (0 < $i ? ', ' : '') . $argList[$i];
	}

	return $result;
}

/**
 * @param array $map
 * @param string $key
 * @return boolean
 */
function isVoidKey($map, $key)
{
	return
		empty($map) ||
		empty($key) ||
		!array_key_exists($key, $map) ||
		is_null($map[$key]);
}

/**
 * @param mixed $value
 * @return mixed
 */
function nonEmpty($value)
{
	return !empty($value) ? $value : '-';
}

/**
 * @param string $scaped
 * @return string
 */
function normalize($scaped)
{
	return preg_replace_callback(
		'/\\\\u([0-9a-f]{4})/i',
		function ($matches)
		{
			return mb_convert_encoding(
				pack('H*', $matches[1]),
				'UTF-8',
				'UTF-16'
			);
		},
		str_replace('\\/', '/', $scaped)
	);
}

/**
 * @param integer $minutes
 * @return string
 */
function numberToTime($minutes)
{
	$hour = floor($minutes / 60);
	$minutes = $minutes % 60;

	return "$hour:$minutes";
}
