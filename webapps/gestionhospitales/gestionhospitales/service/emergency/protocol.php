<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\operativeProtocol
 */
namespace GH\operativeProtocol;

require_once('auth/auth.php');
require_once('diagnosis.php');
require_once('form.php');
require_once('info.php');
require_once('patient.php');
require_once('surgical_order.php');
require_once('util.php');
