<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\emergency\data
 */
namespace GH\emergency\data;

/**
 * @return array.<array.<string, string|number>>
 */
function category()
{
	return array
	(
		array
		(
			'title' => 'C1',
			'value' => 1
		),
		array
		(
			'title' => 'C2',
			'value' => 2
		),
		array
		(
			'title' => 'C3',
			'value' => 3
		),
		array
		(
			'title' => 'C4',
			'value' => 4
		),
		array
		(
			'title' => 'C5',
			'value' => 5
		),
		array
		(
			'title' => '<descategorizar>',
			'value' => 100
		)
	);
}
