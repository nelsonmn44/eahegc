<?php
/**
 * Servicios para gestion-hospitales
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\emergency
 */
namespace GH\emergency;

header('Access-Control-Allow-Origin: *');
require_once(__DIR__ . '/../config.php');
require_once('app.php');

\GH\dbConnect();

$ghApp = \GH\getNewApp();
\Slim\Route::setDefaultConditions(array(
	'id'	=> '[0-9]+'
));

require_once('dau.php');
$ghApp->get(
	'/daus/:id/categorize',
	'\GH\emergency\dau\categorizer'
);

$ghApp->post(
	'/daus/:id/categorize',
	'\GH\emergency\dau\categorize'
);

$ghApp->get(
	'/daus/triage_queue',
	'\GH\emergency\dau\triageQueue'
);

$ghApp->post(
	'/daus/triage_queue/clean',
	'\GH\emergency\dau\cleanTriageQueue'
);

$ghApp->run();
