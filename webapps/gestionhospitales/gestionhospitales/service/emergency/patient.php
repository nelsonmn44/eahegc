<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\emergency\patient
 */
namespace GH\emergency\patient;

use GH\DB;

/**
 * @param integer $id
 * @return array.<string, string|number>|null
 */
function basicData($id)
{
	$query =
		'select
			pat.paciente_id as id,
			pat.rut as nic,
			upper(concat_ws(" ", pat.nombre, pat.apellido_paterno, pat.apellido_materno)) as fullName,
			pat.fecha_nacimiento as birthdate,
			pat.sexo as gender,
			fz_age(pat.fecha_nacimiento) as age
		from paciente as pat
		where pat.paciente_id = %i';

	return DB::$emy->queryFirstRow($query, $id);
}
