<?php
/**
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\emergency\messages
 */
namespace GH\emergency\messages;

abstract class Error
{
	static $DAU_ALREADY_CATEGORIZED		= array
	(
		'id'			=> 2031,
		'description'	=> 'El DAU solicitado ya ha sido categorizado'
	);
	static $DAU_INVALID_CATEGORY		= array
	(
		'id'			=> 2032,
		'description'	=> 'El ID de categoría debe ser válido'
	);
	static $DAU_INVALID_TRIAGE_USER		= array
	(
		'id'			=> 2033,
		'description'	=> 'El ID del usuario triage debe ser válido'
	);
	static $DAU_NOT_FOUND				= array
	(
		'id'			=> 2034,
		'description'	=> 'El DAU solicitado no existe'
	);
	static $DAU_STATS_DATE_NOT_FOUND	= array
	(
		'id'			=> 2035,
		'description'	=> 'Necesita proporcionar la fecha de inicio para obtener las estadísticas DAU'
	);
	static $DAU_STATS_EMPTY				= array
	(
		'id'			=> 2036,
		'description'	=> 'No existen registros DAU para el rango de fechas seleccionado'
	);
	static $TRIAGE_VOID_CLEAN_LIST		= array
	(
		'id'			=> 2037,
		'description'	=> 'La lista de limpieza Triage no debe ser vacía'
	);
}
