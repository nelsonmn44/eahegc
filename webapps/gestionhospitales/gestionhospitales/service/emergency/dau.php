<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\emergency\dau
 */
namespace GH\emergency\dau;

require_once('data.php');
require_once('messages.php');
require_once('patient.php');
require_once('util.php');

use GH;
use GH\DB;
use GH\emergency\data;
use GH\emergency\messages\Error;
use GH\emergency\patient;
use GH\util;

/**
 * @api
 */
function categorizer($id)
{
	$query =
		'select
			dau.paciente_id as patientId,
			dau.admision_fecha as incomeDate,
			spy.nombre as speciality,
			coalesce(nullif(dau.triage_cat_tecnico, ""), dau.triage_cat_sistema) as category,
			tch.id as technicianId,
			upper(concat_ws(" ", trim(tch.nombre), trim(tch.apellido_paterno), trim(tch.apellido_materno))) as technicianName
		from dau
		inner join especialidad as spy
			on dau.especialidad_id = spy.especialidad_id
		left join usuario as tch
			on dau.triage_user_id = tch.id
		where dau.dau_id = %i';

	$dau = DB::$emy->queryFirstRow($query, $id);
	if (is_null($dau))
		GH\halt(409, Error::$DAU_NOT_FOUND);

	$form = array
	(
		'data'	=> array
		(
			'category'	=> data\category()
		),
		'fields'	=> array
		(
			'categoryId'	=> array
			(
				'data'		=> 'category',
				'required'	=> true
			),
			'triageUserId'	=> array
			(
				'required'	=> true
			)
		),
		'model'	=> array
		(
			'categoryId'	=> (isset($dau['category']) ? substr($dau['category'], 1) : NULL),
			'triageUserId'	=> NULL
		)
	);

	$info = array
	(
		'patient'	=> patient\basicData($dau['patientId']),
		'dau'		=> array
		(
			'id'				=> $id,
			'incomeDate'		=> $dau['incomeDate'],
			'specialityName'	=> $dau['speciality'],
			'technicianName'	=> $dau['technicianName']
		)
	);

	$output = array
	(
		'info'	=> $info,
		'form'	=> $form
	);

	GH\respond($output);
}

/**
 * @api
 */
function categorize($id)
{
	$query	=
		'select
			coalesce(nullif(dau.triage_cat_tecnico, ""), dau.triage_cat_sistema) as category
		from dau
		where dau.dau_id = %i';
	$dau	= DB::$emy->queryFirstRow($query, $id);
	$input	= GH\receive();

	if (is_null($dau))
		GH\halt(409, Error::$DAU_NOT_FOUND);
	else if (util\isVoidKey($input, 'triageUserId'))
		GH\halt(409, Error::$DAU_INVALID_TRIAGE_USER);
	else if (
		util\isVoidKey($input, 'categoryId') ||
		1	> $input['categoryId'] ||
		5	< $input['categoryId'] &&
		100	!= $input['categoryId']
	)
		GH\halt(409, Error::$DAU_INVALID_CATEGORY);

	if (100 != $input['categoryId'])
		DB::$emy->update('dau', array
			(
				'triage_cat_tecnico'	=> 'C' . $input['categoryId'],
				'triage_user_id'		=> DB::$emy->sqleval('case
					when triage_fecha is null then %i
					else triage_user_id
				end', $input['triageUserId']),
				'dauestado_id'			=> DB::$emy->sqleval('case
					when triage_fecha is null then 8
					else dauestado_id
				end'),
				'triage_fecha'			=> DB::$emy->sqleval('case
					when triage_fecha is null then now()
					else triage_fecha
				end')
			), 'dau_id = %i', $id
		);
	else
		DB::$emy->update('dau', array
			(
				'triage_cat_tecnico'	=> NULL,
				'triage_user_id'		=> NULL,
				'dauestado_id'			=> 0,
				'triage_fecha'			=> NULL,
			), 'dau_id = %i', $id
		);
}

/**
 * @param string $beginDate
 * @param string $endDate
 */
function categoryStats($beginDate, $endDate)
{
	$query =
		'select
			dau.dau_id as "DAU",
			dau.triage_fecha as "FECHA TRIAGE",
			spc.nombre as "ESPECIALIDAD",
			dau.triage_cat_sistema as "CATEGORÍA SISTEMA",
			dau.triage_cat_tecnico as "CATEGORÍA TÉCNICO",
			coalesce(nullif(dau.triage_cat_tecnico, ""), dau.triage_cat_sistema) as "CATEGORÍA ASUMIDA",
			upper(concat_ws(" ", trim(tch.nombre), trim(tch.apellido_paterno), trim(tch.apellido_materno))) as "NOMBRE TÉCNICO"
		from dau
		left join usuario as tch
			on dau.triage_user_id = tch.id
		inner join especialidad as spc
			on dau.especialidad_id = spc.especialidad_id
		where dau.triage_fecha is not null
			and dau.triage_fecha between %s and %s
		order by dau.triage_fecha desc';

	return DB::$emy->query($query, $beginDate, $endDate);
}

/**
 * @param string $beginDate
 * @param string $endDate
 */
function fullStats($beginDate, $endDate)
{
	$query =
		'select
			dau.dau_id as "DAU",
			pat.rut as "RUT",
			upper(concat_ws(" ", trim(pat.nombre), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as "NOMBRE PACIENTE",
			case
				when var.age <= 4 then "[0-4]"
				when var.age >= 5 and var.age <= 9 then "[5-9]"
				when var.age >= 10 and var.age <= 14 then "[10-14]"
				when var.age >= 15 and var.age <= 19 then "[15-19]"
				when var.age >= 20 and var.age <= 24 then "[20-24]"
				when var.age >= 25 and var.age <= 29 then "[25-29]"
			end as "RANGO ETARIO",
			dau.triage_fecha as "FECHA TRIAGE",
			dau.atencion_fecha as "FECHA ATENCIÓN",
			coalesce(nullif(dau.triage_cat_tecnico, ""), dau.triage_cat_sistema) as "CATEGORIA",
			dst.descripcion as "ESTADO"
		from dau
		inner join paciente as pat
			on dau.paciente_id = pat.paciente_id
		inner join (
			select
				dau.dau_id,
				floor(datediff(dau.admision_fecha, pat.fecha_nacimiento) / 365) as age
			from dau
			inner join paciente as pat
				on dau.paciente_id = pat.paciente_id
		) as var
			on dau.dau_id = var.dau_id
		inner join dauestado as dst
			on dau.dauestado_id = dst.dauestado_id
		where dau.admision_fecha between %s and %s';

	return DB::$emy->query($query, $beginDate, $endDate);
}

/**
 * @api
 */
function triageQueue()
{
	$query =
		'select
			dau.dau_id as dauId,
			dau.dauestado_id as statusId,
			sts.descripcion as statusName,
			dau.especialidad_id as specialityId,
			spy.nombre as specialityName,
			dau.admision_fecha as incomeDate,
			var.elapsed as elapsedMinutes,
			case
				when 30 < var.elapsed then 2
				when 15 < var.elapsed then 1
				else 0
			end as waitStatusId,
			pat.paciente_id as patientId,
			upper(concat_ws(" ", trim(pat.nombre), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as patientName
		from dau
		inner join especialidad as spy
			on dau.especialidad_id = spy.especialidad_id
		inner join dauestado as sts
			on dau.dauestado_id = sts.dauestado_id
			and dau.dauestado_id in (0, 4, 5)
		inner join paciente as pat
			on dau.paciente_id = pat.paciente_id
		inner join (
			select
				dau.dau_id,
				timestampdiff(minute, dau.admision_fecha, now()) as elapsed
			from dau
		) as var
			on dau.dau_id = var.dau_id
			and 181 > var.elapsed
		where dau.triage_fecha is null
		order by dau.admision_fecha desc';

	$rowList = DB::$emy->query($query);

	$output = array
	(
		'elements'	=> $rowList,
		'pageSize'	=> count($rowList),
		'offset'	=> 0,
		'size'		=> count($rowList)
	);

	GH\respond($output);
}

function cleanTriageQueue()
{
	$input	= GH\receive();

	if (
		util\isVoidKey($input, 'elements') ||
		1 > count($input['elements'])
	)
		GH\halt(404, Error::$TRIAGE_VOID_CLEAN_LIST);

	DB::$emy->update('dau', array
		(
			'dauestado_id' => 6
		), 'dau_id in %li', $input['elements']
	);
}
