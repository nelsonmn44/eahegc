'use strict';

function init()
{
	var paramMap = {};

	var paramList = location.search.substr(1).split('&');
	for (var i = 0; paramList[i].length > i ; ++i)
	{
		var tmp = paramList[i].split('=');
		paramMap[tmp[0]] = decodeURIComponent(tmp[1]);
	}
	
	paramMap['topage'] = paramMap['topage'] || 1;

	for (var i = 0, param; param = Object.keys(paramMap)[i]; ++i)
	{
		var nodeList = document.getElementsByClassName(param);
		for (var j = 0, node; node = nodeList[j]; ++j)
			node.textContent = paramMap[param];
	}
}
