<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\diagnosis
 */
namespace GH\diagnosis;

use GH\DB;

/**
 *
 */
function pre($sorId)
{
	$query =
		'select
			concat_ws(" - ",
				concat_ws(", ",
					group_concat(distinct dgn.codigo separator ", "),
					substring_index(sor.otro_diagnostico, " - ", 1)),
				case sor.tabla_diag
					when "" then null
					else substring_index(sor.tabla_diag, " - ", -1) end) as diagnosis,
			concat_ws(" - ",
				concat_ws(", ",
					group_concat(itv.codigo separator ", "),
					substring_index(sor.otro_diagnostico_intervencion, " - ", 1)),
				case sor.tabla_int
					when "" then null
					else substring_index(sor.tabla_int, " - ", -1) end) as intervention,
			case sor.tabla_obs
				when "" then null
				else sor.tabla_obs end as remark
		from ordenquirurgica as sor
		left join oqdiagnostico as sod
			on sor.id = sod.oq_id
		left join oqdiagintervencion as soi
			on sod.oq_id = soi.oq_id
			and sod.diagnostico_id = soi.diagnostico_id
		left join intervencion as itv
			on soi.intervencion_id = itv.id
		left join diagnostico as dgn
			on sod.diagnostico_id = dgn.id
		where sor.id = %i
		group by sor.id';

	return DB::$gen->query($query, $sorId);
}

/**
 *
 */
function preForm($sorId)
{
	$temp = pre($sorId);
	$row = $temp[0];

	$field = array
	(
		array
		(
			array
			(
				'name'		=> 'diagnosis',
				'title'		=> 'Diagnóstico',
				'type'		=> 'paragraph',
				'value'		=> (isset($row['diagnosisCode']) ? $row['diagnosisCode'] . ' - ' : '') . $row['diagnosis'],
				'required'	=> true
			),
			array
			(
				'name'		=> 'intervention',
				'title'		=> 'Intervenciones',
				'type'		=> 'paragraph',
				'value'		=> (isset($row['interventionCode']) ? $row['interventionCode'] . ' - ' : '') . $row['intervention'],
				'required'	=> true
			),
			array
			(
				'name'		=> 'remark',
				'title'		=> 'Observaciones',
				'type'		=> 'paragraph',
				'value'		=> $row['remark']
			)
		)
	);

	return $field;
}

/**
 *
 */
function post($sorId)
{
	$query =
		'select
			opd.postdiagnosis as diagnosis,
			opd.postintervention as intervention,
			opd.postremark as remark
		from ordenquirurgica as sor
		inner join gh_operative_protocol as op
			on sor.id = op.surgical_order_id
			and sor.id = %i
		inner join gh_operative_protocol_detail as opd
			on op.id = opd.operative_protocol_id';

	return DB::$gen->query($query, $sorId);
}

/**
 *
 */
function postForm($sorId)
{
	$temp = post($sorId);
	$row = $temp[0];

	$field = array
	(
		array
		(
			array
			(
				'name'		=> 'diagnosis',
				'title'		=> 'Diagnóstico',
				'type'		=> 'paragraph',
				'value'		=> $row['diagnosis'],
				'required'	=> true
			),
			array
			(
				'name'		=> 'intervention',
				'title'		=> 'Intervenciones',
				'type'		=> 'paragraph',
				'value'		=> $row['intervention'],
				'required'	=> true
			),
			array
			(
				'name'		=> 'remark',
				'title'		=> 'Observaciones',
				'type'		=> 'paragraph',
				'value'		=> $row['remark']
			)
		)
	);

	return $field;
}
