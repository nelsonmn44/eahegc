<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\info
 */
namespace GH\info;

/**
 * Transpone la información agrupada en filas con la metainformación de
 * los campos de información rápida
 */
function format($meta, $row)
{
	$i = 0;
	$mapList = array();

	foreach ($meta as $name => $title)
	{
		$mapList[$i] = array
		(
			'name'	=> $name,
			'title' => $title,
			'value'	=> $row[$name]
		);

		$i++;
	}

	return $mapList;
}
