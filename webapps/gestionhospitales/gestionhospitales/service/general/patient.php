<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\patient
 */
namespace GH\patient;

use GH\DB;

/**
 *
 */
function report($patId)
{
	$query =
		'select
			pat.id,
			upper(concat_ws(" ", pat.primer_nombre, pat.segundo_nombre, pat.apellido_paterno, pat.apellido_materno)) as fullName,
			pat.rut as nic,
			pat.ficha as "file",
			pat.sexo as gender,
			pat.fecha_nacimiento as birthdate,
			fz_age(pat.fecha_nacimiento) as age,
			upper(ins.nombre) as insurance,
			upper(pat.direccion) as address,
			upper(dic.nombre) as district,
			pat.telefono as phone
		from paciente as pat
		left join prevision as ins
			on pat.prevision_id = ins.id
		left join comuna as dic
			on pat.comuna_id = dic.id
		where pat.id = %i';

	return DB::$gen->queryFirstRow($query, $patId);
}

/**
 *
 */
function reportMeta()
{
	$query =
		'select
			"ID" as id,
			"Nombre" as fullName,
			"RUT" as nic,
			"Ficha" as "file",
			"Sexo" as gender,
			"Edad" as age,
			"Fecha Nacimiento" as birthdate,
			"Previsión" as insurance,
			"Dirección" as address,
			"Comuna" as district,
			"Fono" as phone';

	return DB::$gen->queryFirstRow($query);
}
