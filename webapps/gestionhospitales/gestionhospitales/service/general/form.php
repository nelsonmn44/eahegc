<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\form
 */
namespace GH\form;

use GH\DB;

/**
 * @param array.<mixed> $fieldMap
 * @return array.<mixed>
 */
function extractModel(&$fieldList)
{
	$valueMap = array();

	for ($i = 0, $il = count($fieldList); $il > $i; ++$i)
	{
		$field = &$fieldList[$i];
		if ('section' === $field['type'])
			$valueMap[$field['name']] = extractModel($field['value']);
		else if ('diagnosis' === $field['type'])
		{
			$valueMap[$field['name']] = array();
			$diagValueGroup = &$valueMap[$field['name']];

			if (!array_key_exists('value', $field))
				continue;

			for ($j = 0, $jl = count($field['value']); $jl > $j; ++$j)
			{
				$diagValueGroup[$j] = array();
				$diagFieldList = &$field['value'][$j];
				for ($k = 0, $lk = count($diagFieldList); $lk > $k; ++$k)
				{
					$diagField = &$diagFieldList[$k];

					if (array_key_exists('value', $diagField))
						$diagValueGroup[$j][$diagField['name']] = $diagField['value'];
					else
						$diagValueGroup[$j][$diagField['name']] = NULL;

					unset($diagField['value']);
				}
			}
		}
		else
		{
			if (array_key_exists('value', $field))
				$valueMap[$field['name']] = $field['value'];
			else
				$valueMap[$field['name']] = NULL;

			unset($field['value']);
		}
	}

	return $valueMap;
}

/**
 * @param array.mixed $form
 */
function format(&$form)
{
	if (array_key_exists('fieldList', $form))
		$form['valueMap'] = extractModel($form['fieldList']);
}

/**
 *
 */
function anesthesia()
{
	$query =
		'select
			ant.name as title,
			ant.id as value
		from gh_anesthesia_type as ant
		order by ant.id';

	return DB::$gen->query($query);
}

/**
 *
 */
function antiseptic()
{
	$query =
		'select
			anc.name as title,
			anc.id as value
		from gh_antiseptic as anc
		order by anc.id';

	return DB::$gen->query($query);
}

/**
 *
 */
function bool()
{
	return array
	(
		array
		(
			'title' => 'Si',
			'value' => 1
		),
		array
		(
			'title' => 'No',
			'value' => 0
		)
	);
}

/**
 *
 */
function causal()
{
	$query =
		'select
			concat_ws(" - ", sca.grupo, sca.nombre) as title,
			sca.id as value
		from oqcausasuspension as sca
		order by sca.grupo, sca.nombre';

	return DB::$gen->query($query);
}

/**
 *
 */
function careType()
{
	$query =
		'select
			cry.nombre as title,
			cry.id as value
		from tipohospitalizacion as cry
		order by cry.id';

	return DB::$gen->query($query);
}

/**
 * @param integer $speciality id especialidad
 */
function diagnosis($speciality)
{
	$query =
		'select
			dgn.nombre as title,
			dgn.id as value
		from diagnostico as dgn
		inner join especialidad as spe
			on dgn.especialidad_id = spe.id
			and spe.id = %i';

	return DB::$gen->query($query, $speciality);
}

/**
 *
 */
function pavillion()
{
	$query =
		'select
			pvs.sala as title,
			pvs.id as value
		from pabellon_sala as pvs
		order by pvs.id';

	return DB::$gen->query($query);
}

/**
 *
 */
function risk()
{
	$query =
		'select
			srs.name as title,
			srs.id as value
		from gh_surgery_risk as srs
		order by srs.id';

	return DB::$gen->query($query);
}

function medics($speId)
{
	$query =
		'select
			upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) as title,
			usr.id as value
		from usuario as usr
		where usr.esmedico = true
			and usr.esanestesista = false';

	return DB::$gen->query($query);
}

function anaesthetists()
{
	$query =
		'select
			upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) as title,
			usr.id as value
		from usuario as usr
		where usr.esanestesista = true';

	return DB::$gen->query($query);
}

/**
 *
 */
function technicians()
{
	$query =
		'select
			upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) as title,
			usr.id as value
		from usuario as usr
		where usr.esmedico = false
			and usr.esanestesista = false
			and usr.nombreusuario <> "urgencia"
			and usr.nombreusuario <> "admin"';

	return DB::$gen->query($query);
}

/**
 *
 */
function tries()
{
	$query =
		'select
			srt.name as title,
			srt.id as value
		from gh_surgery_try as srt
		order by srt.id';

	return DB::$gen->query($query);
}

/**
 *
 */
function wound()
{
	$query =
		'select
			wnt.name as title,
			wnt.id as value
		from gh_wound_type as wnt
		order by wnt.id';

	return DB::$gen->query($query);
}
