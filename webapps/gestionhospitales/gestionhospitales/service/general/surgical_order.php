<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\surgicalOrder
 */
namespace GH\surgicalOrder;

require_once('logger.php');
require_once('messages.php');
require_once('util.php');

use GH;
use GH\DB;
use GH\logger;
use GH\messages\Error;
use GH\util;

/**
 * @api
 * @param integer $id
 */
function suspendForm($id)
{
	$query =
		'select
			sor.estado_id as statusId
		from ordenquirurgica as sor
		where sor.estado_id in (2, 3, 4, 13, 14)
			and sor.id = %i';

	$sor = DB::$gen->queryFirstRow($query, $id);
	if (is_null($sor))
		GH\halt(409, GH\bloat(Error::$SOR_NON_SUSPENDABLE, $id));

	$form = array();
	$form['dataMap'] = array
	(
		'causal' => GH\form\causal()
	);

	$fieldList = array
	(
		array
		(
			'name'		=> 'suspend',
			'title'		=> 'Información de Suspención',
			'type'		=> 'section',
			'value'		=> array
			(
				array
				(
					'name'		=> 'causal',
					'title'		=> 'Causal de suspensión',
					'type'		=> 'combo',
					'data'		=> 'causal',
					'required'	=> true,
					'value'		=> (14 == $sor['statusId'] ? 48 : null)
				),
				array
				(
					'name'		=> 'remark',
					'title'		=> 'Observaciones',
					'type'		=> 'paragraph'
				)
			)
		)
	);

	$form['fieldList'] = $fieldList;
	GH\form\format($form);

	$output	= array
	(
		'form' => $form
	);

	GH\respond($output);
}

/**
 * @param integer $id
 * @param integer $causalId
 * @param string $remark
 */
function suspendedMark($id, $causalId, $remark = NULL)
{
	$query =
		'select
			sor.paciente_id as patientId,
			concat_ws(" - ", sca.grupo, sca.nombre) as causalName
		from ordenquirurgica as sor
		inner join oqcausasuspension as sca
			on %i1 = sca.id
		where sor.id = %i0';

	$sor = DB::$gen->queryFirstRow($query, $id, $causalId);

	DB::$gen->startTransaction();
	try
	{
		DB::$gen->update('ordenquirurgica', array
		(
			'estado_id'					=> 1,
			'causa_suspension_id'		=> $causalId,
			'fecha_tabla'				=> NULL,
			'soloqestado_id' 			=> 1,
			'sol_pablogico' 			=> '',
			'sol_hora' 					=> '',
			'tabla_hora' 				=> '',
			'posicion_le' 				=> NULL,
			'tiempo_adicional_extra'	=> 0,
			'esCondicional' 			=> 0,
			'pabsala_id' 				=> NULL
		), 'id = %i', $id);

		logger\push(
			'Orden Suspendida',
			$sor['causalName'] . (isset($remark) ? ': ' . $remark : ''),
			$sor['patientId'],
			$id
		);

		DB::$gen->commit();
	}
	catch (MeekroDBException $exception)
	{
		DB::$gen->rollback();
	}
}

/**
 * @api
 * @param integer $id
 */
function suspend($id)
{
	$query =
		'select
			sor.id
		from ordenquirurgica as sor
		where sor.estado_id in (2, 3, 4, 13, 14)
			and sor.id = %i';

	$sor	= DB::$gen->queryFirstRow($query, $id);
	$input	= GH\receive();

	if (is_null($sor))
		GH\halt(409, GH\bloat(Error::$SOR_NON_SUSPENDABLE, $id));
	else if (util\isVoidKey($input, 'causal'))
		GH\halt(404, GH\bloat(Error::$SOR_VOID_CAUSAL, $id));
	else if (util\isVoidKey($input, 'remark'))
		$input['remark'] = NULL;

	suspendedMark($id, $input['causal'], $input['remark']);
}

/**
 * @param integer $id
 * @return array.<string, string|number>
 */
function getPathology($id)
{
	$query =
		'select
			sop.oq_id as surgicalOrderId,
			pat.id as pathologyId,
			pat.nombre as pathologyName
		from oqpatologia as sop
		inner join patologia as pat
			on sop.patologia_id = pat.id
		where sop.oq_id = %i';

	return DB::$gen->query($query, $id);
}

/**
 * @param integer $id
 * @return array.<string, string|integer>|null
 */
function getStaff($id)
{
	$query =
		'select
			sur.id as id,
			upper(concat_ws(" ", sur.apellido_paterno, sur.apellido_materno, sur.nombre)) as name
		from ordenquirurgica as sor
		inner join usuario as sur
			on sor.cirujano_id = sur.id
		where sor.id = %i';

	$surgeon = DB::$gen->queryFirstRow($query, $id);

	$query =
		'select
			usr.id,
			upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) as name,
			usr.esmedico isMedic,
			usr.esanestesista as isAnaesthetist
		from oqequipomedico as sos
		inner join usuario as usr
			on sos.user_id = usr.id
			and usr.id <> %i
		where sos.oq_id = %i
		order by isMedic desc, isAnaesthetist desc, id';

	$rowList = DB::$gen->query($query, isset($surgeon) ? $surgeon['id'] : 0, $id);
	if (is_null($surgeon) && 0 === count($rowList))
		return;

	$staff = array
	(
		'surgeonId'				=> isset($surgeon) ? $surgeon['id'] : NULL,
		'surgeonName'			=> isset($surgeon) ? $surgeon['name'] : NULL,
		'firstAssistantId'		=> NULL,
		'firstAssistantName'	=> NULL,
		'secondAssistantId'		=> NULL,
		'secondAssistantName'	=> NULL,
		'thirdAssistantId'		=> NULL,
		'thirdAssistantName'	=> NULL,
		'anaesthetistId'		=> NULL,
		'anaesthetistName'		=> NULL,
		'surgicalNurseId'		=> NULL,
		'surgicalNurseName'		=> NULL,
		'firstRoomNurseId'		=> NULL,
		'firstRoomNurseName'	=> NULL
	);

	$m = $a = 0;
	for ($i = 0, $size = count($rowList); $size > $i; ++$i)
	{
		if ($rowList[$i]['isMedic'] && !$rowList[$i]['isAnaesthetist'])
		{
			switch ($m)
			{
				case 0 :
				{
					$staff['firstAssistantId']		= $rowList[$i]['id'];
					$staff['firstAssistantName']	= $rowList[$i]['name'];
					break;
				}
				case 1 :
				{
					$staff['secondAssistantId']		= $rowList[$i]['id'];
					$staff['secondAssistantName']	= $rowList[$i]['name'];
					break;
				}
				case 2 :
				{
					$staff['thirdAssistantId']		= $rowList[$i]['id'];
					$staff['thirdAssistantName']	= $rowList[$i]['name'];
				}
			}
			++$m;
		}
		else if ($rowList[$i]['isAnaesthetist'] && 0 === $a)
		{
			$staff['anaesthetistId']	= $rowList[$i]['id'];
			$staff['anaesthetistName']	= $rowList[$i]['name'];
			++$a;
		}
	}

	return $staff;
}

/**
 * @param integer $id
 * @param string $date
 */
function operatedMark($id, $date)
{
	$query =
		'select
			sor.paciente_id as patientId
		from ordenquirurgica as sor
		where sor.id = %i';

	$sor = DB::$gen->queryFirstRow($query, $id);

	DB::$gen->startTransaction();
	try
	{
		$now = date('Y-m-d H:i:s');

		DB::$gen->update('ordenquirurgica', array
		(
			'fecha_operado'		=> $date,
			'fecha_operacion'	=> $now,
			'fecha_le_out'		=> $now,
			'estado_id'			=> 5
		), 'id = %i', $id);

		logger\push(
			'Orden Operada',
			"Paciente fue operado el $date",
			$sor['patientId'],
			$id
		);

		DB::$gen->commit();
	}
	catch (MeekroDBException $exception)
	{
		DB::$gen->rollback();
	}
}
