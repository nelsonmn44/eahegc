<?php
/**
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\messages
 */
namespace GH\messages;

abstract class Error
{
	static $OP_ALREADY_VALIDATED		= array
	(
		'id'			=> 1023,
		'description'	=> 'El protocolo %d ya ha sido validado'
	);
	static $OP_NON_CANCELABLE			= array
	(
		'id'			=> 1024,
		'description'	=> 'El protocolo %d no existe o no es candidato para cancelación'
	);
	static $OP_NON_REMOVABLE			= array
	(
		'id'			=> 1024,
		'description'	=> 'El protocolo %d no existe o no es candidato para eliminación'
	);
	static $OP_NOT_FOUND				= array
	(
		'id'			=> 1022,
		'description'	=> 'No existe el protocolo operatorio asociado al ID %d'
	);
	static $OP_STATS_DATE_NOT_FOUND		= array
	(
		'id'			=> 1021,
		'description'	=> 'Necesita proporcionar la fecha de inicio para obtener las estadísticas de Protocolos Emitidos'
	);
	static $SOR_NON_PROTOCOLIZABLE		= array
	(
		'id'			=> 1022,
		'description'	=> 'La orden quirúrgica %d ya está protocolizada o no es candidata para protocolización'
	);
	static $SOR_NON_SUSPENDABLE			= array
	(
		'id'			=> 1021,
		'description'	=> 'La orden asociada al ID %d no es válida para suspensión'
	);
	static $SOR_VOID_CAUSAL				= array
	(
		'id'			=> 1052,
		'description'	=> 'Para suspender la orden %d necesita especificar una causal'
	);
}
