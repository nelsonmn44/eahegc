<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\logger
 */
namespace GH\logger;

require_once('auth/auth.php');

use GH\auth;
use GH\DB;

/**
 *
 */
function push($title, $description, $patientId, $sorId = NULL)
{
	$user = auth\user();

	DB::$gen->insert('actividad', array(
		'id'					=> 0,
		'actividad'				=> $title,
		'observaciones'			=> $description,
		'updated_at'			=> date('Y-m-d H:i:s'),
		'mostrarenpacientes'	=> 1,
		'paciente_id'			=> $patientId,
		'usuario_id'			=> $user['id'],
		'oq_id'					=> $sorId
	));
}
