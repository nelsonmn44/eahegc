<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\operativeProtocol
 */
namespace GH\operativeProtocol;

require_once('auth/auth.php');
require_once('diagnosis.php');
require_once('form.php');
require_once('info.php');
require_once('patient.php');
require_once('surgical_order.php');
require_once('util.php');

use GH;
use GH\DB;
use GH\surgicalOrder;

/**
 * @param integer $opId
 * @return array.<string, string|number>|null
 */
function report($opId)
{
	$query =
		"select
			op.id as operativeProtocolId,
			op.surgical_order_id as surgicalOrderId,
			sor.paciente_id as patientId,
			op.creation_date as creationDate,
			date_format(op.surgery_date, '%Y-%m-%d') as surgeryDate,
			date_format(op.surgery_date, '%H:%i') as surgeryStart,
			date_format(date_add(op.surgery_date, interval opd.surgical_time hour_second), '%H:%i') as surgeryEnd,
			date_format(opd.surgical_time, '%H:%i') as surgicalTime,
			upper(concat_ws(' ', usr.nombre, usr.apellido_paterno, usr.apellido_materno)) as userFullName,
			opd.prediagnosis as prediagnosis,
			opd.preintervention as preintervention,
			opd.preremark as preremark,
			opd.postdiagnosis as postdiagnosis,
			opd.postintervention as postintervention,
			opd.postremark as postremark,
			spy.nombre as specialityName,
			crt.nombre as careTypeName,
			pvl.sala as pavilionName,
			sgt.name as surgeryTryName,
			wnt.name as woundTypeName,
			ant.name as anesthesiaTypeName,
			srk.name as surgeryRiskName,
			anc.name as antisepticName,
			case when opd.biopsy then 'Si' else 'No' end as biopsy,
			case when opd.culture then 'Si' else 'No' end as culture,
			case when opd.imaging then 'Si' else 'No' end as imaging,
			case when opd.skinprep then 'Si' else 'No' end as skinprep,
			case when opd.antibiotic_prophylaxis is null then 'No' else concat('Si (', opd.antibiotic_prophylaxis, ')') end as antibiotic,
			opd.surgical_description as surgicalDescription
		from gh_operative_protocol as op
		inner join gh_operative_protocol_detail opd
			on op.id = opd.operative_protocol_id
		inner join ordenquirurgica as sor
			on op.surgical_order_id = sor.id
		inner join usuario as usr
			on op.user_id = usr.id
		left join tipohospitalizacion as crt
			on opd.care_type_id = crt.id
		inner join especialidad as spy
			on sor.especialidad_id = spy.id
		left join pabellon_sala as pvl
			on opd.pavilion_id = pvl.id
		left join gh_surgery_try as sgt
			on opd.surgery_try_id = sgt.id
		left join gh_wound_type as wnt
			on opd.wound_type_id = wnt.id
		left join gh_anesthesia_type as ant
			on opd.anesthesia_type_id = ant.id
		left join gh_surgery_risk as srk
			on opd.surgery_risk_id = srk.id
		left join gh_antiseptic as anc
			on opd.antiseptic_id = anc.id
		where op.id = $opId";

	return DB::$gen->queryFirstRow($query);
}

/**
 * @param integer $opId
 * @return array.<string, string|integer>|null
 */
function getStaff($opId)
{
	$query =
		'select
			staff.operative_protocol_id,
			sur.id as surgeonId,
			upper(concat_ws(" ", sur.apellido_paterno, sur.apellido_materno, sur.nombre)) as surgeonName,
			group_concat(staff.first_assistant_id) as firstAssistantId,
			group_concat(staff.first_assistant_name) as firstAssistantName,
			group_concat(staff.second_assistant_id) as secondAssistantId,
			group_concat(staff.second_assistant_name) as secondAssistantName,
			group_concat(staff.third_assistant_id) as thirdAssistantId,
			group_concat(staff.third_assistant_name) as thirdAssistantName,
			group_concat(staff.anaesthetist_id) as anaesthetistId,
			group_concat(staff.anaesthetist_name) as anaesthetistName,
			group_concat(staff.surgical_nurse_id) as surgicalNurseId,
			group_concat(staff.surgical_nurse_name) as surgicalNurseName,
			group_concat(staff.first_room_nurse_id) as firstRoomNurseId,
			group_concat(staff.first_room_nurse_name) as firstRoomNurseName,
			group_concat(staff.second_room_nurse_id) as secondRoomNurseId,
			group_concat(staff.second_room_nurse_name) as secondRoomNurseName
		from (
			select
				ops.operative_protocol_id,
				case when 2 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end as first_assistant_id,
				case when 2 = ops.medical_title_id and 1 = ops.ordinal then upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) end as first_assistant_name,
				case when 2 = ops.medical_title_id and 2 = ops.ordinal then ops.user_id end as second_assistant_id,
				case when 2 = ops.medical_title_id and 2 = ops.ordinal then upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) end as second_assistant_name,
				case when 2 = ops.medical_title_id and 3 = ops.ordinal then ops.user_id end as third_assistant_id,
				case when 2 = ops.medical_title_id and 3 = ops.ordinal then upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) end as third_assistant_name,
				case when 3 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end as anaesthetist_id,
				case when 3 = ops.medical_title_id and 1 = ops.ordinal then upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) end as anaesthetist_name,
				case when 4 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end as surgical_nurse_id,
				case when 4 = ops.medical_title_id and 1 = ops.ordinal then upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) end as surgical_nurse_name,
				case when 5 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end as first_room_nurse_id,
				case when 5 = ops.medical_title_id and 1 = ops.ordinal then upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) end as first_room_nurse_name,
				case when 5 = ops.medical_title_id and 2 = ops.ordinal then ops.user_id end as second_room_nurse_id,
				case when 5 = ops.medical_title_id and 2 = ops.ordinal then upper(concat_ws(" ", usr.apellido_paterno, usr.apellido_materno, usr.nombre)) end as second_room_nurse_name
			from gh_operative_protocol_staff as ops
			left join usuario as usr
				on ops.user_id = usr.id
			union
			select
				ocs.operative_protocol_id,
				case when 2 = ocs.medical_title_id and 1 = ocs.ordinal then ocs.custom_user_uid end as first_assistant_id,
				case when 2 = ocs.medical_title_id and 1 = ocs.ordinal then cus.name end as first_assistant_name,
				case when 2 = ocs.medical_title_id and 2 = ocs.ordinal then ocs.custom_user_uid end as second_assistant_id,
				case when 2 = ocs.medical_title_id and 2 = ocs.ordinal then cus.name end as second_assistant_name,
				case when 2 = ocs.medical_title_id and 3 = ocs.ordinal then ocs.custom_user_uid end as third_assistant_id,
				case when 2 = ocs.medical_title_id and 3 = ocs.ordinal then cus.name end as third_assistant_name,
				case when 3 = ocs.medical_title_id and 1 = ocs.ordinal then ocs.custom_user_uid end as anaesthetist_id,
				case when 3 = ocs.medical_title_id and 1 = ocs.ordinal then cus.name end as anaesthetist_name,
				case when 4 = ocs.medical_title_id and 1 = ocs.ordinal then ocs.custom_user_uid end as surgical_nurse_id,
				case when 4 = ocs.medical_title_id and 1 = ocs.ordinal then cus.name end as surgical_nurse_name,
				case when 5 = ocs.medical_title_id and 1 = ocs.ordinal then ocs.custom_user_uid end as first_room_nurse_id,
				case when 5 = ocs.medical_title_id and 1 = ocs.ordinal then cus.name end as first_room_nurse_name,
				case when 5 = ocs.medical_title_id and 2 = ocs.ordinal then ocs.custom_user_uid end as second_room_nurse_id,
				case when 5 = ocs.medical_title_id and 2 = ocs.ordinal then cus.name end as second_room_nurse_name
			from gh_operative_protocol_custom_staff as ocs
			left join gh_custom_user as cus
				on ocs.custom_user_uid = cus.uid) as staff
		inner join gh_operative_protocol as op
			on staff.operative_protocol_id = op.id
		inner join usuario as sur
			on op.surgeon_id = sur.id
		where staff.operative_protocol_id = %i
		group by staff.operative_protocol_id';

	return DB::$gen->queryFirstRow($query, $opId);
}

/**
 * @param integer $opId
 * @return string
 */
function getAttachments($opId)
{
	$query =
		'select
			oas.surgical_order_id as surgicalOrderId,
			oas.income_date as incomeDate,
			oas.surgeon_id as surgeonId,
			oas.surgeon_name as surgeonName
		from gh_operative_protocol_attached_surgical_order as oas
		where oas.operative_protocol_id = %i';

	return DB::$gen->query($query, $opId);
}

/**
 * @param array.<string, string|integer> $op
 * @return array.<string, *>
 */
function document($op)
{
	$document = array();
	$document['patient'] = \GH\info\format(
		\GH\patient\reportMeta(),
		\GH\patient\report($op['patientId'])
	);

	$document['info'] = \GH\info\format(
		array (
			'operativeProtocolId'	=> 'Protocolo Nº',
			'surgicalOrderId'		=> 'Orden Quirúrgica Nº',
			'date'					=> 'Fecha Protocolo',
			'userFullName'			=> 'Usuario'
		),
		array (
			'operativeProtocolId'	=> $op['operativeProtocolId'],
			'surgicalOrderId'		=> $op['surgicalOrderId'],
			'date'					=> $op['creationDate'],
			'userFullName'			=> $op['userFullName']
		)
	);

	$document['protocol'] = \GH\info\format(
		array (
			'surgeryDate'	=> 'Fecha Intervención',
			'surgeryStart'	=> 'Hora Inicio Intervención',
			'surgeryEnd'	=> 'Hora Término Intervención',
			'surgicalTime'	=> 'Duración Intervención',
			'careTypeName'	=> 'Modalidad Atención',
			'pavilionName'	=> 'Pabellón'
		),
		array (
			'surgeryDate'	=> $op['surgeryDate'],
			'surgeryStart'	=> $op['surgeryStart'],
			'surgeryEnd'	=> $op['surgeryEnd'],
			'surgicalTime'	=> $op['surgicalTime'],
			'careTypeName'	=> $op['careTypeName'],
			'pavilionName'	=> $op['pavilionName']
		)
	);

	$staff = getStaff($op['operativeProtocolId']);
	$document['staff'] = \GH\info\format(
		array (
			'surgeon'			=> 'Cirujano',
			'firstAssistant'	=> 'Ayudante 1',
			'secondAssistant'	=> 'Ayudante 2',
			'thirdAssistant'	=> 'Ayudante 3',
			'anaesthetist'		=> 'Anestesiólogo',
			'surgicalNurse'		=> 'Arsenalera',
			'firstRoomNurse'	=> 'Pabellonera'
		),
		array (
			'surgeon'			=> $staff['surgeonName'],
			'firstAssistant'	=> $staff['firstAssistantName'],
			'secondAssistant'	=> $staff['secondAssistantName'],
			'thirdAssistant'	=> $staff['thirdAssistantName'],
			'anaesthetist'		=> $staff['anaesthetistName'],
			'surgicalNurse'		=> $staff['surgicalNurseName'],
			'firstRoomNurse'	=> $staff['firstRoomNurseName']
		)
	);

	$document['procedure'] = \GH\info\format(
		array (
			'prediagnosis'			=> 'Diagnósticos Preoperatorios',
			'postdiagnosis'			=> 'Diagnósticos Postoperatorios',
			'surgeryTry'			=> 'Reintervención',
			'woundType'				=> 'Tipo de Herida',
			'anesthesiaType'		=> 'Tipo de Anestesia',
			'surgeryRisk'			=> 'Riesgo Operatorio',
			'biopsy'				=> 'Biopsia',
			'culture'				=> 'Cultivo',
			'imaging'				=> 'Rayos (TV)',
			'antiseptic'			=> 'Antiséptico Utilizado',
			'skinprep'				=> 'Preparación Piel',
			'antibiotic'			=> 'Antibioprofilaxis Quirúrgica',
			'surgicalDescription'	=> 'Descripción Quirúrgica'
		),
		array (
			'prediagnosis'			=> \GH\info\format(
				array (
					'diagnosis'		=> 'Diagnostico',
					'intervention'	=> 'Intervenciones',
					'remark'		=> 'Observaciones'
				),
				array (
					'diagnosis'		=> $op['prediagnosis'],
					'intervention'	=> $op['preintervention'],
					'remark'		=> $op['preremark']
				)
			),
			'postdiagnosis'			=> \GH\info\format(
				array (
					'diagnosis'		=> 'Diagnostico',
					'intervention'	=> 'Intervenciones',
					'remark'		=> 'Observaciones'
				),
				array (
					'diagnosis'		=> $op['postdiagnosis'],
					'intervention'	=> $op['postintervention'],
					'remark'		=> $op['postremark']
				)
			),
			'surgeryTry'			=> $op['surgeryTryName'],
			'woundType'				=> $op['woundTypeName'],
			'anesthesiaType'		=> $op['anesthesiaTypeName'],
			'surgeryRisk'			=> $op['surgeryRiskName'],
			'biopsy'				=> $op['biopsy'],
			'culture'				=> $op['culture'],
			'imaging'				=> $op['imaging'],
			'antiseptic'			=> $op['antisepticName'],
			'skinprep'				=> $op['skinprep'],
			'antibiotic'			=> $op['antibiotic'],
			'surgicalDescription'	=> $op['surgicalDescription']
		)
	);

	$attachList = getAttachments($op['operativeProtocolId']);
	for ($i = 0, $size = count($attachList); $size > $i; ++$i)
		$attachList[$i]['preDiagnosisList'] = \GH\diagnosis\pre($attachList[$i]['surgicalOrderId']);

	$document['attachList'] = $attachList;

	return $document;
}

/**
 * @param string $criteria
 * @param integer $offset
 * @param integer $count
 * @param integer &$size
 */
function pendingSearch($criteria, $offset, $count, &$size)
{
	$query =
		'select
			count(1) as size
		from gh_operative_protocol_candidate as opc
		inner join ordenquirurgica as sor
			on opc.surgical_order_id = sor.id
		inner join paciente as pat
			on opc.patient_id = pat.id
		where opc.patient_name like %ss
			or opc.surgeon_name like %ss0
			or date_format(opc.programmed_date, "%Y-%m-%d") like %ss0
			or sor.tabla_diag like %ss0
			or sor.tabla_int like %ss0
			or pat.rut like %ss0
			or pat.ficha like %ss0';

	$row = DB::$gen->queryFirstRow($query, $criteria);
	$size = $row['size'];

	$query =
		'select
			opc.operative_protocol_id as operativeProtocolId,
			opc.surgical_order_id as surgicalOrderId,
			opc.elapsed_days + 1 as elapsedDays,
			opc.programmed_date as programmedDate,
			opc.pavilion_id as pavilionId,
			opc.pavilion_name as pavilionName,
			opc.patient_id as patientId,
			opc.patient_name as patientName,
			opc.surgeon_id as surgeonId,
			opc.surgeon_name as surgeonName,
			opc.speciality_id as specialityId,
			opc.speciality_name as specialityName,
			opc.status_id as statusId,
			opc.status_name as statusName
		from gh_operative_protocol_candidate as opc
		inner join ordenquirurgica as sor
			on opc.surgical_order_id = sor.id
		inner join paciente as pat
			on opc.patient_id = pat.id
		where opc.patient_name like %ss
			or opc.surgeon_name like %ss0
			or date_format(opc.programmed_date, "%Y-%m-%d") like %ss0
			or sor.tabla_diag like %ss0
			or sor.tabla_int like %ss0
			or pat.rut like %ss0
			or pat.ficha like %ss0
		order by elapsedDays desc, programmedDate desc
		limit %i1, %i2';

	$rowList = DB::$gen->query($query, $criteria, $offset, $count);

	for ($i = 0, $length = count($rowList); $length > $i; ++$i)
		$rowList[$i]['preDiagnosisList'] = \GH\diagnosis\pre($rowList[$i]['surgicalOrderId']);

	return $rowList;
}

/**
 * @api
 */
function pendingList()
{
	$params = \GH\getParams();
	if (array_key_exists('filter', $params))
	{
		$size = 0;
		$output = array
		(
			'itemList' => pendingSearch(
				$params['filter'], $params['offset'], $params['count'], $size
			),
			'fields' => array
			(
				'size' => array
				(
					'value' => $size
				),
				'pageSize' => array
				(
					'value' => $params['count']
				),
				'offset' => array
				(
					'value' => $params['offset']
				)
			)
		);

		\GH\respond($output);
		return;
	}

	$query =
		'select
			opc.operative_protocol_id as operativeProtocolId,
			opc.surgical_order_id as surgicalOrderId,
			opc.elapsed_days + 1 as elapsedDays,
			opc.programmed_date as programmedDate,
			opc.pavilion_id as pavilionId,
			opc.pavilion_name as pavilionName,
			opc.patient_id as patientId,
			opc.patient_name as patientName,
			opc.surgeon_id as surgeonId,
			opc.surgeon_name as surgeonName,
			opc.speciality_id as specialityId,
			opc.speciality_name as specialityName,
			opc.status_id as statusId,
			opc.status_name as statusName
		from gh_operative_protocol_candidate as opc
		where opc.elapsed_days < 40
			and opc.status_id in (2, 3, 4, 13, 14, 51)
		order by elapsedDays desc, programmedDate desc
		limit %i';

	$rowList = DB::$gen->query($query, PAGE_SIZE_MAX);

	for ($i = 0, $size = count($rowList); $size > $i; ++$i)
		$rowList[$i]['preDiagnosisList'] = \GH\diagnosis\pre($rowList[$i]['surgicalOrderId']);

	$output = array
	(
		'fields' => array
		(
			'size' => array
			(
				'value' => count($rowList)
			),
			'pageSize' => array
			(
				'value' => PAGE_SIZE_MAX
			),
			'offset' => array
			(
				'value' => 0
			)
		),
		'itemList' => $rowList
	);

	\GH\respond($output);
}

/**
 * @param string $criteria
 * @param number $offset
 * @param number $count
 */
function issuedSearch($criteria, $offset, $count)
{
	$query =
		'select
			opi.operative_protocol_id as operativeProtocolId,
			opi.surgical_order_id as surgicalOrderId,
			opi.surgery_date as protocolDate,
			opi.patient_id as patientId,
			opi.patient_name as patientName,
			opi.speciality_id as specialityId,
			opi.speciality_name as specialityName,
			opi.pavilion_id as pavilionId,
			opi.pavilion_name as pavilionName,
			opi.surgeon_id as surgeonId,
			opi.surgeon_name as surgeonName,
			opi.status_id as statusId,
			opi.status_name as statusName,
			opi.try_id as tryId,
			opi.try_name as tryName
		from gh_operative_protocol_issued as opi
		inner join gh_operative_protocol_detail as opd
			on opi.operative_protocol_id = opd.operative_protocol_id
		inner join paciente as pat
			on opi.patient_id = pat.id
		left join gh_operative_protocol_staff as ops
			on opi.operative_protocol_id = ops.operative_protocol_id
		left join usuario as usr
			on ops.user_id = usr.id
		where opi.patient_name like %ss
			or opi.surgeon_name like %ss0
			or concat_ws(" ", usr.nombre, usr.apellido_paterno, usr.apellido_materno) like %ss0
			or date_format(opi.surgery_date, "%Y-%m-%d") like %ss0
			or opd.postdiagnosis like %ss0
			or opd.postintervention like %ss0
			or pat.rut like %ss0
			or pat.ficha like %ss0
			or opi.try_name like %ss0
		group by operativeProtocolId
		order by protocolDate asc
		limit %i1, %i2';

	$rowList = DB::$gen->query($query, $criteria, $offset, $count);

	for ($i = 0, $size = count($rowList); $size > $i; ++$i)
		$rowList[$i]['postDiagnosisList'] = \GH\diagnosis\post($rowList[$i]['surgicalOrderId']);

	return $rowList;
}

/**
 * @api
 */
function issuedList()
{
	$params = \GH\getParams();
	if (array_key_exists('filter', $params))
	{
		$output = array
		(
			'fields' => array
			(
				'size' => array
				(
					'value' => 200
				),
				'pageSize' => array
				(
					'value' => $params['count']
				),
				'offset' => array
				(
					'value' => $params['offset']
				)
			),
			'itemList' => issuedSearch(
				$params['filter'], $params['offset'], $params['count']
			)
		);

		\GH\respond($output);
		return;
	}

	$query =
		'QW2Z';

	$rowList = DB::$gen->query($query, PAGE_LENGTH);

	for ($i = 0, $size = count($rowList); $size > $i; ++$i)
		$rowList[$i]['postDiagnosisList'] = \GH\diagnosis\post($rowList[$i]['surgicalOrderId']);

	$output = array
	(
		'fields' => array
		(
			'size' => array
			(
				'value' => PAGE_LENGTH
			)
		),
		'itemList' => $rowList
	);

	\GH\respond($output);
}

/**
 * @param integer $surgeonId
 * @param integer $surgicalOrderId
 */
function getDescriptionTemplate($surgeonId, $surgicalOrderId)
{
	if (is_null($surgicalOrderId))
		return;

	if (is_null($surgeonId))
	{
		$query =
			'select
				sor.cirujano_id as surgeonId
			from ordenquirurgica as sor
			where sor.id = %i';

		$sor = DB::$gen->queryFirstRow($query, $surgicalOrderId);
		if (is_null($sor) || is_null($sor['surgeonId']))
			return;

		$surgeonId = $sor['surgeonId'];
	}

	$query =
		'select
			min(soi.intervencion_id) as mandatoryId
		from ordenquirurgica as sor
		inner join oqdiagnostico as sod
			on sor.id = sod.oq_id
		inner join oqdiagintervencion as soi
			on sod.oq_id = soi.oq_id
			and sod.diagnostico_id = soi.diagnostico_id
		where sor.id = %i
		group by sor.id';

	$sor = DB::$gen->queryFirstRow($query, $surgicalOrderId);
	if (is_null($sor) || is_null($sor['mandatoryId']))
		return;

	$query =
		'select
			odt.template
		from gh_operative_protocol_description_template as odt
		where odt.user_id = %i
			and odt.intervention_id = %i';

	$row = DB::$gen->queryFirstRow($query, $surgeonId, $sor['mandatoryId']);
	return $row['template'];
}

/**
 * @param integer $opId
 */
function setDescriptionTemplate($opId)
{
	if (is_null($opId))
		return;

	$query =
		'select
			op.surgical_order_id as surgicalOrderId,
			op.surgeon_id as surgeonId,
			opd.surgical_description as surgicalDescription
		from gh_operative_protocol as op
		inner join gh_operative_protocol_detail as opd
			on op.id = opd.operative_protocol_id
		where op.id = %i';

	$op = DB::$gen->queryFirstRow($query, $opId);
	if (is_null($op))
		return;

	$query =
		'select
			min(soi.intervencion_id) as mandatoryId
		from ordenquirurgica as sor
		inner join oqdiagnostico as sod
			on sor.id = sod.oq_id
		inner join oqdiagintervencion as soi
			on sod.oq_id = soi.oq_id
			and sod.diagnostico_id = soi.diagnostico_id
		where sor.id = %i
		group by sor.id';

	$sor = DB::$gen->queryFirstRow($query, $op['surgicalOrderId']);
	if (is_null($sor) || !array_key_exists('mandatoryId', $sor))
		return;

	$query =
		'select
			odt.template
		from gh_operative_protocol_description_template as odt
		where odt.user_id = %i
			and odt.intervention_id = %i';

	if (is_null(DB::$gen->queryFirstRow($query, $op['surgeonId'], $sor['mandatoryId'])))
		DB::$gen->insert('gh_operative_protocol_description_template', array(
			'user_id'			=> $op['surgeonId'],
			'intervention_id'	=> $sor['mandatoryId'],
			'template'			=> $op['surgicalDescription']
		));
}

/**
 * @api
 * @param integer $soId
 */
function createForm($soId)
{
	$query =
		"select
			opc.surgical_order_id as id,
			opc.patient_id as patientId,
			opc.speciality_id as specialityId,
			opc.status_id as statusId,
			date_format(opc.programmed_date, '%Y-%m-%d') as procedureDate,
			date_format(opc.programmed_date, '%H:%i:%s') as procedureStart,
			date_format(date_add(opc.programmed_date, interval sor.tiempo_estimado + sor.tiempo_adicional minute), '%H:%i:%s') as procedureEnd,
			sor.tipo_hospitalizacion_id as careTypeId,
			opc.pavilion_id as pavilionId,
			opc.surgeon_id as surgeonId,
			sor.tabla_diag as prediagnosis,
			sor.tabla_int as preintervention,
			sor.tabla_obs as preremark
		from gh_operative_protocol_candidate as opc
		inner join ordenquirurgica as sor
			on opc.surgical_order_id = sor.id
		where opc.surgical_order_id = $soId
			and opc.status_id <> 51";

	$sor = DB::$gen->queryFirstRow($query);
	if (is_null($sor))
		GH\halt(409, GH\bloat(Error::$SOR_NON_PROTOCOLIZABLE, $soId));

	$now = date('Y-m-d H:i:s');

	$meta = array
	(
		'patient' => array
		(
			'id' => $sor['patientId']
		),
		'protocol' => array
		(
			'date' => $now
		)
	);

	$document = array();
	$document['patient'] = \GH\info\format(
		\GH\patient\reportMeta(),
		\GH\patient\report($sor['patientId'])
	);

	$user = \GH\auth\user();
	$document['info'] = \GH\info\format(
		array (
			'surgicalOrderId'	=> 'Orden Quirúrgica Nº',
			'date'				=> 'Fecha Protocolo',
			'user'				=> 'Usuario'
		),
		array (
			'surgicalOrderId'	=> $sor['id'],
			'date'				=> $now,
			'user'				=> $user['fullname']
		)
	);

	$form = array();
	$form['dataMap'] = array
	(
		'bool'			=> \GH\form\bool(),
		'careType'		=> \GH\form\careType(),
		'pavilion'		=> \GH\form\pavillion(),
		'tries'			=> \GH\form\tries(),
		'wound'			=> \GH\form\wound(),
		'anesthesia'	=> \GH\form\anesthesia(),
		'risk'			=> \GH\form\risk(),
		'medic'			=> \GH\form\medics($sor['specialityId']),
		'anaesthetist'	=> \GH\form\anaesthetists(),
		'antiseptic'	=> \GH\form\antiseptic(),
		'technician'	=> \GH\form\technicians()
	);

	$staff = \GH\surgicalOrder\getStaff($soId);
	$fieldList = array
	(
		array
		(
			'name'		=> 'protocol',
			'title'		=> 'Datos Protocolo Operatorio',
			'type'		=> 'section',
			'value'		=> array
			(
				array
				(
					'name'		=> 'date',
					'title'		=> 'Fecha Intervención',
					'type'		=> 'date',
					'value'		=> $sor['procedureDate'],
					'required'	=> true
				),
				array
				(
					'name'		=> 'start',
					'title'		=> 'Hora Inicio Intervención',
					'type'		=> 'time',
					'required'	=> true
				),
				array
				(
					'name'		=> 'end',
					'title'		=> 'Hora Término Intervención',
					'type'		=> 'time',
					'required'	=> true
				),
				array
				(
					'name'		=> 'careType',
					'title'		=> 'Modalidad Atención',
					'type'		=> 'combo',
					'data'		=> 'careType',
					'value'		=> $sor['careTypeId'],
					'required'	=> true
				),
				array
				(
					'name'		=> 'pavilion',
					'title'		=> 'Pabellón',
					'type'		=> 'combo',
					'data'		=> 'pavilion',
					'value'		=> $sor['pavilionId'],
					'required'	=> true
				)
			)
		),
		array
		(
			'name'		=> 'staff',
			'title'		=> 'Equipo Médico',
			'type'		=> 'section',
			'value'		=> array
			(
				array
				(
					'name'		=> 'surgeon',
					'title'		=> 'Primer Cirujano',
					'type'		=> 'typeahead',
					'data'		=> 'medic',
					'filter'	=> 'user',
					'value'		=> $staff['surgeonId'],
					'required'	=> true
				),
				array
				(
					'name'		=> 'firstAssistant',
					'title'		=> 'Ayudante 1',
					'type'		=> 'typeahead',
					'filter'	=> 'user',
					'value'		=> $staff['firstAssistantId'],
					'data'		=> 'medic',
					'required'	=> true
				),
				array
				(
					'name'		=> 'secondAssistant',
					'title'		=> 'Ayudante 2',
					'type'		=> 'typeahead',
					'filter'	=> 'user',
					'value'		=> $staff['secondAssistantId'],
					'data'		=> 'medic'
				),
				array
				(
					'name'		=> 'thirdAssistant',
					'title'		=> 'Ayudante 3',
					'type'		=> 'typeahead',
					'filter'	=> 'user',
					'value'		=> $staff['thirdAssistantId'],
					'data'		=> 'medic'
				),
				array
				(
					'name'		=> 'anaesthetist',
					'title'		=> 'Anestesiólogo',
					'type'		=> 'typeahead',
					'filter'	=> 'user',
					'value'		=> $staff['anaesthetistId'],
					'data'		=> 'anaesthetist',
					'required'	=> true
				),
				array
				(
					'name'		=> 'surgicalNurse',
					'title'		=> 'Arsenalera',
					'type'		=> 'typeahead',
					'filter'	=> 'user',
					'data'		=> 'technician',
					'value'		=> $staff['surgicalNurseId'],
					'required'	=> true
				),
				array
				(
					'name'		=> 'firstRoomNurse',
					'title'		=> 'Pabellonera',
					'type'		=> 'typeahead',
					'filter'	=> 'user',
					'data'		=> 'technician',
					'value'		=> $staff['firstRoomNurseId'],
					'required'	=> true
				)
			)
		),
		array
		(
			'name'		=> 'procedure',
			'title'		=> 'Detalle Intervención',
			'type'		=> 'section',
			'value'		=> array
			(
				array
				(
					'name'		=> 'prediagnosis',
					'title'		=> 'Diagnósticos Preoperatorios',
					'type'		=> 'diagnosis',
					'value'		=> \GH\diagnosis\preForm($sor['id'])
				),
				array
				(
					'name'		=> 'tries',
					'title'		=> 'Reintervención',
					'type'		=> 'radiogroup',
					'data'		=> 'tries',
					'required'	=> true
				),
				array
				(
					'name'		=> 'antibiotic',
					'title'		=> 'Antibioprofilaxis Quirúrgica',
					'type'		=> 'boolcheck',
					'onType'	=> 'time'
				),
				array
				(
					'name'		=> 'description',
					'title'		=> 'Descripción Quirúrgica',
					'type'		=> 'paragraph',
					'required'	=> true,
					'value'		=> getDescriptionTemplate($sor['surgeonId'], $soId)
				)
			)
		)
	);

	$form['fieldList'] = $fieldList;
	\GH\form\format($form);

	$output	= array
	(
		'meta'		=> $meta,
		'document'	=> $document,
		'form'		=> $form
	);

	\GH\respond($output);
}

/**
 * @param string $name
 * @return string como uid
 */
function customUser($name)
{
	$query =
		'select
			csr.uid,
			csr.name
		from gh_custom_user csr
		where csr.name = %s';

	$customUser = DB::$gen->queryFirstRow($query, strtoupper($name));

	$usrUid;
	if (is_null($customUser))
	{
		$usrUid = uniqid('', true);
		DB::$gen->insert('gh_custom_user', array(
			'uid'	=> $usrUid,
			'name'	=> strtoupper($name)
		));
	}
	else
		$usrUid = $customUser['uid'];

	return $usrUid;
}

/**
 * @api
 */
function create()
{
	$user = \GH\auth\user();

	$input			= \GH\receive();
	$document		= $input['document'];
	$procedure		= $input['procedure'];
	$protocol		= $input['protocol'];
	$staff			= $input['staff'];
	$prediagnosis	= $procedure['prediagnosis'][0];

	$query =
		'select
			opc.surgical_order_id
		from gh_operative_protocol_candidate as opc
		where opc.status_id <> 51
			and opc.surgical_order_id = %i';

	$sor = DB::$gen->queryFirstRow($query, $document['surgicalOrder']);
	if (is_null($sor))
		GH\halt(409, GH\bloat(Error::$SOR_NON_PROTOCOLIZABLE, $soId));

	$opId;
	DB::$gen->startTransaction();
	try
	{
		DB::$gen->insert('gh_operative_protocol', array(
			'id'					=> 0,
			'surgical_order_id'		=> $document['surgicalOrder'],
			'creation_date'			=> $document['date'],
			'surgery_date'			=> $protocol['date'] . ' ' . $protocol['start'],
			'surgeon_id'			=> $staff['surgeon'],
			'user_id'				=> $user['id']
		));

		$opId = DB::$gen->insertId();

		DB::$gen->insert('gh_operative_protocol_detail', array(
			'operative_protocol_id'		=> $opId,
			'surgical_time'				=> array_key_exists('end', $protocol)				? DB::$gen->sqleval('timediff(%t, %t)', $protocol['end'], $protocol['start']) : NULL,
			'care_type_id'				=> array_key_exists('careType', $protocol)			? $protocol['careType'] : NULL,
			'pavilion_id'				=> array_key_exists('pavilion', $protocol)			? $protocol['pavilion'] : NULL,
			'surgery_try_id'			=> array_key_exists('tries', $procedure)			? $procedure['tries'] : NULL,
			'antibiotic_prophylaxis'	=> array_key_exists('antibiotic', $procedure)		? $procedure['antibiotic'] : NULL,
			'surgical_description'		=> array_key_exists('description', $procedure)		? $procedure['description'] : NULL
		));

		if (array_key_exists('firstAssistant', $staff))
		{
			if (is_numeric($staff['firstAssistant']))
				DB::$gen->insert('gh_operative_protocol_staff', array(
					'operative_protocol_id'	=> $opId,
					'user_id'				=> $staff['firstAssistant'],
					'medical_title_id'		=> 2
				));
			else if (is_string($staff['firstAssistant']))
				DB::$gen->insert('gh_operative_protocol_custom_staff', array(
					'operative_protocol_id'	=> $opId,
					'custom_user_uid'		=> customUser($staff['firstAssistant']),
					'medical_title_id'		=> 2
				));
		}

		if (array_key_exists('secondAssistant', $staff))
		{
			if (is_numeric($staff['secondAssistant']))
				DB::$gen->insert('gh_operative_protocol_staff', array(
					'operative_protocol_id'	=> $opId,
					'user_id'				=> $staff['secondAssistant'],
					'medical_title_id'		=> 2,
					'ordinal'				=> 2
				));
			else if (is_string($staff['secondAssistant']))
				DB::$gen->insert('gh_operative_protocol_custom_staff', array(
					'operative_protocol_id'	=> $opId,
					'custom_user_uid'		=> customUser($staff['secondAssistant']),
					'medical_title_id'		=> 2,
					'ordinal'				=> 2
				));
		}

		if (array_key_exists('thirdAssistant', $staff))
		{
			if (is_numeric($staff['thirdAssistant']))
				DB::$gen->insert('gh_operative_protocol_staff', array(
					'operative_protocol_id'	=> $opId,
					'user_id'				=> $staff['thirdAssistant'],
					'medical_title_id'		=> 2,
					'ordinal'				=> 3
				));
			else if (is_string($staff['thirdAssistant']))
				DB::$gen->insert('gh_operative_protocol_custom_staff', array(
					'operative_protocol_id'	=> $opId,
					'custom_user_uid'		=> customUser($staff['thirdAssistant']),
					'medical_title_id'		=> 2,
					'ordinal'				=> 3
				));
		}

		if (array_key_exists('anaesthetist', $staff))
		{
			if (is_numeric($staff['anaesthetist']))
				DB::$gen->insert('gh_operative_protocol_staff', array(
					'operative_protocol_id'	=> $opId,
					'user_id'				=> $staff['anaesthetist'],
					'medical_title_id'		=> 3
				));
		}

		if (array_key_exists('surgicalNurse', $staff))
		{
			if (is_numeric($staff['surgicalNurse']))
				DB::$gen->insert('gh_operative_protocol_staff', array(
					'operative_protocol_id'	=> $opId,
					'user_id'				=> $staff['surgicalNurse'],
					'medical_title_id'		=> 4
				));
			else if (is_string($staff['surgicalNurse']))
				DB::$gen->insert('gh_operative_protocol_custom_staff', array(
					'operative_protocol_id'	=> $opId,
					'custom_user_uid'		=> customUser($staff['surgicalNurse']),
					'medical_title_id'		=> 4
				));
		}

		if (array_key_exists('firstRoomNurse', $staff))
		{
			if (is_numeric($staff['firstRoomNurse']))
				DB::$gen->insert('gh_operative_protocol_staff', array(
					'operative_protocol_id'	=> $opId,
					'user_id'				=> $staff['firstRoomNurse'],
					'medical_title_id'		=> 5
				));
			else if (is_string($staff['firstRoomNurse']))
				DB::$gen->insert('gh_operative_protocol_custom_staff', array(
					'operative_protocol_id'	=> $opId,
					'custom_user_uid'		=> customUser($staff['firstRoomNurse']),
					'medical_title_id'		=> 5
				));
		}

		setDescriptionTemplate($opId);
		DB::$gen->commit();
	}
	catch (MeekroDBException $exception)
	{
		DB::$gen->rollback();
	}

	\GH\respond(array('id' => $opId), 201);
}

/**
 * @api
 * @param integer $opId
 */
function validateForm($opId)
{
	$query =
		'select
			opi.status_id
		from gh_operative_protocol_issued as opi
		where opi.operative_protocol_id = %i';

	$op = DB::$gen->queryFirstRow($query, $opId);
	if (is_null($op))
		GH\halt(409, GH\bloat(Error::$OP_NOT_FOUND, $opId));
	else if(52 == $op['status_id'])
		GH\halt(409, GH\bloat(Error::$OP_ALREADY_VALIDATED, $opId));

	$op = report($opId);

	$query =
		'select
			case when %i = opc.surgical_order_id then true end as current,
			opc.surgical_order_id as surgicalOrderId,
			opc.income_date as incomeDate,
			opc.patient_id as patientId,
			opc.patient_name as patientName,
			opc.surgeon_id as surgeonId,
			opc.surgeon_name as surgeonName,
			opc.status_id as statusId,
			opc.status_name as statusName
		from gh_operative_protocol_candidate as opc
		where opc.patient_id = %i
			and opc.status_id <> 5
			or opc.surgical_order_id = %i0
		order by current desc';

	$attachList = DB::$gen->query($query, $op['surgicalOrderId'], $op['patientId']);

	for ($i = 0, $size = count($attachList); $size > $i; ++$i)
		$attachList[$i]['preDiagnosisList'] = \GH\diagnosis\pre($attachList[$i]['surgicalOrderId']);

	$output = array(
		'meta'		=> array
		(
			'patient'	=> array
			(
				'id'	=> $op['patientId']
			)
		),
		'document'	=> document($op),
		'attach'	=> $attachList
	);

	\GH\respond($output);
}

/**
 * @api
 * @param integer $opId
 */
function validate($opId)
{
	$query =
		'select
			opi.surgical_order_id as surgicalOrderId,
			opi.surgery_date as surgeryDate,
			opi.status_id as operativeProtocolStatusId,
			sor.estado_id as surgicalOrderStatusId
		from gh_operative_protocol_issued as opi
		inner join ordenquirurgica as sor
			on opi.surgical_order_id = sor.id
		where opi.operative_protocol_id = %i';

	$op = DB::$gen->queryFirstRow($query, $opId);
	if (is_null($op))
		GH\halt(409, GH\bloat(Error::$OP_NOT_FOUND, $opId));
	else if(52 == $op['operativeProtocolStatusId'])
		GH\halt(409, GH\bloat(Error::$OP_ALREADY_VALIDATED, $opId));

	$input = \GH\receive();

	DB::$gen->startTransaction();
	try
	{
		DB::$gen->update('gh_operative_protocol', array
		(
			'validation_date' => date('Y-m-d H:i:s')
		), 'id = %i', $opId);

		if (array_key_exists('attached', $input) && 1 < count($input['attached']))
		{
			$attachList = $input['attached'];
			for ($i = 1, $size = count($attachList); $size > $i; ++$i)
				DB::$gen->insert('gh_attached_surgical_order', array(
					'operative_protocol_id'	=> $opId,
					'surgical_order_id'		=> $attachList[$i]
				));
		}

		if (5 != $op['surgicalOrderStatusId'])
			surgicalOrder\operatedMark(
				$op['surgicalOrderId'],
				$op['surgeryDate']
			);

		DB::$gen->commit();
	}
	catch (MeekroDBException $exception)
	{
		DB::$gen->rollback();
	}
}

/**
 * @api
 * @param integer $opId
 */
function removeForm($opId)
{
	$query =
		'select
			opi.operative_protocol_id as operativeProtocolId,
			opi.surgical_order_id as surgicalOrderId,
			opi.operative_protocol_date as creationDate,
			opi.patient_name as patientName,
			opi.surgeon_name as surgeonName,
			opi.status_name as statusName
		from gh_operative_protocol_issued as opi
		where opi.status_id = 51
			and opi.operative_protocol_id = %i';

	$op = DB::$gen->queryFirstRow($query, $opId);
	if (is_null($op))
		GH\halt(409, GH\bloat(Error::$OP_NON_REMOVABLE, $opId));

	$op['postDiagnosisList'] = \GH\diagnosis\post($op['surgicalOrderId']);

	$output = array(
		'info' => $op
	);

	\GH\sendData($output);
}

/**
 * @api
 * @param integer $opId
 */
function remove($opId)
{
	$query =
		'select
			opi.status_name as statusName
		from gh_operative_protocol_issued as opi
		where opi.status_id = 51
			and opi.operative_protocol_id = %i';

	$op = DB::$gen->queryFirstRow($query, $opId);
	if (is_null($op))
		GH\halt(409, GH\bloat(Error::$OP_NON_CANCELABLE, $opId));

	DB::$gen->startTransaction();
	try
	{
		DB::$gen->delete(
			'gh_attached_surgical_order',
			'operative_protocol_id = %i',
			$opId
		);

		DB::$gen->delete(
			'gh_operative_protocol_staff',
			'operative_protocol_id = %i',
			$opId
		);

		DB::$gen->delete(
			'gh_operative_protocol_custom_staff',
			'operative_protocol_id = %i',
			$opId
		);

		DB::$gen->delete(
			'gh_operative_protocol_detail',
			'operative_protocol_id = %i',
			$opId
		);

		DB::$gen->delete(
			'gh_operative_protocol',
			'id = %i',
			$opId
		);

		DB::$gen->commit();
	}
	catch (MeekroDBException $exception)
	{
		DB::$gen->rollback();
	}
}

/**
 * @api
 * @param integer $opId
 */
function view($opId)
{
	$op = report($opId);

	$output = array(
		'meta'		=> array
		(
			'patient'	=> array
			(
				'id'	=> $op['patientId']
			)
		),
		'document'	=> document($op)
	);

	\GH\respond($output);
}

/**
 * @param string $beginDate
 * @param string $endDate
 */
function issuedReport($beginDate, $endDate)
{
	$query =
		'select
			op.id as "ID PROTOCOLO",
			op.surgical_order_id as "ID IQ",
			pat.ficha as "FICHA DEL PACIENTE",
			case locate(" - ", opd.postintervention) when 0 then null else substring_index(opd.postintervention, " - ", 1) end as "CODIGO INTERVENCIÓN FONASA",
			case locate(" - ", opd.postdiagnosis) when 0 then null else substring_index(opd.postdiagnosis, " - ", 1) end as "CODIGO CIE10",
			substring_index(opd.postdiagnosis, " - ", -1) as "GLOSA IQ",
			substring_index(pat.rut, "-", 1) as "RUT DEL PACIENTE",
			case locate("-", pat.rut) when 0 then null else substring_index(pat.rut, "-", -1) end as "DV",
			upper(concat_ws(" ", substring_index(trim(pat.primer_nombre), " ", 1), substring_index(trim(pat.segundo_nombre), " ", -1))) as "NOMBRE PACIENTE",
			upper(trim(pat.apellido_paterno)) as "APELLIDO PATERNO",
			upper(trim(pat.apellido_materno)) as "APELLIDO MATERNO",
			pat.fecha_nacimiento as "FECHA NACIMIENTO",
			case pat.sexo when "M" then "1" when "F" then "2" else null end as "SEXO",
			date_format(op.surgery_date, "%Y-%m-%d") as "FECHA IQ",
			spc.nombre as "ESPECIALIDAD",
			upper(concat_ws(" ", trim(sur.nombre), trim(sur.apellido_paterno), trim(sur.apellido_materno))) as "NOMBRE CIRUJANO",
			case
				when ant.id is not null then upper(concat_ws(" ", trim(ant.nombre), trim(ant.apellido_paterno), trim(ant.apellido_materno)))
				else upper(trim(cus.name))
			end as "NOMBRE ANESTESISTA",
			opd.surgical_time "TIEMPO REAL IQ",
			"LEQ" as "ORIGEN",
			str.name as "REINTERVENCIÓN"
		from gh_operative_protocol as op
		inner join gh_operative_protocol_detail as opd
			on op.id = opd.operative_protocol_id
		inner join ordenquirurgica as sor
			on op.surgical_order_id = sor.id
		inner join paciente as pat
			on sor.paciente_id = pat.id
		left join usuario as sur
			on op.surgeon_id = sur.id
		inner join especialidad as spc
			on sor.especialidad_id = spc.id
		left join (gh_operative_protocol_medical_team as otm, usuario as ant)
			on (op.id = otm.operative_protocol_id and otm.anaesthetist_id = ant.id)
		left join (gh_operative_protocol_custom_staff as ocs, gh_custom_user as cus)
			on (op.id = ocs.operative_protocol_id
				and ocs.medical_title_id = 3
				and ocs.custom_user_uid = cus.uid)
		inner join gh_surgery_try as str
			on opd.surgery_try_id = str.id
		where op.surgery_date between %s0 and %s1';

	return DB::$gen->query($query, $beginDate, $endDate);
}

/**
 * @param string $beginDate
 * @param string $endDate
 */
function suspendReport($beginDate, $endDate)
{
	$query = 'select
			sor.id as "ID IQ",
			pat.ficha as "FICHA DEL PACIENTE",
			case locate(" - ", sds.intervention) when 0 then null else substring_index(sds.intervention, " - ", 1) end as "CODIGO INTERVENCIÓN FONASA",
			case locate(" - ", sds.diagnosis) when 0 then null else substring_index(sds.diagnosis, " - ", 1) end as "CODIGO CIE10",
			substring_index(sds.diagnosis, " - ", -1) as "GLOSA IQ",
			substring_index(pat.rut, "-", 1) as "RUT DEL PACIENTE",
			case locate("-", pat.rut) when 0 then null else substring_index(pat.rut, "-", -1) end as "DV",
			upper(concat_ws(" ", substring_index(trim(pat.primer_nombre), " ", 1), substring_index(trim(pat.segundo_nombre), " ", -1))) as "NOMBRE PACIENTE",
			upper(trim(pat.apellido_paterno)) as "APELLIDO PATERNO",
			upper(trim(pat.apellido_materno)) as "APELLIDO MATERNO",
			spc.nombre as "ESPECIALIDAD",
			date(act.updated_at) as "FECHA SUSPENSION",
			act.observaciones as "CAUSAL SUSPENSIÓN",
			upper(concat_ws(" ", trim(sur.nombre), trim(sur.apellido_paterno), trim(sur.apellido_materno))) as "NOMBRE CIRUJANO",
			upper(concat_ws(" ", trim(ant.nombre), trim(ant.apellido_paterno), trim(ant.apellido_materno))) as "NOMBRE ANESTESISTA",
			"LEQ" as "ORIGEN"
		from ordenquirurgica as sor
		inner join gh_surgical_order_diagnosis_summary as sds
			on sor.id = sds.surgical_order_id
		inner join paciente as pat
			on sor.paciente_id = pat.id
		left join usuario as sur
			on sor.cirujano_id = sur.id
		inner join especialidad as spc
			on sor.especialidad_id = spc.id
		left join (oqequipomedico as smt, usuario as ant)
			on (sor.id = smt.oq_id and smt.user_id = ant.id and ant.esanestesista)
		inner join actividad as act
			on sor.id = act.oq_id
			and act.actividad = "Orden Suspendida"
		where act.updated_at between %s0 and %s1
		order by sor.id';

	return DB::$gen->query($query, $beginDate, $endDate);
}

/**
 * @param string $beginDate
 * @param string $endDate
 */
function pendingsReport($beginDate, $endDate)
{
	$query =
		'select
			sor.id as "ID IQ",
			case sor.estado_id when 14 then 1 else 0 end as "CONDICIONAL",
			pat.ficha as "FICHA DEL PACIENTE",
			case locate(" - ", sds.intervention) when 0 then null else substring_index(sds.intervention, " - ", 1) end as "CODIGO INTERVENCIÓN FONASA",
			case locate(" - ", sds.diagnosis) when 0 then null else substring_index(sds.diagnosis, " - ", 1) end as "CODIGO CIE10",
			substring_index(sds.diagnosis, " - ", -1) as "GLOSA IQ",
			substring_index(pat.rut, "-", 1) as "RUT DEL PACIENTE",
			case locate("-", pat.rut) when 0 then null else substring_index(pat.rut, "-", -1) end as "DV",
			upper(concat_ws(" ", substring_index(trim(pat.primer_nombre), " ", 1), substring_index(trim(pat.segundo_nombre), " ", -1))) as "NOMBRE PACIENTE",
			upper(trim(pat.apellido_paterno)) as "APELLIDO PATERNO",
			upper(trim(pat.apellido_materno)) as "APELLIDO MATERNO",
			spc.nombre as "ESPECIALIDAD",
			upper(concat_ws(" ", trim(sur.nombre), trim(sur.apellido_paterno), trim(sur.apellido_materno))) as "NOMBRE CIRUJANO",
			upper(concat_ws(" ", trim(ant.nombre), trim(ant.apellido_paterno), trim(ant.apellido_materno))) as "NOMBRE ANESTESISTA",
			"LEQ" as "ORIGEN"
		from ordenquirurgica as sor
		inner join gh_surgical_order_diagnosis_summary as sds
			on sor.id = sds.surgical_order_id
		inner join paciente as pat
			on sor.paciente_id = pat.id
		left join usuario as sur
			on sor.cirujano_id = sur.id
		inner join especialidad as spc
			on sor.especialidad_id = spc.id
		left join oqcausasuspension as sca
			on sor.causa_suspension_id = sca.id
		left join (oqequipomedico as smt, usuario as ant)
			on (sor.id = smt.oq_id and smt.user_id = ant.id and ant.esanestesista)
		left join gh_operative_protocol as op
			on sor.id = op.surgical_order_id
		where sor.fecha_tabla between %s0 and %s1
			and op.id is null
			and sor.causa_suspension_id is null';

	return DB::$gen->query($query, $beginDate, $endDate);
}
