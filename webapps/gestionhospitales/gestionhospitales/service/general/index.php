<?php
/**
 * Servicios para gestion-hospitales
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\general
 */
namespace GH\general;

header('Access-Control-Allow-Origin: *');
require_once(__DIR__ . '/../config.php');
require_once('app.php');

\GH\dbConnect();

$ghApp = \GH\getNewApp();
\Slim\Route::setDefaultConditions(array(
	'opId'		=> '[0-9]+',
	'soId'		=> '[0-9]+'
));

require_once('operative_protocol.php');
$ghApp->get(
	'/operative_protocol/issued',
	'\GH\operativeProtocol\issuedList'
);

$ghApp->get(
	'/operative_protocol/pendings',
	'\GH\operativeProtocol\pendingList'
);

$ghApp->get(
	'/operative_protocol/:soId/create',
	'\GH\operativeProtocol\createForm'
);

$ghApp->post(
	'/operative_protocol',
	'\GH\operativeProtocol\create'
);

$ghApp->get(
	'/operative_protocol/:opId/validate',
	'\GH\operativeProtocol\validateForm'
);

$ghApp->put(
	'/operative_protocol/:opId/validate',
	'\GH\operativeProtocol\validate'
);

$ghApp->get(
	'/operative_protocol/:opId/remove',
	'\GH\operativeProtocol\removeForm'
);

$ghApp->delete(
	'/operative_protocol/:opId',
	'\GH\operativeProtocol\remove'
);

$ghApp->get(
	'/operative_protocol/:opId',
	'\GH\operativeProtocol\view'
);

require_once('surgical_order.php');
$ghApp->get(
	'/surgical_order/:soId/suspend',
	'\GH\surgicalOrder\suspendForm'
);

$ghApp->put(
	'/surgical_order/:soId/suspend',
	'\GH\surgicalOrder\suspend'
);

$ghApp->run();
