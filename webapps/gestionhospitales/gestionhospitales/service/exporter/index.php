<?php
/**
 * Servicios para gestion-hospitales
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @link		www.pertikos.cl
 * @version		1.0
 * @package		GH\exporter
 */
namespace GH\exporter;

header('Access-Control-Allow-Origin: *');
require_once(__DIR__ . '/../config.php');
require_once('app.php');

\GH\dbConnect();

$ghApp = \GH\getNewApp();
\Slim\Route::setDefaultConditions(array(
	'opId'		=> '[0-9]+',
	'soId'		=> '[0-9]+'
));

require_once('emergency.php');
$ghApp->get(
	'/emergency/dau/category-stats',
	'\GH\exporter\emergency\categoryStats'
);

$ghApp->get(
	'/emergency/dau/stats',
	'\GH\exporter\emergency\dauStats'
);

require_once('operative_protocol.php');
$ghApp->get(
	'/:opId/operative_protocol',
	'\GH\exporter\operativeProtocol\document'
);

$ghApp->get(
	'/operative_protocol/issued',
	'\GH\exporter\operativeProtocol\issued'
);

$ghApp->get(
	'/operative_protocol/suspend',
	'\GH\exporter\operativeProtocol\suspend'
);

$ghApp->get(
	'/operative_protocol/pendings',
	'\GH\exporter\operativeProtocol\pendings'
);

$ghApp->run();
