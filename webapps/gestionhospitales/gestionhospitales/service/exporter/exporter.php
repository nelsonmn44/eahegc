<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\exporter
 */
namespace GH\exporter;

/**
 * @param array.<string, mixed> $rowList
 * @param string|NULL $filename
 */
function sendCsv($rowList, $filename = 'export.csv')
{
	$headers = array();
	if (0 < count($rowList))
	{
		$i = 0;
		foreach ($rowList[0] as $column => $value)
		{
			$headers[$i] = $column;
			++$i;
		}
	}

	$output = fopen('php://output', 'w');
	if ($output)
	{
		header('Content-Type: text/csv; charset=utf-8');
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header('Pragma: no-cache');
		header('Expires: 0');

		fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($output, $headers, ';');
		for ($i = 0; count($rowList) > $i; ++$i)
			fputcsv($output, array_values($rowList[$i]), ';');

		fclose($output);
	}
}
