<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\exporter\operativeProtocol
 */
namespace GH\exporter\operativeProtocol;

require_once('exporter.php');
require_once('general/messages.php');
require_once('general/operative_protocol.php');
require_once('general/patient.php');
require_once('general/surgical_order.php');
require_once('util.php');

use GH\exporter;
use GH\messages\Error;
use GH\util;
use mikehaertl\wkhtmlto\Pdf;

/**
 * @api
 * @param integer $id
 */
function document($id)
{
	$op = \GH\operativeProtocol\report($id);
	if (is_null($op))
	{
		\GH\respond(array('warning' => array("No existe el protocolo operatorio asociado al ID '$id'")));
		exit;
	}

	$patient		= \GH\patient\report($op['patientId']);
	$pathologyList	= array();
	{
		$rowList = \GH\surgicalOrder\getPathology($op['surgicalOrderId']);
		for ($i = 0, $size = count($rowList); $size > $i; ++$i)
			$pathologyList[$i] = $rowList[$i]['pathologyName'];
	}
	$staff			= \GH\operativeProtocol\getStaff($op['operativeProtocolId']);

	$app = \Slim\Slim::getInstance();
	$app->contentType('application/pdf; charset=utf-8');

	$tempFile = TEMP_PATH . uniqid() . '.html';
	file_put_contents($tempFile,
		str_replace(
			array
			(
				'{patientName}',
				'{patientFile}',
				'{patientNic}',
				'{patientInsurance}',
				'{patientAge}',
				'{patientGender}',
				'{source}',
				'{care}',
				'{preDiagnosis}',
				'{preIntervention}',
				'{postDiagnosis}',
				'{postIntervention}',
				'{pathologyList}',
				'{remark}',
				'{pavilion}',
				'{surgeon}',
				'{anaesthetist}',
				'{assistantList}',
				'{surgicalNurse}',
				'{roomNurseList}',
				'{date}',
				'{start}',
				'{end}',
				'{duration}',
				'{tries}',
				'{wound}',
				'{skinprep}',
				'{anesthesia}',
				'{risk}',
				'{antiseptic}',
				'{biopsy}',
				'{culture}',
				'{imaging}',
				'{antibiotic}',
				'{description}'
			),
			array
			(
				util\nonEmpty($patient['fullName']),
				util\nonEmpty($patient['file']),
				util\nonEmpty($patient['nic']),
				util\nonEmpty($patient['insurance']),
				util\nonEmpty($patient['age']),
				util\nonEmpty($patient['gender']),
				util\nonEmpty($op['specialityName']),
				util\nonEmpty($op['careTypeName']),
				util\nonEmpty($op['prediagnosis']),
				util\nonEmpty($op['preintervention']),
				util\nonEmpty($op['postdiagnosis']),
				util\nonEmpty($op['postintervention']),
				util\nonEmpty(call_user_func_array('GH\util\concatWs', $pathologyList)),
				util\nonEmpty($op['postremark']),
				util\nonEmpty($op['pavilionName']),
				util\nonEmpty($staff['surgeonName']),
				util\nonEmpty($staff['anaesthetistName']),
				util\nonEmpty(\GH\util\concatWs($staff['firstAssistantName'], $staff['secondAssistantName'], $staff['thirdAssistantName'])),
				util\nonEmpty($staff['surgicalNurseName']),
				util\nonEmpty(\GH\util\concatWs($staff['firstRoomNurseName'], $staff['secondRoomNurseName'])),
				util\nonEmpty($op['surgeryDate']),
				util\nonEmpty($op['surgeryStart']),
				util\nonEmpty($op['surgeryEnd']),
				util\nonEmpty($op['surgicalTime']),
				util\nonEmpty($op['surgeryTryName']),
				util\nonEmpty($op['woundTypeName']),
				util\nonEmpty($op['skinprep']),
				util\nonEmpty($op['anesthesiaTypeName']),
				util\nonEmpty($op['surgeryRiskName']),
				util\nonEmpty($op['antisepticName']),
				util\nonEmpty($op['biopsy']),
				util\nonEmpty($op['culture']),
				util\nonEmpty($op['imaging']),
				util\nonEmpty($op['antibiotic']),
				util\nonEmpty($op['surgicalDescription'])
			),
			file_get_contents(RESOURCE_PATH . 'template/operative_protocol-body.html')
		),
		LOCK_EX
	);

	$options = array
	(
//		'binary'			=> 'C:/wkhtmltopdf/bin/wkhtmltopdf.exe', //Binario de Windows
		'disable-smart-shrinking',
		'header-html'		=> RESOURCE_PATH . 'template/operative_protocol-header.html',
		'header-spacing'	=> 3,
		'footer-html'		=> RESOURCE_PATH . 'template/operative_protocol-footer.html',
		'footer-spacing'	=> 1,
		'margin-bottom'		=> 12,
		'margin-left'		=> 5,
		'margin-right'		=> 5,
		'margin-top'		=> 29,
		'page-size'			=> 'Letter',
		'replace' 			=> array
		(
			'opNumber'		=> $id,
			'currentDate'	=> date('Y-m-d H:i:s'),
		),
//		'zoom'				=> 1.34, //Factor de corrección para Windows
		'tmpDir'			=> TEMP_PATH
	);

	$pdf = new Pdf($options);
	$pdf->addPage($tempFile);

	header('Content-Type: application/pdf; charset=utf-8');

	if (!$pdf->send())
	{
		unlink($tempFile);
		throw new \Exception('Error al crear el archivo PDF: ' . $pdf->getError());
	}
	else
		unlink($tempFile);
}

/**
 * @api
 */
function issued()
{
	$params = \GH\getParams();
	if (util\isVoidKey($params, 'begin'))
		\GH\halt(404, Error::$OP_STATS_DATE_NOT_FOUND);
	else
		$params['begin'] = date('Y-m-d', strtotime($params['begin']));

	if (util\isVoidKey($params, 'end'))
		$params['end'] = date('Y-m-d');
	else
		$params['end'] = date('Y-m-d', strtotime($params['end']));

	$rowList = \GH\operativeProtocol\issuedReport(
		$params['begin'] . '  00:00:00',
		$params['end'] . '  23:59:59'
	);

	exporter\sendCsv(
		$rowList,
		'protocolos_emitidos_' . $params['begin'] . '_' . $params['end'] . '.csv'
	);
}

/**
 * @api
 */
function suspend()
{
	$params = \GH\getParams();
	if (util\isVoidKey($params, 'begin'))
		\GH\halt(404, Error::$OP_STATS_DATE_NOT_FOUND);
	else
		$params['begin'] = date('Y-m-d', strtotime($params['begin']));

	if (util\isVoidKey($params, 'end'))
		$params['end'] = date('Y-m-d');
	else
		$params['end'] = date('Y-m-d', strtotime($params['end']));

	$rowList = \GH\operativeProtocol\suspendReport(
		$params['begin'] . '  00:00:00',
		$params['end'] . '  23:59:59'
	);

	exporter\sendCsv(
		$rowList,
		'intervenciones_suspendidas_' . $params['begin'] . '_' . $params['end'] . '.csv'
	);
}

/**
 * @api
 */
function pendings()
{
	$params = \GH\getParams();
	if (util\isVoidKey($params, 'begin'))
		\GH\halt(404, Error::$OP_STATS_DATE_NOT_FOUND);
	else
		$params['begin'] = date('Y-m-d', strtotime($params['begin']));

	if (util\isVoidKey($params, 'end'))
		$params['end'] = date('Y-m-d');
	else
		$params['end'] = date('Y-m-d', strtotime($params['end']));

	$rowList = \GH\operativeProtocol\pendingsReport(
		$params['begin'] . '  00:00:00',
		$params['end'] . '  23:59:59'
	);

	exporter\sendCsv(
		$rowList,
		'pendientes_protocolo_' . $params['begin'] . '_' . $params['end'] . '.csv'
	);
}
