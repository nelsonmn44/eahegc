<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		GH\exporter\emergency
 */
namespace GH\exporter\emergency;

require_once('emergency/dau.php');
require_once('emergency/messages.php');
require_once('exporter.php');
require_once('util.php');

use GH\exporter;
use GH\emergency\dau;
use GH\emergency\messages\Error;
use GH\util;

/**
 * @api
 */
function categoryStats()
{
	$params = \GH\getParams();
	if (util\isVoidKey($params, 'begin'))
		\GH\halt(404, Error::$DAU_STATS_DATE_NOT_FOUND);
	else
		$params['begin'] = date('Y-m-d', strtotime($params['begin']));

	if (util\isVoidKey($params, 'end'))
		$params['end'] = date('Y-m-d');
	else
		$params['end'] = date('Y-m-d', strtotime($params['end']));

	$rowList = dau\categoryStats(
		$params['begin']	. ' 00:00:00',
		$params['end']		. ' 23:59:59'
	);

	exporter\sendCsv(
		$rowList,
		'dau-category-stats_' . $params['begin'] . '_' . $params['end'] . '.csv'
	);
}

/**
 * @api
 */
function dauStats()
{
	$params = \GH\getParams();
	if (util\isVoidKey($params, 'begin'))
		\GH\halt(404, Error::$DAU_STATS_DATE_NOT_FOUND);
	else
		$params['begin'] = date('Y-m-d', strtotime($params['begin']));

	if (util\isVoidKey($params, 'end'))
		$params['end'] = date('Y-m-d');
	else
		$params['end'] = date('Y-m-d', strtotime($params['end']));

	$rowList = dau\fullStats(
		$params['begin']	. ' 00:00:00',
		$params['end']		. ' 23:59:59'
	);

	exporter\sendCsv(
		$rowList,
		'dau-stats_' . $params['begin'] . '_' . $params['end'] . '.csv'
	);
}
