<?php
/**
 *
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @version		1.0
 * @package		-
 */
namespace GH;

function errorHandler($errno, $errstr, $errfile, $errline)
{
	respond(array
		(
			'elements' => array
			(
				array
				(
					'id'			=> $errno,
					'description'	=> $errstr
				)
			)
		),
		500
	);

	return true;
}
set_error_handler('\GH\errorHandler', E_ALL);

require_once('meekrodb.php');
require_once('Slim/Slim.php');
require_once('util.php');

use Slim\Slim;
use GH\util;

abstract class DB
{
	static $gen = NULL;
	static $emy = NULL;
}

/**
 * @param array.<string, string|integer>
 */
function dbConnect()
{
	\DB::$error_handler				= false;
	\DB::$nested_transactions		= false;
	\DB::$throw_exception_on_error	= true;

	global $dbConfigGen, $dbConfigEmy;

	DB::$gen = new \MeekroDB(
		$dbConfigGen['host'],
		$dbConfigGen['user'],
		$dbConfigGen['password'],
		$dbConfigGen['dbName'],
		$dbConfigGen['port'],
		$dbConfigGen['encoding']
	);

	DB::$emy = new \MeekroDB(
		$dbConfigEmy['host'],
		$dbConfigEmy['user'],
		$dbConfigEmy['password'],
		$dbConfigEmy['dbName'],
		$dbConfigEmy['port'],
		$dbConfigEmy['encoding']
	);
}

Slim::registerAutoloader();

/**
 * @return array.<mixed>
 */
function getParams()
{
	return Slim::getInstance()->request()->get();
}

/**
 * @param integer $statusCode
 * @param array.<mixed> ...$errorList
 */
function halt($statusCode)
{
	$args = func_get_args();
	Slim::getInstance()->halt(
		array_shift($args), //obtiene y saca del arreglo params el código de estado
		json_encode(
			array
			(
				'elements' => $args
			),
			JSON_NUMERIC_CHECK
		)
	);
}

/**
 * @return array.<mixed>
 */
function receive()
{
	$body = json_decode(Slim::getInstance()->request()->getBody(), true);
	return isset($body) ? $body : array();
}

/**
 * @param array.<mixed> $output
 * @param integer $statusCode
 */
function respond($output, $statusCode = 200)
{
	$app = Slim::getInstance();
	$app->response->setStatus($statusCode);
	echo util\normalize(json_encode($output, JSON_NUMERIC_CHECK));

	//No funciona en la versión actual del servidor del hospital
	//echo json_encode($output,
	//	JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
	//);
}

/**
 * @param array.<mixed> $data
 */
function sendData($data)
{
	respond(array('data' => $data));
}

function getNewApp()
{
	global $appConfig;
	$app = new Slim($appConfig);

	$app->contentType('application/json; charset=utf-8');
	$app->setName('gh');
	$app->error(function (\Exception $exception) use ($app)
	{
		respond(array('error' => array(
			$exception->getMessage()
		)));
	});

	return $app;
}

/**
 * @param array.<mixed> $message
 * @param string|integer ...$replaces
 * @return array.<mixed>
 */
function bloat($message)
{
	$args = func_get_args();
	$args[0] = $message['description'];
	$message['description'] = call_user_func_array('sprintf', $args);

	return $message;
}
