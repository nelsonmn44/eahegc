<?php
/**
 * Archivo de configuración global
 *
 * @author		Alvaro Fuentes <alvaro.fuentes.zurita@gmail.com>
 * @link		www.pertikos.cl
 * @version		1.0
 * @package		-
 */

/**
 * Espacio de nombres global para Gestión Hospitales
 */
namespace GH;

/**
 * Añade una ruta de inclusión adicional en formato canónico a la lista de rutas donde se buscarán
 * los archivos externos requeridos
 *
 * @param string $path
 */
function addRealIncludePath($path)
{
	set_include_path(get_include_path() . PATH_SEPARATOR . realpath($path));
}

// Agrega ruta hacia las librerías externas para poder requerirlas sin necesidad de especificarla
// en cada inclusión
//addRealIncludePath($_SERVER['DOCUMENT_ROOT'] . '/../lib');
addRealIncludePath(__DIR__ );
addRealIncludePath(__DIR__ . '/lib');

/**
 * @const number
 */
define('PAGE_LENGTH', 30);

/**
 * @const number
 */
define('PAGE_SIZE_MAX', 80);

/**
 * @const string
 */
define('BASE_PATH', $_SERVER['DOCUMENT_ROOT'] . '/gestionhospitales/');

/**
 * @const string
 */
define('SERVICE_PATH', BASE_PATH . 'service/');

/**
 * @const string
 */
define('LIB_PATH', SERVICE_PATH . 'lib/');

/**
 * @const string
 */
define('RESOURCE_PATH', LIB_PATH . 'asset/');

/**
 * @const string
 */
define('TEMP_PATH', RESOURCE_PATH . 'temp/');

/**
 * @var array<string>
 */
$dbConfigGen	= array
(
	'user'		=> 'gpadmin',
	'password'	=> 'gpadmin123',
	'dbName'	=> 'gestionhospitales',
	'host'		=> 'localhost',
	'port'		=> '3306',
	'encoding'	=> 'utf8'
);

/**
 * @var array<string>
 */
$dbConfigEmy 		= array
(
	'user'		=> 'gpadmin',
	'password'	=> 'gpadmin123',
	'dbName'	=> 'urgencia',
	'host'		=> 'localhost',
	'port'		=> '3306',
	'encoding'	=> 'utf8'
);

require_once('Slim/Log.php');
require_once('Slim/Slim.php');

/**
 * @var array<string>
 */
$appConfig = array
(
	'cookies.cipher'		=> MCRYPT_RIJNDAEL_256,
	'cookies.cipher_mode'	=> MCRYPT_MODE_CBC,
	'cookies.httponly'		=> false,
	'cookies.lifetime'		=> '20 minutes',
	'cookies.path'			=> '/',
	'cookies.secret_key'	=> 'asd+as+d-34534+++f423rf3$%&/#$%#$%#$%2f d´rtergbv',
	'cookies.secure'		=> false,
	'debug'					=> true,
	'http.version'			=> '1.1',
	'log.level'				=> \Slim\Log::DEBUG,
	'log.enabled'			=> true,
	'mode'					=> 'development'
);
