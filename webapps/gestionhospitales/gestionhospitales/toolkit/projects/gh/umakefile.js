var ROOT		= '/srv/http/webapps/gestionhospitales/gestionhospitales/';
var TLK			= ROOT + 'toolkit/';
var LIB			= TLK + 'library/';
var EXT			= LIB + 'externs/';
var OUT			= ROOT;

var GH			= TLK + 'projects/gh/';
var GH_CTX		= 'gh/';
var GH_RES		= GH + 'assets/';
var GH_SRC		= GH + 'source/' + GH_CTX;

var GH_OUT		= OUT + GH_CTX;
var GH_DPL		= GH_OUT + GH_CTX;
var GH_DPL_CTN	= GH_DPL + 'content/';
var GH_DPL_RES	= GH_DPL + 'assets/';

/* ========================================================================== */

target([GH_DPL, GH_DPL_CTN, GH_DPL_RES], null, dirMake());

/* ========================================================================== */

var partials = enlist(
	LIB + 'fz/element/**/*.html',
	GH_SRC + '*/**/*.html'
);

target(
	toPath(partials, GH_DPL_RES),
	partials,
	fileCopy()
);

target(
	GH_OUT + 'index.html',
	GH + 'source/index.html',
	fileCopy()
);

task('copy-partials',
[
	GH_DPL_RES,
	toPath(partials, GH_DPL_RES),
	GH_OUT + 'index.html'
]);

/* ========================================================================== */

var resources = enlist(GH_RES + '*');

target(
	toPath(resources, GH_DPL_RES),
	resources,
	fileCopy()
);

task('copy-resources',
[
	GH_DPL_RES,
	toPath(resources, GH_DPL_RES)
]);

/* ========================================================================== */

var styles = enlist(GH_SRC + '**/*.css');

target(
	toPath(styles, GH_DPL_RES),
	styles,
	fileCopy()
);

task('build-style',
[
	GH_DPL_RES,
	toPath(styles, GH_DPL_RES)
]);

/* ========================================================================== */

target(
	GH_DPL + 'app.js',
	[
		LIB + 'angular/angular.js',
		LIB + 'angular/resource.js',
		LIB + 'angular/ui-router.js',
		LIB + 'angular/ui-bootstrap.js',
		LIB + 'angular/locale_es-cl.js',
		LIB + 'fz/**/*.js',
		GH_SRC + '**/*.js'
	],
	function(target, source)
	{
		var cmd = 'closure' +
			' --angular_pass true' +
			' --compilation_level SIMPLE_OPTIMIZATIONS' +
			' --closure_entry_point gh.login' +
			' --create_source_map ' + target + '.map' +
			' --externs ' + EXT + 'angular-1.3.js' +
			' --externs ' + EXT + 'angular-1.3-q_templated.js' +
			' --externs ' + EXT + 'angular-1.3-http-promise_templated.js' +
			' --externs ' + EXT + 'angular_ui_router.js' +
			' --extra_annotation_name abstract' +
			' --extra_annotation_name animations' +
			' --extra_annotation_name element' +
			' --extra_annotation_name eventOf' +
			' --extra_annotation_name eventType' +
			' --extra_annotation_name import' +
			' --extra_annotation_name ngdoc' +
			' --extra_annotation_name methodOf' +
			' --extra_annotation_name priority' +
			' --extra_annotation_name restrict' +
			' --extra_annotation_name scope' +
			' --extra_annotation_name todo' +
			' --extra_annotation_name usage' +
			' --js_output_file ' + target +
			' --language_in ECMASCRIPT5_STRICT' +
     		' --source_map_format V3' +
     		' --source_map_location_mapping "' + ROOT + '|/"' +
			' --use_types_for_optimization' +
			' --warning_level VERBOSE';

		for (var i = 0; source.length > i; ++i)
			cmd += ' --js ' + source[i];

		var code = shell.exec(cmd).code;
		shell
			.echo('//# sourceMappingURL=' + _path.basename(target) + '.map')
			.toEnd(target);

		return code;
	}
);

task('build-client', [GH_DPL, GH_DPL + 'app.js']);

/* ========================================================================== */

task('clear-all', null, function(changed)
{
	shell.rm('-rf', GH_OUT + '*');
});

/* ========================================================================== */

task('debug',
[
	'copy-partials',
	'copy-resources',
	'build-style',
	'build-client'
]);
