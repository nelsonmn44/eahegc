goog.require('fz.config');
goog.require('gh.server');

goog.provide('gh.server.operativeProtocol');

/**
 * @typedef {{
 *		pendingList		: function((!gh.server.param.Search|function(!gh.server.Input)), function(!gh.server.Input)=),
 *		issuedList		: function((!gh.server.param.Search|function(!gh.server.Input)), function(!gh.server.Input)=),
 *		createForm		: function(!gh.server.param.Id, function(!gh.server.Input)),
 *		create			: function(!Object, function(!gh.server.Input)=),
 *		removeForm		: function(!gh.server.param.Id, function(!gh.server.Input)),
 *		remove			: function(!gh.server.param.Id),
 *		validateForm	: function(!gh.server.param.Id, function(!gh.server.Input)),
 *		validate		: function(!Object, function(!gh.server.Input)=),
 *		get				: function(!gh.server.param.Id, function(!gh.server.Input))
 *	}}
 */
gh.server.operativeProtocol.Resource;

/**
 * @ngInject
 * @constructor
 * @param {!angular.$resource} $resource
 * @param {!fz.config.Config} fzConfig
 * @return {!gh.server.operativeProtocol.Resource}
 */
gh.server.operativeProtocol.OperativeProtocol = function ($resource, fzConfig)
{
	return /** @type {!gh.server.operativeProtocol.Resource} */ ($resource(
		fzConfig.get('base.service.url') + '/general/operative_protocol/:listController:id/:docController',
		{
			id				: '@id',
			listController	: '@listController',
			docController	: '@docController'
		},
		{
			pendingList :
			{
				method	: 'GET',
				params	:
				{
					listController : 'pendings'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			issuedList :
			{
				method	: 'GET',
				params	:
				{
					listController : 'issued'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			createForm :
			{
				method	: 'GET',
				params	:
				{
					docController : 'create'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			create :
			{
				method	: 'POST',
				interceptor : fzConfig.get('server.interceptor')
			},
			removeForm :
			{
				method	: 'GET',
				params	:
				{
					docController : 'remove'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			remove :
			{
				method	: 'DELETE',
				interceptor : fzConfig.get('server.interceptor')
			},
			validateForm :
			{
				method	: 'GET',
				params	:
				{
					docController : 'validate'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			validate :
			{
				method	: 'PUT',
				params	:
				{
					docController : 'validate'
				},
				interceptor : fzConfig.get('server.interceptor')
			}
		}
	));
}

/**
 * @type {!angular.Module}
 */
gh.server.operativeProtocol.module = angular.module(
	'gh.server.operativeProtocol',
	[
		'ngResource',
		fz.config.module.name,
		gh.server.module.name
	]
);

gh.server.operativeProtocol.module.service(
	'ghOperativeProtocol', gh.server.operativeProtocol.OperativeProtocol
);
