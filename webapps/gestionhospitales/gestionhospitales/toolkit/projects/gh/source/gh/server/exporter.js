goog.require('fz.config');
goog.require('fz.util');

goog.provide('gh.server.exporter');

/**
 * @typedef {{
 *		document	: function(string, number),
 *		collection	: function(string, Object.<string, *>=)
 *	}}
 */
gh.server.exporter.Resource;

/**
 * @ngInject
 * @constructor
 * @param {!fz.config.Config} fzConfig
 * @param {!angular.$window} $window
 * @return {!gh.server.exporter.Resource}
 */
gh.server.exporter.Exporter = function(fzConfig, $window)
{
	return /** @type {!gh.server.exporter.Resource} */ (
	{
		document : function(controller, id)
		{
			if (!controller || !id)
				console.error('Es necesario especificar el controlador y el id para exportar un documento');
			else
				$window.location.href = fzConfig.get('base.service.url') + '/exporter/' + id + '/' + controller;
		},
		collection : function(controller, params)
		{
			var query = fz.util.toQueryString(params);
			if (!query)
				query = '';

			if (!controller)
				console.error('Es necesario especificar el controlador para exportar una colección');
			else
				$window.location.href = fzConfig.get('base.service.url') + '/exporter/' + controller + query;
		}
	});
}

/**
 * @type {!angular.Module}
 */
gh.server.exporter.module = angular.module('gh.server.exporter',
[
	fz.config.module.name
]);

gh.server.exporter.module.service(
	'ghExporter', gh.server.exporter.Exporter
);
