goog.require('fz');
goog.require('fz.config');
goog.require('fz.node');

goog.provide('gh.site.home');

/**
 * @const
 * @type {string}
 */
gh.site.home.NAME = 'home';

/**
 * @type {!angular.Module}
 */
gh.site.home.module = angular.module(
	gh.site.home.NAME,
	[
		fz.config.module.name,
		fz.node.module.name
	]
);

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!angular.$injector} $injector
 * @param {!angular.Scope} $scope
 */
gh.site.home.Controller = function(fzNode, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config : fzNode.get(gh.site.home.NAME),
		traits : fz.site.Trait.FRAME,
		$scope : $scope
	});

	this.initMenu();
}
fz.inherit(gh.site.home.Controller, fz.site.Controller);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.home.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.home.NAME,
			state	:
			{
				controller	: gh.site.home.Controller,
				parent		: gh.site.auth.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-frame-default.html',
				url			: '/home'
			},
			redirectTo	: 'home.menu'
		}),
		fzConfig.getNode(gh.site.home.NAME)
	);
}
gh.site.home.module.config(gh.site.home.module._config);
