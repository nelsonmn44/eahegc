goog.require('fz');
goog.require('fz.config');
goog.require('fz.node');
goog.require('gh.server');
goog.require('gh.server.operativeProtocol');
goog.require('gh.site.operativeProtocol');

goog.provide('gh.site.operativeProtocol.reports');

/**
 * @const
 * @private
 * @type {string}
 */
gh.site.operativeProtocol.reports.NAME = 'operativeProtocol.reports';

/**
 * @typedef {{
 *		onExport	: function(string),
 *		issued		: !{begin : (!Date|undefined), end : (!Date|undefined)},
 *		suspend		: !{begin : (!Date|undefined), end : (!Date|undefined)},
 *		pendings	: !{begin : (!Date|undefined), end : (!Date|undefined)}
 * }}
 */
gh.site.operativeProtocol.reports.Scope;

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.exporter.Resource} ghExporter
 * @param {!angular.$injector} $injector
 * @param {!gh.site.operativeProtocol.reports.Scope} $scope
 * @param {!angular.$filter} $filter
 */
gh.site.operativeProtocol.reports.Controller = function(fzNode, ghExporter, $injector, $scope, $filter)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.operativeProtocol.reports.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	var now = new Date();

	$scope.issued =
	{
		range :
		{
			begin	: new Date(now.getFullYear(), now.getMonth(), 1),
			end		: new Date(now.getFullYear(), now.getMonth() + 1, 0)
		}
	};

	$scope.suspend =
	{
		range :
		{
			begin	: new Date(now.getFullYear(), now.getMonth(), 1),
			end		: new Date(now.getFullYear(), now.getMonth() + 1, 0)
		}
	};

	$scope.pendings =
	{
		range :
		{
			begin	: new Date(2014, 9, 15, 0, 0, 0),
			end		: new Date()
		}
	};

	$scope.onExport = function(target)
	{
		ghExporter.collection('operative_protocol/' + target,
		{
			begin	: $filter('date')($scope[target].range.begin, 'yyyy-MM-dd'),
			end		: $filter('date')($scope[target].range.end, 'yyyy-MM-dd')
		});
	}

	this.initMenu();
}
fz.inherit(gh.site.operativeProtocol.reports.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.operativeProtocol.reports.module = angular.module(
	gh.site.operativeProtocol.reports.NAME,
	[
		fz.config.module.name,
		fz.node.module.name,
		gh.server.operativeProtocol.module.name,
		gh.site.operativeProtocol.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.operativeProtocol.reports.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.operativeProtocol.reports.NAME,
			state	:
			{
				controller	: gh.site.operativeProtocol.reports.Controller,
				parent		: gh.site.operativeProtocol.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-op-reports.html',
				url			: '/reports'
			}
		}),
		fzConfig.getNode(gh.site.operativeProtocol.reports.NAME)
	);
}
gh.site.operativeProtocol.reports.module.config(gh.site.operativeProtocol.reports.module._config);
