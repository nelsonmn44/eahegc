goog.require('fz.config');
goog.require('gh.server');

goog.provide('gh.server.emergency');

/**
 * @typedef {{
 *		dauCategorizer		: function(!gh.server.param.Id, function(!gh.server.Input)),
 *		categorizeDau		: function(!Object, function(!gh.server.Input)=),
 *		triageQueue			: function(function(!gh.server.Input)),
 *		cleanTriageQueue	: function(!Object, function(!gh.server.Input)=)
 *	}}
 */
gh.server.emergency.Resource;

/**
 * @ngInject
 * @constructor
 * @param {!angular.$resource} $resource
 * @param {!fz.config.Config} fzConfig
 * @return {!gh.server.emergency.Resource}
 */
gh.server.emergency.Emergency = function ($resource, fzConfig)
{
	return /** @type {!gh.server.emergency.Resource} */ ($resource(
		fzConfig.get('base.service.url') + '/emergency/:collection/:store/:id/:controller',
		{
			id				: '@id',
			collection		: '@collection',
			controller		: '@controller'
		},
		{
			dauCategorizer :
			{
				method	: 'GET',
				params	:
				{
					collection	: 'daus',
					controller	: 'categorize'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			categorizeDau :
			{
				method	: 'POST',
				params	:
				{
					collection	: 'daus',
					controller	: 'categorize'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			triageQueue :
			{
				method	: 'GET',
				params	:
				{
					collection	: 'daus',
					store		: 'triage_queue'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			cleanTriageQueue :
			{
				method	: 'POST',
				params	:
				{
					collection	: 'daus',
					store		: 'triage_queue',
					controller	: 'clean'
				},
				interceptor : fzConfig.get('server.interceptor')
			}
		}
	));
}

/**
 * @type {!angular.Module}
 */
gh.server.emergency.module = angular.module('gh.server.emergency',
[
	'ngResource',
	fz.config.module.name,
	gh.server.module.name
]);

gh.server.emergency.module.service(
	'ghEmergency', gh.server.emergency.Emergency
);
