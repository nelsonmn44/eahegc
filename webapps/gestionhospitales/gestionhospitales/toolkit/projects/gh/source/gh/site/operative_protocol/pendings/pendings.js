goog.require('fz');
goog.require('fz.config');
goog.require('fz.element.table');
goog.require('fz.node');
goog.require('fz.site.Action');
goog.require('fz.site.Controller');
goog.require('fz.site.Search');
goog.require('fz.site.Paging');
goog.require('gh.server');
goog.require('gh.server.operativeProtocol');
goog.require('gh.site.operativeProtocol');

goog.provide('gh.site.operativeProtocol.pendings');

/**
 * @const
 * @private
 * @type {string}
 */
gh.site.operativeProtocol.pendings.NAME = 'operativeProtocol.pendings';

/**
 * @type {!Object.<string, !Object>}
 */
gh.site.operativeProtocol.pendings.actionMap =
{
	cancel	: {},
	check	: {},
	fill	: {},
	suspend	: {}
};

/**
 * @typedef {{
 *		doCancel		: function(this:fz.element.table.Action, !fz.element.table.Row),
 *		doCheck			: function(this:fz.element.table.Action, !fz.element.table.Row),
 *		doFill			: function(this:fz.element.table.Action, !fz.element.table.Row),
 *		doSuspend		: function(this:fz.element.table.Action, !fz.element.table.Row),
 *		filterCancel	: function(!fz.element.table.Row, !fz.element.table.Action):(boolean|undefined),
 *		filterCheck		: function(!fz.element.table.Row, !fz.element.table.Action):(boolean|undefined),
 *		filterFill		: function(!fz.element.table.Row, !fz.element.table.Action):(boolean|undefined),
 *		filterSuspend	: function(!fz.element.table.Row, !fz.element.table.Action):(boolean|undefined),
 *		paging			: !fz.site.Paging,
 *		search			: !fz.site.Search,
 *		keyPressFilter	: function(*),
 *		columnList		: !Array.<!fz.element.table.Column>,
 *		rowList			: (Array.<!fz.element.table.Row>|undefined)
 * }}
 */
gh.site.operativeProtocol.pendings.Scope;

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.operativeProtocol.Resource} ghOperativeProtocol
 * @param {!angular.$injector} $injector
 * @param {!gh.site.operativeProtocol.pendings.Scope} $scope
 */
gh.site.operativeProtocol.pendings.Controller = function(fzNode, ghOperativeProtocol, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.operativeProtocol.pendings.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	$scope.doCancel = function(row)
	{
		self.fzNode.activate(
			'operativeProtocol.cancel',
			{id : row['operativeProtocolId']}
		);
	}

	$scope.doCheck = function(row)
	{
		self.fzNode.activate(
			'operativeProtocol.check',
			{id : row['operativeProtocolId']}
		);
	}

	$scope.doFill = function(row)
	{
		self.fzNode.activate(
			'operativeProtocol.fill',
			{id : row['surgicalOrderId']}
		);
	}

	$scope.doSuspend = function(row)
	{
		self.fzNode.activate(
			'surgicalOrder.suspend',
			{id : row['surgicalOrderId']}
		);
	}

	var action = new fz.site.Action(
		gh.site.operativeProtocol.pendings.actionMap, this.config.actionMap
	).filter($scope);

	$scope.filterCancel = function(row, action)
	{
		if (51 === row['statusId'])
			return true;
	}

	$scope.filterCheck = function(row, action)
	{
		if (51 === row['statusId'])
			return true;
	}

	$scope.filterFill = function(row, action)
	{
		if (1	=== row['statusId'] ||
			2	=== row['statusId'] ||
			3	=== row['statusId'] ||
			4	=== row['statusId'] ||
			5	=== row['statusId'] ||
			11	=== row['statusId'] ||
			13	=== row['statusId'] ||
			14	=== row['statusId'])
			return true;
	}

	$scope.filterSuspend = function(row, action)
	{
		if (2	=== row['statusId'] ||
			3	=== row['statusId'] ||
			4	=== row['statusId'] ||
			13	=== row['statusId'] ||
			14	=== row['statusId'])
			return true;
	}

	/**
	 * @private
	 */
	this._searchClean = function()
	{
		$scope.paging.update(0, 0);
		$scope.counter = fz.VOID;

		ghOperativeProtocol.pendingList(function(input)
		{
			$scope.counter =
			{
				total	: input['fields']['size']['value']
			}

			$scope.rowList = /** @type {!Array.<!fz.element.table.Row>} */ (input['itemList']);
			counter.offset = 0;
		});
	}

	/**
	 * @type {!gh.server.param.Search}
	 */
	var searchParam = /** @type {!gh.server.param.Search} */ (
	{
		count : 30
	});

	/**
	 * @param {?(string|number)=} model
	 * @private
	 */
	this._searchRun = function(model)
	{
		$scope.search.updated || $scope.paging.update(0, 0);

		searchParam.filter = /** @type {string} */ (model);
		searchParam.offset = searchParam.count * $scope.paging.index;

		$scope.counter = fz.VOID;
		ghOperativeProtocol.pendingList(searchParam, function(input)
		{
			$scope.rowList = /** @type {!Array.<!fz.element.table.Row>} */ (input['itemList']);
			$scope.paging.update($scope.rowList.length);

			$scope.counter =
			{
				start	: 1 + input['fields']['offset']['value'],
				end		: input['fields']['offset']['value'] + $scope.rowList.length,
				total	: input['fields']['size']['value']
			}

			counter.offset = searchParam.offset;
			self.$window.scrollTo(0, 0);
		});
	}

	$scope.search = new fz.site.Search(
		this._searchRun, this._searchClean
	);

	$scope.paging = new fz.site.Paging(
		searchParam.count,
		/** @type {function(number)} */ (angular.bind($scope.search, $scope.search.run)),
		/** @type {function(number)} */ (angular.bind($scope.search, $scope.search.run))
	);

	$scope.keyPressFilter = function(event)
	{
		if (fz.Key.ENTER === event.keyCode && this.search.model)
			this.search.run();
	}

	/**
	 * @type {function(this:fz.element.table.Column, *):string}
	 */
	var elapsedDaysStyle = function(cell)
	{
		switch (cell)
		{
			case 1	: return 'elapsedDays info';
			case 2	: return 'elapsedDays warn';
			default	: return 'elapsedDays alert';
		}
	}

	var counter;
	$scope.columnList = /** @type {!Array.<!fz.element.table.Column>} */ (
	[
		counter = {
			name	: 'counter',
			title	: 'Nº',
			type	: 'counter'
		},
		{
			name	: 'elapsedDays',
			title	: 'Plazo',
			style	: elapsedDaysStyle
		},
		{
			name	: 'programmedDate',
			title	: 'Fecha Programación'
		},
		{
			name	: 'patientName',
			title	: 'Paciente'
		},
		{
			name	: 'preDiagnosisList',
			title	: 'Diagnóstico e Intervenciones',
			type	: 'list'
		},
		{
			name	: 'specialityName',
			title	: 'Especialidad'
		},
		{
			name	: 'pavilionName',
			title	: 'Pabellón'
		},
		{
			name	: 'surgeonName',
			title	: 'Cirujano'
		},
		{
			name	: 'statusName',
			title	: 'Estado'
		},
		{
			name	: 'actions',
			title	: 'Acciones',
			type	: 'action',
			list	:
			[
				{
					name		: 'suspend',
					title		: action.get('suspend').title,
					callback	: $scope.doSuspend,
					filter		: $scope.filterSuspend
				},
				{
					name		: 'fill',
					title		: action.get('fill').title,
					callback	: $scope.doFill,
					filter		: $scope.filterFill
				},
				{
					name		: 'check',
					title		: action.get('check').title,
					callback	: $scope.doCheck,
					filter		: $scope.filterCheck
				},
				{
					name		: 'cancel',
					title		: action.get('cancel').title,
					callback	: $scope.doCancel,
					filter		: $scope.filterCancel
				}
			]
		}
	]);

	this._searchClean();
	this.initMenu();
}
fz.inherit(gh.site.operativeProtocol.pendings.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.operativeProtocol.pendings.module = angular.module(
	gh.site.operativeProtocol.pendings.NAME,
	[
		fz.config.module.name,
		fz.element.table.module.name,
		fz.node.module.name,
		gh.server.operativeProtocol.module.name,
		gh.site.operativeProtocol.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.operativeProtocol.pendings.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.operativeProtocol.pendings.NAME,
			state	:
			{
				controller	: gh.site.operativeProtocol.pendings.Controller,
				parent		: gh.site.operativeProtocol.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-op-pendings.html',
				url			: '/pendings'
			}
		}),
		fzConfig.getNode(gh.site.operativeProtocol.pendings.NAME)
	);
}
gh.site.operativeProtocol.pendings.module.config(gh.site.operativeProtocol.pendings.module._config);
