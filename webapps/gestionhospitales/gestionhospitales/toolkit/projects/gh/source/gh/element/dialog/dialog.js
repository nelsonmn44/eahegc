goog.require('fz.config');

goog.provide('gh.element.dialog');

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @return {!angular.Directive}
 */
gh.element.dialog.directive = function(fzConfig)
{
	var scope =
	{
		/**
		 * @expose
		 * @type {string}
		 */
		clazz		: fz.element.Attrib.CLASS,

		/**
		 * @expose
		 * @type {string}
		 */
		close		: '&fzOnClose',

		/**
		 * @expose
		 * @type {string}
		 */
		messageList	: '=fzMessages',

		/**
		 * @expose
		 * @type {string}
		 */
		title		: fz.element.Attrib.TITLE
	};

	return /** @type {!angular.Directive} */(
	{
		restrict	: 'AE',
		scope		: scope,
		templateUrl	: fzConfig.get('template.pool.url') + '/gh-dialog.html',
		link		: function(scope, element, attrs)
		{
			element.addClass('fz-dialog').addClass(attrs['fzClass']);
		}
	});
}

/**
 * @type {!angular.Module}
 */
gh.element.dialog.module = angular.module('gh.element.dialog',
[
	fz.config.module.name
]);

gh.element.dialog.module.directive(
	'ghDialog',
	gh.element.dialog.directive
);
