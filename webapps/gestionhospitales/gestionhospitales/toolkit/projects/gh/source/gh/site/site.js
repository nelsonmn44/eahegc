goog.require('gh.site.home');
goog.require('gh.site.home.menu');
goog.require('gh.site.operativeProtocol.check');
goog.require('gh.site.operativeProtocol.fill');
goog.require('gh.site.operativeProtocol.issued');
goog.require('gh.site.operativeProtocol.pendings');
goog.require('gh.site.operativeProtocol.reports');
goog.require('gh.site.operativeProtocol.view');
goog.require('gh.site.surgicalOrder.suspend');

goog.provide('gh.site');

/**
 * @type {!angular.Module}
 */
gh.site.module = angular.module(
	'gh.site',
	[
		gh.site.home.module.name,
		gh.site.home.menu.module.name,
		gh.site.operativeProtocol.check.module.name,
		gh.site.operativeProtocol.fill.module.name,
		gh.site.operativeProtocol.issued.module.name,
		gh.site.operativeProtocol.pendings.module.name,
		gh.site.operativeProtocol.reports.module.name,
		gh.site.operativeProtocol.view.module.name,
		gh.site.surgicalOrder.suspend.module.name
	]
);
