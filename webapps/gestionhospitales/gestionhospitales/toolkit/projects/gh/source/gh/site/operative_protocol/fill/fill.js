goog.require('fz');
goog.require('fz.config');
goog.require('fz.element.detail');
goog.require('fz.element.form');
goog.require('fz.node');
goog.require('fz.site.Controller');
goog.require('gh.server');
goog.require('gh.server.operativeProtocol');
goog.require('gh.site.operativeProtocol');

goog.provide('gh.site.operativeProtocol.fill');

/**
 * @const
 * @private
 * @type {string}
 */
gh.site.operativeProtocol.fill.NAME = 'operativeProtocol.fill';

/**
 * @typedef {{
 *		info			: (Array.<!Object>|undefined),
 *		form			: (!fz.element.form.Form|undefined),
 *		doViewPatient	: function(),
 *		onSave			: function()
 * }}
 */
gh.site.operativeProtocol.fill.Scope;

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.operativeProtocol.Resource} ghOperativeProtocol
 * @param {!angular.$injector} $injector
 * @param {!gh.site.operativeProtocol.fill.Scope} $scope
 */
gh.site.operativeProtocol.fill.Controller = function(fzNode, ghOperativeProtocol, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.operativeProtocol.fill.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	/**
	 * @type {(Object.<string, *>|undefined)}
	 */
	var meta;

	$scope.doViewPatient = function()
	{
		self.leaveTo('/gestionhospitales/gestionOq/viewPatient/' + meta['patient']['id']);
	}

	$scope.onSave = function()
	{
		var model = this.form.valueMap;
		for (var i = 0, key; key = Object.keys(model['staff'])[i]; ++i)
		{
			for (var j = 0, subkey; subkey = Object.keys(model['staff'])[j]; ++j)
			{
				if (key !== subkey &&
					null !== model['staff'][key] &&
					null !== model['staff'][subkey] &&
					undefined !== model['staff'][key] &&
					undefined !== model['staff'][subkey] &&
					model['staff'][key] === model['staff'][subkey])
				{
					console.log(key, model['staff'][key], subkey, model['staff'][subkey]);
					this.box.warning = ['Los miembros del equipo médico no pueden ser duplicados'];
					self.$window.scrollTo(0, 0);
					return;
				}
			}
		}
		if (!angular.isNumber(model['staff']['surgeon']))
		{
			this.box.warning = ['El cirujano debe ser un miembro registrado y activo'];
			self.$window.scrollTo(0, 0);
			return;
		}
		else if (
			!model['protocol']['end'] || !model['protocol']['start'] ||
			model['protocol']['end'].getTime() <= model['protocol']['start'].getTime())
		{
			this.box.warning = ['La hora de término de intervención debe ser mayor a la hora de inicio'];
			self.$window.scrollTo(0, 0);
			return;
		}
		else
			this.box.warning = fz.VOID;

		model['document'] =
		{
			'date'			: meta['protocol']['date'],
			'surgicalOrder'	: self.fzNode.paramMap['id']
		};

		fz.element.form.helper.format(this.form);

		ghOperativeProtocol.create(model, function(input)
		{
			self.fzNode.activate(
				'operativeProtocol.check',
				{id : input['id']}
			);
		});
	}

	/**
	 * @type {function(this:fz.element.form.Field, *, number)}
	 */
	var userFilter = function(item, index)
	{
		var staff = /** @type {!Object} */ ($scope.form.valueMap['staff']);
		for (var i = 0, key; key = Object.keys(staff)[i]; ++i)
		{
			if (this.name !== key && staff[key] === item['value'])
				return false;
		}

		return true;
	}

	ghOperativeProtocol.createForm(/** @type {!gh.server.param.Id} */ (this.fzNode.paramMap),
		function(input)
		{
			meta = input['meta'];
			$scope.info = input['document'];

			var form		= input['form'];
			form.control	= $scope.form.control;
			form.filterMap	=
			{
				user : userFilter
			};

			fz.element.form.helper.init(/** @type {fz.element.form.Form} */ (form));
			$scope.form = /** @type {!fz.element.form.Form} */ (form);
		}
	);

	this.initMenu();
}
fz.inherit(gh.site.operativeProtocol.fill.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.operativeProtocol.fill.module = angular.module(
	gh.site.operativeProtocol.fill.NAME,
	[
		fz.config.module.name,
		fz.element.detail.module.name,
		fz.element.form.module.name,
		fz.node.module.name,
		gh.server.operativeProtocol.module.name,
		gh.site.operativeProtocol.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.operativeProtocol.fill.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.operativeProtocol.fill.NAME,
			state	:
			{
				controller	: gh.site.operativeProtocol.fill.Controller,
				parent		: gh.site.operativeProtocol.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-op-fill.html',
				url			: '/:id/fill'
			}
		}),
		fzConfig.getNode(gh.site.operativeProtocol.fill.NAME)
	);
}
gh.site.operativeProtocol.fill.module.config(gh.site.operativeProtocol.fill.module._config);
