goog.require('fz');
goog.require('fz.config');
goog.require('fz.element.form');
goog.require('fz.node');
goog.require('fz.site.Controller');
goog.require('gh.server');
goog.require('gh.server.surgicalOrder');
goog.require('gh.site.operativeProtocol');

goog.provide('gh.site.surgicalOrder.suspend');

/**
 * @const
 * @type {string}
 */
gh.site.surgicalOrder.suspend.NAME = 'surgicalOrder.suspend';

/**
 * @typedef {{
 *		form		: (!fz.element.form.Form|undefined),
 *		onSuspend	: function()
 * }}
 */
gh.site.surgicalOrder.suspend.Scope;

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.surgicalOrder.Resource} ghSurgicalOrder
 * @param {!angular.$injector} $injector
 * @param {!gh.site.surgicalOrder.suspend.Scope} $scope
 */
gh.site.surgicalOrder.suspend.Controller = function(fzNode, ghSurgicalOrder, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.surgicalOrder.suspend.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	$scope.onSuspend = function()
	{
		var output =
		{
			id		: self.fzNode.paramMap['id'],
			causal	: this.form.valueMap['suspend']['causal'],
			remark	: this.form.valueMap['suspend']['remark']
		};

		ghSurgicalOrder.suspend(output, function(input)
		{
			self.fzNode.activate('operativeProtocol.pendings');
		});
	}

	ghSurgicalOrder.suspendForm(/** @type {!gh.server.param.Id} */ (this.fzNode.paramMap),
		function(input)
		{
			var form		= input['form'];
			form.control	= $scope.form.control;

			fz.element.form.helper.init(/** @type {fz.element.form.Form} */ (form));
			$scope.form		= /** @type {!fz.element.form.Form} */ (form);
		}
	);

	this.initMenu();
}
fz.inherit(gh.site.surgicalOrder.suspend.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.surgicalOrder.suspend.module = angular.module(
	gh.site.surgicalOrder.suspend.NAME,
	[
		fz.config.module.name,
		fz.element.form.module.name,
		fz.node.module.name,
		gh.server.surgicalOrder.module.name,
		gh.site.operativeProtocol.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.surgicalOrder.suspend.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.surgicalOrder.suspend.NAME,
			state	:
			{
				controller	: gh.site.surgicalOrder.suspend.Controller,
				parent		: gh.site.operativeProtocol.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-so-suspend.html',
				url			: '/surgical_order/:id/suspend'
			}
		}),
		fzConfig.getNode(gh.site.surgicalOrder.suspend.NAME)
	);
}
gh.site.surgicalOrder.suspend.module.config(gh.site.surgicalOrder.suspend.module._config);
