goog.require('fz.config');
goog.require('fz.node');
goog.require('fz.site.Controller');
goog.require('gh.site.auth');

goog.provide('gh.site.surgicalAct');

/**
 * @const
 * @type {string}
 */
gh.site.surgicalAct.NAME = 'surgicalAct';

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!angular.$injector} $injector
 * @param {!angular.Scope} $scope
 */
gh.site.surgicalAct.Controller = function(fzNode, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config : fzNode.get(gh.site.surgicalAct.NAME),
		traits : fz.site.Trait.FRAME,
		$scope : $scope
	});

	this.initMenu();
}
fz.inherit(gh.site.surgicalAct.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.surgicalAct.module = angular.module(
	gh.site.surgicalAct.NAME,
	[
		fz.config.module.name,
		fz.node.module.name,
		gh.site.auth.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.surgicalAct.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.surgicalAct.NAME,
			state	:
			{
				controller	: gh.site.surgicalAct.Controller,
				parent		: gh.site.auth.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-frame-default.html',
				url			: '/surgical_act'
			}
		}),
		fzConfig.getNode(gh.site.surgicalAct.NAME)
	);
}
gh.site.surgicalAct.module.config(gh.site.surgicalAct.module._config);
