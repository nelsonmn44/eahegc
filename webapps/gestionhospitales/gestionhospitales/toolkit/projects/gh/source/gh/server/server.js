goog.require('fz.config');
goog.require('gh.dialog');

goog.provide('gh.server');
goog.provide('gh.server.param');

/**
 * @typedef {(Object.<string, ?>|undefined)}
 */
gh.server.Input;

/**
 * @typedef {{
 *		id	: number
 *	}}
 */
gh.server.param.Id;

/**
 * @typedef {{
 *		filter	: string,
 *		offset	: number,
 *		count	: number
 *	}}
 */
gh.server.param.Search;

/**
 * @type {!angular.Module}
 */
gh.server.module = angular.module('gh.server',
[
	fz.config.module.name
]);

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @private
 */
gh.server.module._config = function(fzConfig)
{
	/**
	 * @type {!gh.dialog.Messenger}
	 */
	var messenger = /** @type {!gh.dialog.Messenger} */ (fzConfig.get('base.messenger'));

	fzConfig.set('server.interceptor',
	{
		responseError : function(rejection)
		{
			messenger.error = /** @type {!Array.<!gh.dialog.Message>} */ (
				rejection.data.elements
			);
		}
	});
}
gh.server.module.config(gh.server.module._config);
