goog.require('fz');
goog.require('fz.config');
goog.require('fz.element.table');
goog.require('fz.node');
goog.require('fz.site.Action');
goog.require('fz.site.Controller');
goog.require('fz.site.Search');
goog.require('fz.site.Paging');
goog.require('gh.server');
goog.require('gh.server.operativeProtocol');
goog.require('gh.site.operativeProtocol');

goog.provide('gh.site.operativeProtocol.issued');

/**
 * @const
 * @private
 * @type {string}
 */
gh.site.operativeProtocol.issued.NAME = 'operativeProtocol.issued';

/**
 * @type {!Object.<string, !Object>}
 */
gh.site.operativeProtocol.issued.actionMap =
{
	cancel	: {},
	check	: {},
	view	: {}
};

/**
 * @typedef {{
 *		doCancel		: function(this:fz.element.table.Action, !fz.element.table.Row),
 *		doCheck			: function(this:fz.element.table.Action, !fz.element.table.Row),
 *		doView			: function(this:fz.element.table.Action, !fz.element.table.Row),
 *		filterAction	: function(!fz.element.table.Row, !fz.element.table.Action):(boolean|undefined),
 *		paging			: !fz.site.Paging,
 *		search			: !fz.site.Search,
 *		keyPressFilter	: function(*),
 *		columnList		: !Array.<!fz.element.table.Column>,
 *		rowList			: (Array.<!fz.element.table.Row>|undefined)
 * }}
 */
gh.site.operativeProtocol.issued.Scope;

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.operativeProtocol.Resource} ghOperativeProtocol
 * @param {!angular.$injector} $injector
 * @param {!gh.site.operativeProtocol.issued.Scope} $scope
 */
gh.site.operativeProtocol.issued.Controller = function(fzNode, ghOperativeProtocol, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.operativeProtocol.issued.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	$scope.doCancel = function(row)
	{
		self.fzNode.activate(
			'operativeProtocol.cancel',
			{id : row['operativeProtocolId']}
		);
	}

	$scope.doCheck = function(row)
	{
		self.fzNode.activate(
			'operativeProtocol.check',
			{id : row['operativeProtocolId']}
		);
	}

	$scope.doView = function(row)
	{
		self.fzNode.activate(
			'operativeProtocol.view',
			{id : row['operativeProtocolId']}
		);
	}

	var action = new fz.site.Action(
		gh.site.operativeProtocol.issued.actionMap, this.config.actionMap
	).filter($scope);

	$scope.filterAction = function(row, action)
	{
		if ((51 === row['statusId'] && 'cancel' === action.name) ||
			(51 === row['statusId'] && 'check' === action.name) ||
			(52 === row['statusId'] && 'view' === action.name))
			return true;
	}

	/**
	 * @private
	 */
	this._searchClean = function()
	{
		$scope.paging.update(0, 0);

		ghOperativeProtocol.issuedList(function(input)
		{
			$scope.rowList = /** @type {!Array.<!fz.element.table.Row>} */ (input['itemList']);
			counter.offset = 0;
		});
	}

	/**
	 * @type {!gh.server.param.Search}
	 */
	var searchParam = /** @type {!gh.server.param.Search} */ (
	{
		count : 30
	});

	/**
	 * @param {?(string|number)=} model
	 * @private
	 */
	this._searchRun = function(model)
	{
		$scope.search.updated || $scope.paging.update(0, 0);

		searchParam.filter = /** @type {string} */ (model);
		searchParam.offset = searchParam.count * $scope.paging.index;

		ghOperativeProtocol.issuedList(searchParam, function(input)
		{
			$scope.rowList = /** @type {!Array.<!fz.element.table.Row>} */ (input['itemList']);
			$scope.paging.update($scope.rowList.length);

			counter.offset = searchParam.offset;
			self.$window.scrollTo(0, 0);
		});
	}

	$scope.search = new fz.site.Search(
		this._searchRun, this._searchClean
	);

	$scope.paging = new fz.site.Paging(
		searchParam.count,
		/** @type {function(number)} */ (angular.bind($scope.search, $scope.search.run)),
		/** @type {function(number)} */ (angular.bind($scope.search, $scope.search.run))
	);

	$scope.keyPressFilter = function(event)
	{
		if (fz.Key.ENTER === event.keyCode && this.search.model)
			this.search.run();
	}

	var counter;
	$scope.columnList = /** @type {!Array.<!fz.element.table.Column>} */ (
	[
		counter = {
			name	: 'counter',
			title	: 'Nº',
			type	: 'counter'
		},
		{
			name	: 'protocolDate',
			title	: 'Fecha Protocolo'
		},
		{
			name	: 'patientName',
			title	: 'Paciente'
		},
		{
			name	: 'postDiagnosisList',
			title	: 'Diagnóstico e Intervenciones Postoperatorias',
			type	: 'list'
		},
		{
			name	: 'specialityName',
			title	: 'Especialidad'
		},
		{
			name	: 'pavilionName',
			title	: 'Pabellón'
		},
		{
			name	: 'surgeonName',
			title	: 'Cirujano'
		},
		{
			name	: 'statusName',
			title	: 'Estado'
		},
		{
			name	: 'tryName',
			title	: 'Reintervención'
		},
		{
			name	: 'actions',
			title	: 'Acciones',
			type	: 'action',
			list	:
			[
				{
					name		: 'view',
					title		: action.get('view').title,
					callback	: $scope.doView,
					filter		: $scope.filterAction
				},
				{
					name		: 'check',
					title		: action.get('check').title,
					callback	: $scope.doCheck,
					filter		: $scope.filterAction
				},
				{
					name		: 'cancel',
					title		: action.get('cancel').title,
					callback	: $scope.doCancel,
					filter		: $scope.filterAction
				}
			]
		}
	]);

	this._searchClean();
	this.initMenu();
}
fz.inherit(gh.site.operativeProtocol.issued.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.operativeProtocol.issued.module = angular.module(
	gh.site.operativeProtocol.issued.NAME,
	[
		fz.config.module.name,
		fz.element.table.module.name,
		fz.node.module.name,
		gh.server.operativeProtocol.module.name,
		gh.site.operativeProtocol.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.operativeProtocol.issued.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.operativeProtocol.issued.NAME,
			state	:
			{
				controller	: gh.site.operativeProtocol.issued.Controller,
				parent		: gh.site.operativeProtocol.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-op-issued.html',
				url			: '/issued'
			}
		}),
		fzConfig.getNode(gh.site.operativeProtocol.issued.NAME)
	);
}
gh.site.operativeProtocol.issued.module.config(gh.site.operativeProtocol.issued.module._config);
