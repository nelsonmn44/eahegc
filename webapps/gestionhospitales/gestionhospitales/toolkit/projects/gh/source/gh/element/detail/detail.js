goog.require('fz.config');

goog.provide('gh.element.detail');

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @return {!angular.Directive}
 */
gh.element.detail.directive = function(fzConfig)
{
	var scope =
	{
		/**
		 * @expose
		 * @type {string}
		 */
		clazz		: fz.element.Attrib.CLASS,

		/**
		 * @expose
		 * @type {string}
		 */
		itemList	: fz.element.Attrib.ITEM_LIST,

		/**
		 * @expose
		 * @type {string}
		 */
		model		: fz.element.Attrib.MODEL,

		/**
		 * @expose
		 * @type {string}
		 */
		title		: fz.element.Attrib.TITLE
	};

	return /** @type {!angular.Directive} */(
	{
		restrict	: 'AE',
		scope		: scope,
		templateUrl	: fzConfig.get('template.pool.url') + '/gh-detail.html',
		link		: function (scope, element, attrs)
		{
			element.addClass('fz-detail').addClass(attrs['fzClass']);
		}
	});
}

/**
 * @type {!angular.Module}
 */
gh.element.detail.module = angular.module('gh.element.detail',
[
	fz.config.module.name
]);

gh.element.detail.module.directive(
	'ghDetail',
	gh.element.detail.directive
);
