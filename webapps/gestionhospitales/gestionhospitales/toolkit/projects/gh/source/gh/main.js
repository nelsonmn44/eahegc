goog.require('fz');
goog.require('fz.auth');
goog.require('fz.config');
goog.require('fz.element.breadcrumb');
goog.require('fz.element.menu');
goog.require('gh.element.dialog');
goog.require('gh.dialog');
goog.require('gh.site');

goog.provide('gh.main');

/**
 * @type {!angular.Module}
 */
gh.main.module = angular.module('gh',
[
	fz.Cnt.UI_ROUTER,
	fz.auth.module.name,
	fz.config.module.name,
	fz.element.breadcrumb.module.name,
	fz.element.menu.module.name,
	gh.element.dialog.module.name,
	gh.site.module.name
]);

/**
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNodeProvider
 * @param {!ui.router.$urlRouterProvider} $urlRouterProvider
 * @private
 */
gh.main.module._config = function(fzNodeProvider, $urlRouterProvider)
{
	fzNodeProvider.check();
	$urlRouterProvider.otherwise('/home');
}
gh.main.module.config(gh.main.module._config);

/**
 * @ngInject
 * @private
 * @param {!fz.auth.Auth} fzAuth
 * @param {!fz.config.Config} fzConfig
 * @param {!ui.router.$state} $state
 * @param {!angular.Scope} $rootScope
 * @param {!angular.$window} $window
 */
gh.main.module._run = function(fzAuth, fzConfig, $state, $rootScope, $window)
{
	/**
	 * @type {(fz.scope.SystemInfo|undefined)}
	 */
	$rootScope.system = /** @type {(fz.scope.SystemInfo|undefined)}  */ (fzConfig.get('base.system.list')[0]);

	/**
	 * @type {!fz.scope.Box}
	 */
	$rootScope.box = /** @type {!fz.scope.Box} */ (
	{
		showBreadcrumb	: true,
		showMainMenu	: true,
		showRelatedMenu	: true,
		showTitle		: true
	});

	/**
	 * @type {!gh.dialog.Messenger}
	 */
	$rootScope.messenger = /** @type {!gh.dialog.Messenger} */ (fzConfig.get('base.messenger'));

	/**
	 * @this {angular.Scope}
	 */
	$rootScope.closeDialog = function()
	{
		this.messenger.error = fz.VOID;
	};

	/**
	 * @type {!fz.scope.User}
	 */
	$rootScope.user = /** @type {!fz.scope.User} */ (fzConfig.get('user'));

	/**
	 * @type {function((?string|undefined)):boolean}
	 */
	$rootScope.is = fzAuth.check;

	// Mecanismo global de redirección
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams)
	{
		var toNode = toState.node;
		if (toNode.redirectTo && toNode.redirectTo.state)
		{
			event.preventDefault();
			console.info('Redirección en curso: origen "%s", destino "%s". Redestinando a "%s"', fromState.name, toState.name, toNode.redirectTo.state.name);
			$state.go(toNode.redirectTo.state);
		}
	});

	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams)
	{
		$window.scrollTo(0, 0)
		console.info('Transición de estado ejecutada: origen "%s", destino "%s"', fromState.name, toState.name);
	});
}
gh.main.module.run(gh.main.module._run);
