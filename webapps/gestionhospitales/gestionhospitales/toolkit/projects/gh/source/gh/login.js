goog.require('fz.config');
goog.require('gh.main');

goog.provide('gh.login');

(function()
{
	/**
	 * @type {!angular.$http}
	 */
	var $http = angular.injector(['ng']).get('$http');

	$http.get('/gestionhospitales/service/auth/' + 2 + '/profile')
		.success(function(data, status, headers, config)
		{
			/**
			 * @type {!fz.config.Config}
			 */
			var fzConfig = angular.injector(['fz.config']).get('fzConfig');
			fzConfig.merge(/** @type {fz.config.Settings} */ (data));
			fzConfig.set('base.messenger', {});

			angular.element(document).ready(function()
			{
				angular.bootstrap(document.querySelector('#gh-app'), ['gh']);
			});
		})
		.error(function(data, status, headers, config)
		{
			//window.location.href = '/gestionhospitales';
		}
	);
})();
