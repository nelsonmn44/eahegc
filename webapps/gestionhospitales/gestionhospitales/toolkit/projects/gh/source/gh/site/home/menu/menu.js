goog.require('fz');
goog.require('fz.config');
goog.require('fz.node');
goog.require('gh.site.home');

goog.provide('gh.site.home.menu');

/**
 * @const
 * @private
 * @type {string}
 */
gh.site.home.menu.NAME = 'home.menu';

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.operativeProtocol.Resource} ghOperativeProtocol
 * @param {!angular.$injector} $injector
 * @param {!gh.site.home.menu.Scope} $scope
 */
gh.site.home.menu.Controller = function(fzNode, ghOperativeProtocol, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.home.menu.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	this.initMenu();
}
fz.inherit(gh.site.home.menu.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.home.menu.module = angular.module(
	gh.site.home.menu.NAME,
	[
		fz.config.module.name,
		fz.element.table.module.name,
		fz.node.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.home.menu.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.home.menu.NAME,
			state	:
			{
				controller	: gh.site.home.menu.Controller,
				parent		: gh.site.home.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-home-menu.html',
				url			: '/menu'
			}
		}),
		fzConfig.getNode(gh.site.home.menu.NAME)
	);
}
gh.site.home.menu.module.config(gh.site.home.menu.module._config);
