goog.require('fz.config');
goog.require('fz.node');
goog.require('gh.site.surgicalAct');

goog.provide('gh.site.operativeProtocol');

/**
 * @const
 * @type {string}
 */
gh.site.operativeProtocol.NAME = 'operativeProtocol';

/**
 * @type {!angular.Module}
 */
gh.site.operativeProtocol.module = angular.module(
	gh.site.operativeProtocol.NAME,
	[
		fz.config.module.name,
		fz.node.module.name,
		gh.site.surgicalAct.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.operativeProtocol.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.operativeProtocol.NAME,
			state	:
			{
				parent		: gh.site.surgicalAct.NAME,
				template	: fzConfig.get('template.default'),
				url			: '/operative_protocol'
			}
		}),
		fzConfig.getNode(gh.site.operativeProtocol.NAME)
	);
}
gh.site.operativeProtocol.module.config(gh.site.operativeProtocol.module._config);
