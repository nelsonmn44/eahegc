goog.provide('gh.dialog');

/**
 * @typedef {{
 *		id			: number,
 *		description	: string
 * }}
 */
gh.dialog.Message;

/**
 * @typedef {{
 *		info 	: (Array.<!gh.dialog.Message>|undefined),
 *		warning	: (Array.<!gh.dialog.Message>|undefined),
 *		error	: (Array.<!gh.dialog.Message>|undefined)
 *	}}
 */
gh.dialog.Messenger;
