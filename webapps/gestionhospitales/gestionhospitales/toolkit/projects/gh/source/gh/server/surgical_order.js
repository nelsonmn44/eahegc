goog.require('fz.config');
goog.require('gh.server');

goog.provide('gh.server.surgicalOrder');

/**
 * @typedef {{
 *		suspendForm	: function(!gh.server.param.Id, function(!gh.server.Input)),
 *		suspend		: function(!Object, function(!gh.server.Input)=)
 *	}}
 */
gh.server.surgicalOrder.Resource;

/**
 * @ngInject
 * @constructor
 * @param {!angular.$resource} $resource
 * @param {!fz.config.Config} fzConfig
 * @return {!gh.server.surgicalOrder.Resource}
 */
gh.server.surgicalOrder.SurgicalOrder = function ($resource, fzConfig)
{
	return /** @type {!gh.server.surgicalOrder.Resource} */ ($resource(
		fzConfig.get('base.service.url') + '/general/surgical_order/:listController:id/:docController',
		{
			id				: '@id',
			listController	: '@listController',
			docController	: '@docController'
		},
		{
			suspendForm :
			{
				method	: 'GET',
				params	:
				{
					docController : 'suspend'
				},
				interceptor : fzConfig.get('server.interceptor')
			},
			suspend :
			{
				method	: 'PUT',
				params	:
				{
					docController : 'suspend'
				},
				interceptor : fzConfig.get('server.interceptor')
			}
		}
	));
}

/**
 * @type {!angular.Module}
 */
gh.server.surgicalOrder.module = angular.module(
	'gh.server.surgicalOrder',
	[
		'ngResource',
		fz.config.module.name,
		gh.server.module.name
	]
);

gh.server.surgicalOrder.module.service(
	'ghSurgicalOrder', gh.server.surgicalOrder.SurgicalOrder
);

