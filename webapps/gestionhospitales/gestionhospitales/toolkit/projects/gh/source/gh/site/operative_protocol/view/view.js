goog.require('fz');
goog.require('fz.config');
goog.require('fz.element.detail');
goog.require('fz.node');
goog.require('fz.site.Controller');
goog.require('gh.server');
goog.require('gh.server.operativeProtocol');
goog.require('gh.server.exporter');
goog.require('gh.site.operativeProtocol');

goog.provide('gh.site.operativeProtocol.view');

/**
 * @const
 * @private
 * @type {string}
 */
gh.site.operativeProtocol.view.NAME = 'operativeProtocol.view';

/**
 * @typedef {{
 *		info			: (Array.<!Object>|undefined),
 *		doPrint			: function(),
 *		doViewPatient	: function()
 * }}
 */
gh.site.operativeProtocol.view.Scope;

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.operativeProtocol.Resource} ghOperativeProtocol
 * @param {!gh.server.exporter.Resource} ghExporter
 * @param {!angular.$injector} $injector
 * @param {!gh.site.operativeProtocol.view.Scope} $scope
 */
gh.site.operativeProtocol.view.Controller = function(fzNode, ghOperativeProtocol, ghExporter, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.operativeProtocol.view.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	/**
	 * @type {(Object.<string, *>|undefined)}
	 */
	var meta;

	$scope.doPrint = function()
	{
		ghExporter.document('operative_protocol', self.fzNode.paramMap['id']);
	}

	$scope.doViewPatient = function()
	{
		self.leaveTo('/gestionhospitales/gestionOq/viewPatient/' + meta['patient']['id']);
	}

	$scope.columnList = /** @type {!Array.<!fz.element.table.Column>} */ (
	[
		{
			name	: 'incomeDate',
			title	: 'Fecha Ingreso'
		},
		{
			name	: 'preDiagnosisList',
			title	: 'Diagnóstico e Intervenciones Preoperatorias',
			type	: 'list'
		},
		{
			name	: 'surgeonName',
			title	: 'Cirujano'
		},
		{
			name	: 'surgicalOrderId',
			title	: 'Orden'
		}
	]);

	ghOperativeProtocol.get(/** @type {!gh.server.param.Id} */ (self.fzNode.paramMap),
		function(input)
		{
			meta = input['meta'];
			$scope.info = input['document'];
		}
	);

	this.initMenu();
}
fz.inherit(gh.site.operativeProtocol.view.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.operativeProtocol.view.module = angular.module(
	gh.site.operativeProtocol.view.NAME,
	[
		fz.config.module.name,
		fz.element.detail.module.name,
		fz.node.module.name,
		gh.server.operativeProtocol.module.name,
		gh.server.exporter.module.name,
		gh.site.operativeProtocol.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.operativeProtocol.view.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.operativeProtocol.view.NAME,
			state	:
			{
				controller	: gh.site.operativeProtocol.view.Controller,
				parent		: gh.site.operativeProtocol.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-op-view.html',
				url			: '/:id/view'
			}
		}),
		fzConfig.getNode(gh.site.operativeProtocol.view.NAME)
	);
}
gh.site.operativeProtocol.view.module.config(gh.site.operativeProtocol.view.module._config);
