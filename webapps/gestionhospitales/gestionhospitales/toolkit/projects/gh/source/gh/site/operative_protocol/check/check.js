goog.require('fz');
goog.require('fz.config');
goog.require('fz.element.detail');
goog.require('fz.element.table');
goog.require('fz.node');
goog.require('fz.site.Controller');
goog.require('gh.server');
goog.require('gh.server.operativeProtocol');
goog.require('gh.site.operativeProtocol');

goog.provide('gh.site.operativeProtocol.check');

/**
 * @const
 * @type {string}
 */
gh.site.operativeProtocol.check.NAME = 'operativeProtocol.check';

/**
 * @typedef {{
 *		info			: (Array.<!Object>|undefined),
 *		doValidate		: function(),
 *		doViewPatient	: function(),
 *		columnList		: !Array.<!fz.element.table.Column>,
 *		rowList			: (Array.<!fz.element.table.Row>|undefined)
 * }}
 */
gh.site.operativeProtocol.check.Scope;

/**
 * @constructor
 * @extends fz.site.Controller
 * @ngInject
 * @param {!fz.node.NodeProvider} fzNode
 * @param {!gh.server.operativeProtocol.Resource} ghOperativeProtocol
 * @param {!angular.$injector} $injector
 * @param {!gh.site.operativeProtocol.check.Scope} $scope
 */
gh.site.operativeProtocol.check.Controller = function(fzNode, ghOperativeProtocol, $injector, $scope)
{
	$injector.invoke(fz.site.Controller, this,
	{
		config	: fzNode.get(gh.site.operativeProtocol.check.NAME),
		traits	: fz.site.Trait.PANEL,
		$scope	: $scope
	});

	var self = this;

	/**
	 * @type {(Object.<string, *>|undefined)}
	 */
	var meta;

	$scope.doValidate = function()
	{
		var popList	= [];

		this.columnList[0].model.forEach(function(item, index)
		{
			if (item)
				popList[popList.length] = this.rowList[index]['surgicalOrderId'];
		}, this);

		var output =
		{
			id			: self.fzNode.paramMap['id'],
			attached	: popList
		};

		ghOperativeProtocol.validate(output, function(input)
		{
			self.fzNode.activate(
				'operativeProtocol.view',
				self.fzNode.paramMap
			);
		});
	}

	$scope.doViewPatient = function()
	{
		self.leaveTo('/gestionhospitales/gestionOq/viewPatient/' + meta['patient']['id']);
	}

	/**
	 * @type {function(this:fz.element.table.Column, *):string}
	 */
	var checkStyle = function(row)
	{
		if (row && row['current'])
			return 'attach disabled';

		return 'attach';
	}

	$scope.columnList = /** @type {!Array.<!fz.element.table.Column>} */ (
	[
		{
			name	: 'attach',
			title	: 'Adjuntar',
			type	: 'select',
			model	: [true],
			style	: checkStyle
		},
		{
			name	: 'incomeDate',
			title	: 'Fecha Ingreso'
		},
		{
			name	: 'patientName',
			title	: 'Paciente'
		},
		{
			name	: 'preDiagnosisList',
			title	: 'Diagnóstico e Intervenciones',
			type	: 'list'
		},
		{
			name	: 'surgeonName',
			title	: 'Cirujano'
		},
		{
			name	: 'statusName',
			title	: 'Estado'
		},
		{
			name	: 'surgicalOrderId',
			title	: 'Orden'
		}
	]);

	ghOperativeProtocol.validateForm(/** @type {!gh.server.param.Id} */ (self.fzNode.paramMap),
		function(input)
		{
			meta = input['meta'];

			$scope.info		= input['document'];
			$scope.rowList	= input['attach'];
		}
	);

	this.initMenu();
}
fz.inherit(gh.site.operativeProtocol.check.Controller, fz.site.Controller);

/**
 * @type {!angular.Module}
 */
gh.site.operativeProtocol.check.module = angular.module(
	gh.site.operativeProtocol.check.NAME,
	[
		fz.config.module.name,
		fz.element.detail.module.name,
		fz.element.table.module.name,
		fz.node.module.name,
		gh.server.operativeProtocol.module.name,
		gh.site.operativeProtocol.module.name
	]
);

/**
 * @ngInject
 * @private
 * @param {!fz.config.Config} fzConfig
 * @param {!fz.node.NodeProvider} fzNodeProvider
 */
gh.site.operativeProtocol.check.module._config = function(fzConfig, fzNodeProvider)
{
	fzNodeProvider.addMerged(
		/** @type {!fz.node.Config} */ (
		{
			name	: gh.site.operativeProtocol.check.NAME,
			state	:
			{
				controller	: gh.site.operativeProtocol.check.Controller,
				parent		: gh.site.operativeProtocol.NAME,
				templateUrl	: fzConfig.get('template.pool.url') + '/gh-op-check.html',
				url			: '/:id/check'
			}
		}),
		fzConfig.getNode(gh.site.operativeProtocol.check.NAME)
	);
}
gh.site.operativeProtocol.check.module.config(gh.site.operativeProtocol.check.module._config);
