goog.require('fz');

goog.provide('gh.site.auth');

/**
 * @const
 * @type {string}
 */
gh.site.auth.NAME = 'auth';

/**
 * @typedef {{
 *		user : !fz.scope.User
 * }}
 */
gh.site.auth.Scope;

/**
 * @constructor
 * @ngInject
 * @param {!gh.site.auth.Scope} $scope
 */
gh.site.auth.Controller = function($scope)
{
	console.info(
		'Controlador Global de Autenticación creado para la aplicación "gestion-hospitales (GH)"\n\tUsuario: "%s"',
		$scope.user.username
	);
}

/**
 * @type {!angular.Module}
 */
gh.site.auth.module = angular.module(
	gh.site.auth.NAME,
	[
		fz.Cnt.UI_ROUTER,
		fz.config.module.name
	]
);

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @param {!ui.router.$stateProvider} $stateProvider
 * @private
 */
gh.site.auth.module._config = function(fzConfig, $stateProvider)
{
	/**
	 * @type {!fz.state.Config}
	 */
	var auth =  /** @type {!fz.state.Config} */ (
	{
		abstract	: true,
		controller	: gh.site.auth.Controller,
		template	: fzConfig.get('template.default')
	});

	$stateProvider.state(gh.site.auth.NAME, auth);
}
gh.site.auth.module.config(gh.site.auth.module._config);
