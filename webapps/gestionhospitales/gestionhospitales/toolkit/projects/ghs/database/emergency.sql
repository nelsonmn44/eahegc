use urgencia;

drop view		if exists gh_triage_queue;

create view gh_triage_queue as
select
	dau.dau_id,
	dau.dauestado_id as status_id,
	sts.descripcion as status_name,
	dau.especialidad_id as speciality_id,
	spy.nombre as speciality_name,
	dau.admision_fecha as income_date,
	var.elapsed as elapsed_minutes,
	case
		when var.elapsed > 30 then 2
		when var.elapsed > 15 then 1
		else 0
	end as wait_status_id,
	pat.paciente_id as patient_id,
	upper(concat_ws(" ", trim(pat.nombre), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as patient_name
from dau
inner join especialidad as spy
	on dau.especialidad_id = spy.especialidad_id
inner join dauestado as sts
	on dau.dauestado_id = sts.dauestado_id
	and dau.dauestado_id in (0, 4, 5)
inner join paciente as pat
	on dau.paciente_id = pat.paciente_id
inner join (
	select
		dau.dau_id,
		timestampdiff(minute, dau.admision_fecha, now()) as elapsed
	from dau) as var
	on dau.dau_id = var.dau_id
	and var.elapsed < 181
where dau.triage_fecha is null;
