use gestionhospitales;

drop view		if exists gh_operative_protocol_attached_surgical_order;
drop view		if exists gh_operative_protocol_medical_team;
drop view		if exists gh_operative_protocol_candidate;
drop view		if exists gh_operative_protocol_issued;
drop table		if exists gh_operative_protocol_custom_staff;
drop table		if exists gh_custom_user;
drop table		if exists gh_operative_protocol_description_template;
drop table		if exists gh_attached_surgical_order;
drop table		if exists gh_operative_protocol_staff;
drop table		if exists gh_operative_protocol_detail;
drop table		if exists gh_operative_protocol;
drop table		if exists gh_wound_type;
drop table		if exists gh_surgery_try;
drop table		if exists gh_surgery_risk;
drop table		if exists gh_medical_title;
drop table		if exists gh_antiseptic;
drop table		if exists gh_anesthesia_type;

/* ============================================================================================== */

create table gh_anesthesia_type (
	id		int not null auto_increment primary key,
	name	varchar(40) not null
);

insert into gh_anesthesia_type (name) values ('General');
insert into gh_anesthesia_type (name) values ('Local');
insert into gh_anesthesia_type (name) values ('Regional');
insert into gh_anesthesia_type (name) values ('Mixta');

create table gh_antiseptic (
	id		int not null auto_increment primary key,
	name	varchar(40) not null
);

insert into gh_antiseptic (name) values ('Povidona');
insert into gh_antiseptic (name) values ('Clorhexidina');
insert into gh_antiseptic (name) values ('Otro');

create table gh_medical_role (
	id		int not null auto_increment primary key,
	code	varchar(20) not null unique,
	name	varchar(40) not null
);

insert into gh_medical_role (code, name) values ('surgeon', 'Cirujano');
insert into gh_medical_role (code, name) values ('assistant', 'Ayudante');
insert into gh_medical_role (code, name) values ('anaesthetist', 'Anestesiólogo');
insert into gh_medical_role (code, name) values ('surgicalNurse', 'Arsenalera');
insert into gh_medical_role (code, name) values ('roomNurse', 'Pabellonera');

create table gh_surgery_risk (
	id		int not null auto_increment primary key,
	name	varchar(40) not null
);

insert into gh_surgery_risk (name) values ('Leve');
insert into gh_surgery_risk (name) values ('Moderado');
insert into gh_surgery_risk (name) values ('Grave');

create table gh_surgery_try (
	id		int not null auto_increment primary key,
	name	varchar(40) not null
);

insert into gh_surgery_try (name) values ('Primera intervención');
insert into gh_surgery_try (name) values ('Reintervención');
insert into gh_surgery_try (name) values ('Reintervención programada');

create table gh_wound_type (
	id		int not null auto_increment primary key,
	name	varchar(40) not null
);

insert into gh_wound_type (name) values ('Limpia');
insert into gh_wound_type (name) values ('Limpia-contaminada');
insert into gh_wound_type (name) values ('Contaminada');
insert into gh_wound_type (name) values ('Sucia');

/* ============================================================================================== */

create table gh_operative_protocol (
	id						int not null auto_increment primary key,
	surgical_order_id		int not null unique,
	creation_date			datetime not null,
	validation_date			datetime,
	surgery_date			datetime not null,
	surgeon_id				int not null,
	user_id					int not null,
	foreign key (surgical_order_id)
		references ordenquirurgica (id),
	foreign key (surgeon_id)
		references usuario (id),
	foreign key (user_id)
		references usuario (id)
);

create table gh_operative_protocol_detail (
	operative_protocol_id	int not null primary key,
	surgical_time			time,
	care_type_id			int,
	pavilion_id				int,
	surgery_try_id			int,
	wound_type_id			int,
	anesthesia_type_id		int,
	surgery_risk_id			int,
	antiseptic_id			int,
	prediagnosis			text,
	preintervention			text,
	preremark				text,
	postdiagnosis			text,
	postintervention		text,
	postremark				text,
	biopsy					boolean not null default false,
	culture					boolean not null default false,
	imaging					boolean not null default false,
	skinprep				boolean not null default false,
	antibiotic_prophylaxis	time,
	surgical_description	text,
	constraint foreign key (operative_protocol_id)
		references gh_operative_protocol (id),
	constraint foreign key (care_type_id)
		references tipohospitalizacion (id),
	constraint foreign key (pavilion_id)
		references pabellon_sala (id),
	constraint foreign key (surgery_try_id)
		references gh_surgery_try (id),
	constraint foreign key (wound_type_id)
		references gh_wound_type (id),
	constraint foreign key (anesthesia_type_id)
		references gh_anesthesia_type (id),
	constraint foreign key (surgery_risk_id)
		references gh_surgery_risk (id),
	constraint foreign key (antiseptic_id)
		references gh_antiseptic (id)
);

create table gh_operative_protocol_staff (
	operative_protocol_id	int not null,
	user_id					int not null,
	medical_title_id		int not null,
	ordinal					tinyint not null default 1,
	primary key (operative_protocol_id, user_id),
	constraint foreign key (operative_protocol_id)
		references gh_operative_protocol (id),
	constraint foreign key (user_id)
		references usuario (id),
	constraint foreign key (medical_title_id)
		references gh_medical_title (id)
);

create table gh_attached_surgical_order (
	operative_protocol_id	int not null,
	surgical_order_id		int not null,
	primary key (operative_protocol_id, surgical_order_id),
	foreign key (operative_protocol_id)
		references gh_operative_protocol (id),
	foreign key (surgical_order_id)
		references ordenquirurgica (id)
);

create table gh_operative_protocol_description_template (
	user_id			int not null,
	intervention_id	int not null,
	template		text,
	primary key (user_id, intervention_id),
	constraint foreign key (user_id)
		references usuario (id),
	constraint foreign key (intervention_id)
		references intervencion (id)
);

/* ============================================================================================== */

create table gh_custom_user (
	uid						char(36) not null primary key,
	name					varchar(255) not null
);

create table gh_operative_protocol_custom_staff (
	operative_protocol_id	int not null,
	custom_user_uid			char(36) not null,
	medical_title_id		int not null,
	ordinal					tinyint not null default 1,
	primary key (operative_protocol_id, custom_user_uid),
	constraint foreign key (operative_protocol_id)
		references gh_operative_protocol (id),
	constraint foreign key (custom_user_uid)
		references gh_custom_user (uid),
	constraint foreign key (medical_title_id)
		references gh_medical_title (id)
);

/* ============================================================================================== */
drop view		if exists gh_operative_protocol_issued;
create view gh_operative_protocol_issued as
select
	op.id as operative_protocol_id,
	op.surgical_order_id,
	op.creation_date as operative_protocol_date,
	op.surgery_date,
	pat.id as patient_id,
	pat.ficha as patient_file,
	upper(concat_ws(' ', substring_index(trim(pat.primer_nombre), ' ', 1), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as patient_name,
	sor.especialidad_id as speciality_id,
	spy.nombre as speciality_name,
	opd.pavilion_id,
	pav.sala as pavilion_name,
	op.surgeon_id,
	upper(concat_ws(' ', substring_index(trim(sur.nombre), ' ', 1), sur.apellido_paterno, sur.apellido_materno)) as surgeon_name,
	case when op.validation_date is null then 51 else 52 end as status_id,
	case when op.validation_date is null then "Protocolo no Validado" else "Protocolizado" end as status_name,
	(select
		count(1)
	from gh_attached_surgical_order as aso
	where aso.operative_protocol_id = op.id
	group by aso.operative_protocol_id) as attached_surgical_order_count,
	opd.surgery_try_id as try_id,
	opt.name as try_name
from gh_operative_protocol as op
inner join gh_operative_protocol_detail as opd
	on op.id = opd.operative_protocol_id
inner join ordenquirurgica as sor
	on op.surgical_order_id = sor.id
inner join paciente as pat
	on sor.paciente_id = pat.id
inner join especialidad as spy
	on sor.especialidad_id = spy.id
inner join pabellon_sala as pav
	on opd.pavilion_id = pav.id
inner join usuario as sur
	on op.surgeon_id = sur.id
inner join gh_surgery_try as opt
	on opd.surgery_try_id = opt.id;

create view gh_operative_protocol_candidate as
select
	null as operative_protocol_id,
	sor.id as surgical_order_id,
	sor.fechahora_ingreso as income_date,
	str_to_date(concat(sor.fecha_tabla, ' ', ifnull(sor.tabla_hora, '')), '%Y-%m-%d %H:%i:%s') as programmed_date,
	datediff(now(), sor.fecha_tabla) as elapsed_days,
	pav.id as pavilion_id,
	pav.sala as pavilion_name,
	pat.id as patient_id,
	upper(concat_ws(' ', substring_index(trim(pat.primer_nombre), ' ', 1), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as patient_name,
	sur.id as surgeon_id,
	upper(concat_ws(' ', substring_index(trim(sur.nombre), ' ', 1), sur.apellido_paterno, sur.apellido_materno)) as surgeon_name,
	spy.id as speciality_id,
	spy.nombre as speciality_name,
	sts.id as status_id,
	sts.nombre as status_name
from ordenquirurgica as sor
left join gh_operative_protocol as op
	on sor.id = op.surgical_order_id
left join gh_attached_surgical_order as aso
	on sor.id = aso.surgical_order_id
left join pabellon_sala as pav
	on sor.pabsala_id = pav.id
inner join paciente as pat
	on sor.paciente_id = pat.id
left join usuario as sur
	on sor.cirujano_id = sur.id
inner join especialidad as spy
	on sor.especialidad_id = spy.id
inner join oqestado as sts
	on sor.estado_id = sts.id
where sor.estado_id in (1, 2, 3, 4, 5, 11, 13, 14)
	and op.id is null
	and aso.operative_protocol_id is null
	and (sor.fecha_tabla is null or datediff(now(), sor.fecha_tabla) >= 0)
union
select
	opi.operative_protocol_id,
	opi.surgical_order_id,
	sor.fechahora_ingreso as income_date,
	opi.surgery_date as programmed_date,
	datediff(now(), opi.surgery_date) as elapsed_days,
	opi.pavilion_id,
	opi.pavilion_name,
	opi.patient_id,
	opi.patient_name,
	opi.surgeon_id,
	opi.surgeon_name,
	opi.speciality_id,
	opi.speciality_name,
	opi.status_id,
	opi.status_name
from gh_operative_protocol_issued as opi
inner join ordenquirurgica as sor
	on opi.surgical_order_id = sor.id
where opi.status_id = 51;

create view gh_operative_protocol_medical_team as
select
	ops.operative_protocol_id,
	group_concat(case when 2 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end) as first_assistant_id,
	group_concat(case when 2 = ops.medical_title_id and 2 = ops.ordinal then ops.user_id end) as second_assistant_id,
	group_concat(case when 2 = ops.medical_title_id and 3 = ops.ordinal then ops.user_id end) as third_assistant_id,
	group_concat(case when 3 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end) as anaesthetist_id,
	group_concat(case when 4 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end) as surgical_nurse_id,
	group_concat(case when 5 = ops.medical_title_id and 1 = ops.ordinal then ops.user_id end) as first_room_nurse_id,
	group_concat(case when 5 = ops.medical_title_id and 2 = ops.ordinal then ops.user_id end) as second_room_nurse_id
from gh_operative_protocol_staff as ops
group by ops.operative_protocol_id;

create view gh_operative_protocol_attached_surgical_order as
select
	true as main,
	op.id as operative_protocol_id,
	sor.id as surgical_order_id,
	sor.fechahora_ingreso as income_date,
	sur.id as surgeon_id,
	upper(concat_ws(" ", sur.apellido_paterno, sur.apellido_materno, sur.nombre)) as surgeon_name
from ordenquirurgica as sor
inner join gh_operative_protocol as op
	on sor.id = op.surgical_order_id
left join usuario as sur
	on sor.cirujano_id = sur.id
union
select
	null as main,
	aso.operative_protocol_id as operative_protocol_id,
	sor.id as surgical_order_id,
	sor.fechahora_ingreso as income_date,
	sur.id as surgeon_id,
	upper(concat_ws(" ", sur.apellido_paterno, sur.apellido_materno, sur.nombre)) as surgeon_name
from gh_attached_surgical_order as aso
inner join ordenquirurgica as sor
	on aso.surgical_order_id = sor.id
left join usuario as sur
	on sor.cirujano_id = sur.id;
