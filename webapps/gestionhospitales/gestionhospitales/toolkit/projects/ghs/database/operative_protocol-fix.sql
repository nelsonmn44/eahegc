update gh_operative_protocol_detail as opd
set
	opd.biopsy = case opd.biopsy
		when 2 then 0
		when 1 then 1
		when 0 then 0
	end,
	opd.culture = case opd.culture
		when 2 then 0
		when 1 then 1
		when 0 then 0
	end,
	opd.imaging = case opd.imaging
		when 2 then 0
		when 1 then 1
		when 0 then 0
	end,
	opd.skinprep = case opd.skinprep
		when 2 then 0
		when 1 then 1
		when 0 then 0
	end
where 0 < opd.operative_protocol_id;
