drop function if exists UC_FIRST;
drop function if exists UC_DELIM;

delimiter //

create function UC_FIRST(oldWord varchar(255))
returns varchar(255) deterministic
  return concat(ucase(substring(oldWord, 1, 1)), substring(oldWord, 2));

create function UC_DELIM(oldName varchar(255), delim varchar(1), trimSpaces bool)
returns varchar(255) deterministic
begin
	set @oldString := lower(oldName);
	set @newString := '';

	tokenLoop: loop
		if trimSpaces then
			set @oldString := trim(both ' ' from @oldString);
		end if;

		set @splitPoint := locate(delim, @oldString);

		if @splitPoint = 0 then
			set @newString := concat(@newString, uc_first(@oldString));
			leave tokenLoop;
		end if;

		set @newString := concat(@newString, uc_first(substring(@oldString, 1, @splitPoint)));
		set @oldString := substring(@oldString, @splitPoint + 1);
	end loop tokenLoop;

	return @newString;
end //
delimiter ;

drop function if exists fz_age;
delimiter //

create function fz_age(dob date)
returns char(20)
deterministic
begin
	declare years int default 0;
	declare months int default 0;
	declare days int default 0;
	declare age date;

	select date_add(
		'0001-01-01',
		interval datediff(current_date(), dob)
		day
	) into age;

	if age is null or age = 0 or age = '0000-00-00' then
		return age;
	end if;

	select year(age) - 1	into years;
	select month(age) - 1	into months;
	select day(age) - 1		into days;

	return concat(years, 'a ', months, 'm ', days, 'd');
end //

delimiter ;
