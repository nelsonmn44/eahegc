FICHA DEL PACIENTE
CODIGO INTERVENCIÓN FONASA
CODIGO CIE10
GLOSA IQ
RUT DEL PACIENTE
DV
NOMBRE PACIENTE
APELLIDO PATERNO
APELLIDO MATERNO
FECHA IQ
ESPECIALIDAD
NOMBRE CIRUJANO
NOMBRE ANESTESISTA
TIEMPO REAL IQ
ORIGEN (LEQ-HOSP)

FICHA DEL PACIENTE
CODIGO INTERVENCIÓN FONASA
CODIGO CIE10
GLOSA IQ
RUT DEL PACIENTE
DV
NOMBRE PACIENTE
APELLIDO PATERNO
APELLIDO MATERNO
ESPECIALIDAD
FECHA SUSPENSION
CAUSAL SUSPENSIÓN
NOMBRE CIRUJANO
NOMBRE ANESTESISTA
ORIGEN (LEQ-HOSP)

/* LAST */
select
	*
from ordenquirurgica as sor
where sor.fecha_tabla between "2014-10-15 00:00:00" and "2014-10-20 23:59:59";

select
	*
from gh_operative_protocol as op
where op.surgery_date between "2014-10-15 00:00:00" and "2014-10-20 23:59:59";

select
	sor.*
from ordenquirurgica as sor
inner join gh_operative_protocol as op
	on sor.id = op.surgical_order_id
	and sor.fecha_tabla is null
where sor.fecha_tabla between "2014-10-15 00:00:00" and "2014-10-20 23:59:59";

select
	sor.*
from ordenquirurgica as sor
left join gh_operative_protocol as op
	on sor.id = op.surgical_order_id
where sor.fecha_tabla between "2014-10-15 00:00:00" and "2014-10-20 23:59:59"
	and op.id is null;

/**
 * Lista de Protocolos
 */
select
	op.id as "ID PROTOCOLO",
	op.surgical_order_id as "ID IQ",
	pat.ficha as "FICHA DEL PACIENTE",
	case locate(" - ", opd.postintervention) when 0 then null else substring_index(opd.postintervention, " - ", 1) end as "CODIGO INTERVENCIÓN FONASA",
	case locate(" - ", opd.postdiagnosis) when 0 then null else substring_index(opd.postdiagnosis, " - ", 1) end as "CODIGO CIE10",
	substring_index(opd.postdiagnosis, " - ", -1) as "GLOSA IQ",
	substring_index(pat.rut, "-", 1) as "RUT DEL PACIENTE",
	case locate("-", pat.rut) when 0 then null else substring_index(pat.rut, "-", -1) end as "DV",
	upper(concat_ws(" ", substring_index(trim(pat.primer_nombre), " ", 1), substring_index(trim(pat.segundo_nombre), " ", -1))) as "NOMBRE PACIENTE",
	upper(trim(pat.apellido_paterno)) as "APELLIDO PATERNO",
	upper(trim(pat.apellido_materno)) as "APELLIDO MATERNO",
	pat.fecha_nacimiento as "FECHA NACIMIENTO",
	case pat.sexo when "M" then "1" when "F" then "2" else null end as "SEXO",
	date_format(op.surgery_date, "%Y-%m-%d") as "FECHA IQ",
	spc.nombre as "ESPECIALIDAD",
	upper(concat_ws(" ", trim(sur.nombre), trim(sur.apellido_paterno), trim(sur.apellido_materno))) as "NOMBRE CIRUJANO",
	case
		when ant.id is not null then upper(concat_ws(" ", trim(ant.nombre), trim(ant.apellido_paterno), trim(ant.apellido_materno)))
		else upper(trim(cus.name))
	end as "NOMBRE ANESTESISTA",
	opd.surgical_time "TIEMPO REAL IQ",
	"LEQ" as "ORIGEN"
from gh_operative_protocol as op
inner join gh_operative_protocol_detail as opd
	on op.id = opd.operative_protocol_id
inner join ordenquirurgica as sor
	on op.surgical_order_id = sor.id
inner join paciente as pat
	on sor.paciente_id = pat.id
left join usuario as sur
	on op.surgeon_id = sur.id
inner join especialidad as spc
	on sor.especialidad_id = spc.id
left join (gh_operative_protocol_medical_team as otm, usuario as ant)
	on (op.id = otm.operative_protocol_id and otm.anaesthetist_id = ant.id)
left join (gh_operative_protocol_custom_staff as ocs, gh_custom_user as cus)
	on (op.id = ocs.operative_protocol_id
		and ocs.medical_title_id = 3
		and ocs.custom_user_uid = cus.uid);

/**
 * Lista de Suspendidos
 */
select
	sor.id as "ID IQ",
	pat.ficha as "FICHA DEL PACIENTE",
	case locate(" - ", sds.intervention) when 0 then null else substring_index(sds.intervention, " - ", 1) end as "CODIGO INTERVENCIÓN FONASA",
	case locate(" - ", sds.diagnosis) when 0 then null else substring_index(sds.diagnosis, " - ", 1) end as "CODIGO CIE10",
	substring_index(sds.diagnosis, " - ", -1) as "GLOSA IQ",
	substring_index(pat.rut, "-", 1) as "RUT DEL PACIENTE",
	case locate("-", pat.rut) when 0 then null else substring_index(pat.rut, "-", -1) end as "DV",
	upper(concat_ws(" ", substring_index(trim(pat.primer_nombre), " ", 1), substring_index(trim(pat.segundo_nombre), " ", -1))) as "NOMBRE PACIENTE",
	upper(trim(pat.apellido_paterno)) as "APELLIDO PATERNO",
	upper(trim(pat.apellido_materno)) as "APELLIDO MATERNO",
	spc.nombre as "ESPECIALIDAD",
	date_format(sor.fecha_tabla, "%Y-%m-%d") as "FECHA SUSPENSION",
	sca.nombre as "CAUSAL SUSPENSIÓN",
	upper(concat_ws(" ", trim(sur.nombre), trim(sur.apellido_paterno), trim(sur.apellido_materno))) as "NOMBRE CIRUJANO",
	upper(concat_ws(" ", trim(ant.nombre), trim(ant.apellido_paterno), trim(ant.apellido_materno))) as "NOMBRE ANESTESISTA",
	"LEQ" as "ORIGEN"
from ordenquirurgica as sor
inner join gh_surgical_order_diagnosis_summary as sds
	on sor.id = sds.surgical_order_id
inner join paciente as pat
	on sor.paciente_id = pat.id
left join usuario as sur
	on sor.cirujano_id = sur.id
inner join especialidad as spc
	on sor.especialidad_id = spc.id
left join oqcausasuspension as sca
	on sor.causa_suspension_id = sca.id
left join (oqequipomedico as smt, usuario as ant)
	on (sor.id = smt.oq_id and smt.user_id = ant.id and ant.esanestesista)
left join gh_operative_protocol as op
	on sor.id = op.surgical_order_id
where op.id is null
	and 1 = sor.estado_id
	and sor.causa_suspension_id is not null
	and sor.fecha_tabla between "2014-10-15 00:00:00" and "2014-11-24 23:59:59"
order by sor.id;

/**
 * Lista de Incompletos
 */
select
	sor.id as "ID IQ",
	case sor.estado_id when 14 then 1 else 0 end as "CONDICIONAL",
	pat.ficha as "FICHA DEL PACIENTE",
	case locate(" - ", sds.intervention) when 0 then null else substring_index(sds.intervention, " - ", 1) end as "CODIGO INTERVENCIÓN FONASA",
	case locate(" - ", sds.diagnosis) when 0 then null else substring_index(sds.diagnosis, " - ", 1) end as "CODIGO CIE10",
	substring_index(sds.diagnosis, " - ", -1) as "GLOSA IQ",
	substring_index(pat.rut, "-", 1) as "RUT DEL PACIENTE",
	case locate("-", pat.rut) when 0 then null else substring_index(pat.rut, "-", -1) end as "DV",
	upper(concat_ws(" ", substring_index(trim(pat.primer_nombre), " ", 1), substring_index(trim(pat.segundo_nombre), " ", -1))) as "NOMBRE PACIENTE",
	upper(trim(pat.apellido_paterno)) as "APELLIDO PATERNO",
	upper(trim(pat.apellido_materno)) as "APELLIDO MATERNO",
	spc.nombre as "ESPECIALIDAD",
	upper(concat_ws(" ", trim(sur.nombre), trim(sur.apellido_paterno), trim(sur.apellido_materno))) as "NOMBRE CIRUJANO",
	upper(concat_ws(" ", trim(ant.nombre), trim(ant.apellido_paterno), trim(ant.apellido_materno))) as "NOMBRE ANESTESISTA",
	"LEQ" as "ORIGEN"
from ordenquirurgica as sor
inner join gh_surgical_order_diagnosis_summary as sds
	on sor.id = sds.surgical_order_id
inner join paciente as pat
	on sor.paciente_id = pat.id
left join usuario as sur
	on sor.cirujano_id = sur.id
inner join especialidad as spc
	on sor.especialidad_id = spc.id
left join oqcausasuspension as sca
	on sor.causa_suspension_id = sca.id
left join (oqequipomedico as smt, usuario as ant)
	on (sor.id = smt.oq_id and smt.user_id = ant.id and ant.esanestesista)
left join gh_operative_protocol as op
	on sor.id = op.surgical_order_id
where "2014-10-15 00:00:00" < sor.fecha_tabla
	and op.id is null
	and sor.causa_suspension_id is null;
