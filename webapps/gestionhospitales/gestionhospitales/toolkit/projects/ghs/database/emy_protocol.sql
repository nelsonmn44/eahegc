create table gh_emy_protocol (
	id						int not null auto_increment primary key,
	surgical_order_id		int not null unique,
	creation_date			datetime not null,
	validation_date			datetime,
	surgery_date			datetime not null,
	surgeon_id				int not null,
	user_id					int not null,
	foreign key (surgical_order_id)
		references ordenquirurgica (id),
	foreign key (surgeon_id)
		references usuario (id),
	foreign key (user_id)
		references usuario (id)
);

create table gh_emy_protocol_detail (
	operative_protocol_id	int not null primary key,
	surgical_time			time,
	care_type_id			int,
	pavilion_id				int,
	surgery_try_id			int,
	wound_type_id			int,
	anesthesia_type_id		int,
	surgery_risk_id			int,
	antiseptic_id			int,
	prediagnosis			text,
	preintervention			text,
	preremark				text,
	postdiagnosis			text,
	postintervention		text,
	postremark				text,
	biopsy					boolean not null default false,
	culture					boolean not null default false,
	imaging					boolean not null default false,
	skinprep				boolean not null default false,
	antibiotic_prophylaxis	time,
	surgical_description	text,
	constraint foreign key (operative_protocol_id)
		references gh_emy_protocol (id),
	constraint foreign key (care_type_id)
		references tipohospitalizacion (id),
	constraint foreign key (pavilion_id)
		references pabellon_sala (id),
	constraint foreign key (surgery_try_id)
		references gh_surgery_try (id),
	constraint foreign key (wound_type_id)
		references gh_wound_type (id),
	constraint foreign key (anesthesia_type_id)
		references gh_anesthesia_type (id),
	constraint foreign key (surgery_risk_id)
		references gh_surgery_risk (id),
	constraint foreign key (antiseptic_id)
		references gh_antiseptic (id)
);

create table gh_emy_protocol_staff (
	operative_protocol_id	int not null,
	user_id					int not null,
	medical_title_id		int not null,
	ordinal					tinyint not null default 1,
	primary key (operative_protocol_id, user_id),
	constraint foreign key (operative_protocol_id)
		references gh_emy_protocol (id),
	constraint foreign key (user_id)
		references usuario (id),
	constraint foreign key (medical_title_id)
		references gh_medical_title (id)
);
