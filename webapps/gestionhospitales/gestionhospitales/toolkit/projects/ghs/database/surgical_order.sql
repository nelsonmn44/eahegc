use gestionhospitales;

drop view		if exists gh_surgical_order_diagnosis_summary;

/* ============================================================================================== */

create view gh_surgical_order_diagnosis_summary as
select
	sor.id as surgical_order_id,
	concat_ws(" - ",
		concat_ws(", ",
			group_concat(distinct dgn.codigo separator ", "),
			substring_index(sor.otro_diagnostico, " - ", 1)),
		case sor.tabla_diag
			when "" then null
			else substring_index(sor.tabla_diag, " - ", -1) end) as diagnosis,
	concat_ws(" - ",
		concat_ws(", ",
			group_concat(itv.codigo separator ", "),
			substring_index(sor.otro_diagnostico_intervencion, " - ", 1)),
		case sor.tabla_int
			when "" then null
			else substring_index(sor.tabla_int, " - ", -1) end) as intervention,
	case sor.tabla_obs
		when "" then null
		else sor.tabla_obs end as remark
from ordenquirurgica as sor
left join oqdiagnostico as sod
	on sor.id = sod.oq_id
left join oqdiagintervencion as soi
	on sod.oq_id = soi.oq_id
	and sod.diagnostico_id = soi.diagnostico_id
left join intervencion as itv
	on soi.intervencion_id = itv.id
left join diagnostico as dgn
	on sod.diagnostico_id = dgn.id
group by sor.id;
