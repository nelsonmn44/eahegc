insert high_priority into paciente (
	rut,
	nombre,
	apellido_paterno,
	apellido_materno,
	fecha_nacimiento
)
select distinct
	tmp.rut,
	tmp.nombre,
	tmp.apellido_paterno,
	tmp.apellido_materno,
	tmp.fecha_nacimiento
from temp_paciente as tmp
left join paciente as pat
	on tmp.rut = pat.rut
where pat.paciente_id is null;

delete pat from paciente as pat
inner join temp_paciente as tmp
	on pat.rut = tmp.rut;

/* Checkeo de respaldo */
select
	min(tmp.fecha_triage) as dates
from temp_triage as tmp
union
select
	max(tmp.fecha_triage) as dates
from temp_triage as tmp;

select
	count(1)
from temp_triage as trg
where trg.fecha_triage is not null
union
select
	count(1)
from temp_triage as trg
where trg.fecha_triage is null;
