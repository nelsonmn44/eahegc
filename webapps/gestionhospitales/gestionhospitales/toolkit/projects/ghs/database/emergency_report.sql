select
	dau.dau_id as "DAU",
	pat.rut as "RUT",
	upper(concat_ws(" ", trim(pat.nombre), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as "NOMBRE PACIENTE",
	case
		when floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) <= 4 then '[0-4]'
		when floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) >= 5 and floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) <= 9 then '[5-9]'
		when floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) >= 10 and floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) <= 14 then '[10-14]'
		when floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) >= 15 and floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) <= 19 then '[15-19]'
		when floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) >= 20 and floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) <= 24 then '[20-24]'
		when floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) >= 25 and floor(datediff(dau.triage_fecha, pat.fecha_nacimiento) / 365) <= 29 then '[25-29]'
	end as "RANGO ETARIO",
	dau.triage_fecha as "FECHA TRIAGE",
	dau.atencion_fecha as "FECHA ATENCIÓN",
	coalesce(nullif(dau.triage_cat_tecnico, ''), dau.triage_cat_sistema) as "CATEGORIA"
from dau
inner join paciente as pat
	on dau.paciente_id = pat.paciente_id
where dau.admision_fecha between "2014-10-01 00:00:00" and "2014-11-01 00:00:00";

-- Reporte para tesista
select
	dau.dau_id as "DAU",
	pat.rut as "RUT",
	upper(concat_ws(" ", trim(pat.nombre), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as "NOMBRE",
	spc.nombre as "ESPECIALIDAD",
	dau.triage_fecha as "FECHA TRIAGE",
	msr.temperatura as "TEMPERATURA",
	msr.frecuencia_cardiaca as "FRECUENCIA CARDIACA",
	msr.frecuencia_respiratoria as "FRECUENCIA RESPIRATORIA",
	msr.saturacion_oxigeno as "SATURACION OXIGENO",
	msr.peso as "PESO",
	cns.descripcion as "NIVEL CONSCIENCIA",
	coalesce(nullif(dau.triage_cat_tecnico, ''), dau.triage_cat_sistema) as "CATEGORIA"
from dau
inner join paciente as pat
	on dau.paciente_id = pat.paciente_id
inner join especialidad as spc
	on dau.especialidad_id = spc.especialidad_id
left join medicion as msr
	on dau.dau_id = msr.dau_id
left join nivel_consciencia cns
	on msr.nivel_consciencia = cns.idnivel_consciencia
where dau.triage_fecha > '2014-07-04 00:00:00'

select
	dau.dau_id as "CORRELATIVO",
	spc.nombre as "ESPECIALIDAD",
	dau.triage_fecha as "FECHA TRIAGE",
	pat.rut as "RUT",
	upper(concat_ws(" ", trim(pat.nombre), trim(pat.apellido_paterno), trim(pat.apellido_materno))) as "NOMBRE PACIENTE",
	msr.temperatura as "TEMPERATURA",
	msr.frecuencia_cardiaca as "FRECUENCIA CARDIACA",
	msr.frecuencia_respiratoria as "FRECUENCIA RESPIRATORIA",
	msr.saturacion_oxigeno as "SATURACION OXIGENO",
	msr.peso as "PESO",
	cns.descripcion as "NIVEL CONSCIENCIA",
	coalesce(nullif(dau.triage_cat_tecnico, ''), dau.triage_cat_sistema) as "CATEGORIA"
from dau
inner join paciente as pat
	on dau.paciente_id = pat.paciente_id
inner join especialidad as spc
	on dau.especialidad_id = spc.especialidad_id
left join medicion as msr
	on dau.dau_id = msr.dau_id
	and msr.medicion_fecha = (
		select
			min(msr_min_date.medicion_fecha)
		from medicion as msr_min_date
		where msr.dau_id = msr_min_date.dau_id
	)
left join nivel_consciencia cns
	on msr.nivel_consciencia = cns.idnivel_consciencia
where dau.triage_fecha between '2014-01-01 00:00:00' and '2014-12-31 23:59:59'
order by dau.triage_fecha
