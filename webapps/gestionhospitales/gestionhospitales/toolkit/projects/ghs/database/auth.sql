use gestionhospitales;

drop table		if exists gh_user_role;
drop table		if exists gh_role_permission;
drop table		if exists gh_permission;
drop table		if exists gh_role;

/* ============================================================================================== */

create table gh_role (
	id				int not null auto_increment primary key,
	name			varchar(40) not null unique
);

insert into gh_role (name) values ('admin');
insert into gh_role (name) values ('byarza');
insert into gh_role (name) values ('emergencyAdmin');
insert into gh_role (name) values ('emergencyUser');
insert into gh_role (name) values ('direccion');
insert into gh_role (name) values ('medic');
insert into gh_role (name) values ('monitor');
insert into gh_role (name) values ('nurse');
insert into gh_role (name) values ('protocolUser');
insert into gh_role (name) values ('user');

create table gh_permission (
	id				int not null auto_increment primary key,
	name			varchar(128) not null unique
);

insert into gh_permission (name) values ('*');
insert into gh_permission (name) values ('adminPanel');
insert into gh_permission (name) values ('adminPanel.menu');
insert into gh_permission (name) values ('home');
insert into gh_permission (name) values ('externalMonitor');
insert into gh_permission (name) values ('emergency');
insert into gh_permission (name) values ('emergency.admin');
insert into gh_permission (name) values ('emergency.categorizer');
insert into gh_permission (name) values ('emergency.reports');
insert into gh_permission (name) values ('emergency.triage');
insert into gh_permission (name) values ('emergency.triageCleaner');
insert into gh_permission (name) values ('internalMonitor');
insert into gh_permission (name) values ('operativeProtocol');
insert into gh_permission (name) values ('operativeProtocol.cancel');
insert into gh_permission (name) values ('operativeProtocol.check');
insert into gh_permission (name) values ('operativeProtocol.check:validate');
insert into gh_permission (name) values ('operativeProtocol.check:viewPatient');
insert into gh_permission (name) values ('operativeProtocol.fill');
insert into gh_permission (name) values ('operativeProtocol.fill:viewPatient');
insert into gh_permission (name) values ('operativeProtocol.issued');
insert into gh_permission (name) values ('operativeProtocol.issued:cancel');
insert into gh_permission (name) values ('operativeProtocol.issued:check');
insert into gh_permission (name) values ('operativeProtocol.issued:view');
insert into gh_permission (name) values ('operativeProtocol.pendings');
insert into gh_permission (name) values ('operativeProtocol.pendings:cancel');
insert into gh_permission (name) values ('operativeProtocol.pendings:check');
insert into gh_permission (name) values ('operativeProtocol.pendings:fill');
insert into gh_permission (name) values ('operativeProtocol.pendings:suspend');
insert into gh_permission (name) values ('operativeProtocol.reports');
insert into gh_permission (name) values ('operativeProtocol.view');
insert into gh_permission (name) values ('operativeProtocol.view:print');
insert into gh_permission (name) values ('operativeProtocol.view:viewPatient');
insert into gh_permission (name) values ('surgicalAct');
insert into gh_permission (name) values ('surgicalOrder.suspend');

create table gh_role_permission (
	role_id			int not null,
	permission_id	int not null,
	primary key (role_id, permission_id),
	foreign key (role_id)
		references gh_role (id),
	foreign key (permission_id)
		references gh_permission (id)
);

/* ================================================================== */

insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'user'),
	(select id from gh_permission where name = 'home')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'user'),
	(select id from gh_permission where name = 'adminPanel')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'user'),
	(select id from gh_permission where name = 'adminPanel.menu')
);

/* ================================================================== */

insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'admin'),
	(select id from gh_permission where name = '*')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'byarza'),
	(select id from gh_permission where name = '*')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'direccion'),
	(select id from gh_permission where name = '*')
);

/* ================================================================== */

insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'emergencyAdmin'),
	(select id from gh_permission where name = 'emergency')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'emergencyAdmin'),
	(select id from gh_permission where name = 'emergency.admin')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'emergencyAdmin'),
	(select id from gh_permission where name = 'emergency.categorizer')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'emergencyAdmin'),
	(select id from gh_permission where name = 'emergency.reports')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'emergencyAdmin'),
	(select id from gh_permission where name = 'emergency.triage')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'emergencyAdmin'),
	(select id from gh_permission where name = 'emergency.triageCleaner')
);

/* ================================================================== */

insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'emergency')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'emergency.admin')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'emergency.categorizer')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'surgicalAct')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.cancel')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.check')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.check:validate')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.check:viewPatient')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.fill')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.fill:viewPatient')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.issued')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.issued:cancel')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.issued:check')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.issued:view')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.pendings')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:cancel')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:check')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:fill')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:suspend')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.view')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.view:print')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'operativeProtocol.view:viewPatient')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'medic'),
	(select id from gh_permission where name = 'surgicalOrder.suspend')
);

/* ================================================================== */

insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'monitor'),
	(select id from gh_permission where name = 'surgicalAct')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'monitor'),
	(select id from gh_permission where name = 'externalMonitor')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'monitor'),
	(select id from gh_permission where name = 'internalMonitor')
);

/* ================================================================== */

insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'nurse'),
	(select id from gh_permission where name = 'surgicalAct')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'nurse'),
	(select id from gh_permission where name = 'operativeProtocol')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'nurse'),
	(select id from gh_permission where name = 'operativeProtocol.pendings')
);

/* ================================================================== */

insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'surgicalAct')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.cancel')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.check')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.check:validate')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.check:viewPatient')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.fill')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.fill:viewPatient')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.issued')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.issued:cancel')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.issued:check')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.issued:view')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.pendings')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:cancel')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:check')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:fill')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.pendings:suspend')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.reports')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.view')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.view:print')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'operativeProtocol.view:viewPatient')
);
insert into gh_role_permission (role_id, permission_id) values (
	(select id from gh_role where name = 'protocolUser'),
	(select id from gh_permission where name = 'surgicalOrder.suspend')
);

create table gh_user_role (
	user_id			int not null,
	role_id			int not null,
	primary key (user_id, role_id),
	foreign key (user_id)
		references usuario (id),
	foreign key (role_id)
		references gh_role (id)
);

insert into gh_user_role (user_id, role_id)
select
	usr.id as user_id,
	rle.id as role_id
from authassignment as aas
inner join usuario as usr
	on aas.userid = usr.id
inner join gh_role as rle
	on 'user' = rle.name
union
select
	usr.id as user_id,
	rle.id as role_id
from authassignment as aas
inner join usuario as usr
	on aas.userid = usr.id
inner join gh_role as rle
	on 'medic' = rle.name
where aas.itemname in ('Becado', 'JefePabellon', 'Medico')
union
select
	usr.id as user_id,
	rle.id as role_id
from authassignment as aas
inner join usuario as usr
	on aas.userid = usr.id
inner join gh_role as rle
	on 'admin' = rle.name
where aas.itemname = 'Admin'
union
select
	usr.id as user_id,
	rle.id as role_id
from authassignment as aas
inner join usuario as usr
	on aas.userid = usr.id
inner join gh_role as rle
	on 'nurse' = rle.name
where aas.itemname = 'Enfermera'
union
select
	usr.id as user_id,
	rle.id as role_id
from authassignment as aas
inner join usuario as usr
	on aas.userid = usr.id
inner join gh_role as rle
	on 'monitor' = rle.name
where aas.itemname = 'MONITOR'
union
select
	usr.id as user_id,
	rle.id as role_id
from usuario as usr
inner join gh_role as rle
	on 'protocolUser' = rle.name
where usr.nombreusuario in ('sbascunan', 'cbrito', 'mdegiorgis', 'hherrera')
union
select
	usr.id as user_id,
	rle.id as role_id
from usuario as usr
inner join gh_role as rle
	on 'emergencyAdmin' = rle.name
where
	usr.nombreusuario in ('dmaldonado', 'jcarreno', 'jromero', 'mrojas');

/* Registros de interfaz con el sistema antiguo */
insert ignore into authitem (name, `type`, description) values (
	'emergency.admin',
	1,
	'Permisos para labores administrativas de urgencia en sistema NUEVO'
);

insert ignore into authassignment (itemname, userid)
select
	'emergency.admin' as itemname,
	usr.id
from usuario as usr
where usr.nombreusuario in ('dmaldonado', 'jcarreno', 'jromero', 'mrojas');

insert ignore into authitemchild (parent, child)
select
	'Admin' as parent,
	'emergency.admin' as child;
