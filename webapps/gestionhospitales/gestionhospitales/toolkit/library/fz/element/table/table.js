goog.require('fz');
goog.require('fz.config');
goog.require('fz.element');

goog.provide('fz.element.table');

/**
 * @enum {string}
 */
fz.element.table.ColumnType =
{
	ACTION	: 'action',
	COUNTER	: 'counter',
	LIST	: 'list',
	SELECT	: 'select'
};

/**
 * @typedef {{
 *		name	: string,
 *		title	: (?string|undefined),
 *		type	: (fz.element.table.ColumnType|undefined),
 *		list	: (Array.<!Object>|undefined),
 *		model	: (Object|undefined),
 *		offset	: (?number|undefined),
 *		style	: (?function(this:fz.element.table.Column, *):string|undefined)
 * }}
 */
fz.element.table.Column;

/**
 * @typedef {Object.<string, *>}
 */
fz.element.table.Row;

/**
 * @typedef {{
 *		name		: string,
 *		title		: (?string|undefined),
 *		filter		: (?function(!fz.element.table.Row, !fz.element.table.Action):(boolean|undefined)|undefined),
 *		callback	: (?function(fz.element.table.Row)|undefined)
 * }}
 */
fz.element.table.Action;

/**
 * @typedef {{
 *		clazz 		: (?string|undefined),
 *		columnList 	: !Array.<!fz.element.table.Column>,
 *		rowList 	: (Array.<(fz.element.table.Row|undefined)>|undefined),
 *		title 		: (?string|undefined),
 *		$even 		: (?boolean|undefined),
 *		_action 	: (fz.element.table.Action|undefined),
 *		_column 	: (fz.element.table.Column|undefined),
 *		_item 		: (Object|undefined),
 *		_itemList 	: (Array.<!Object>|undefined),
 *		_key 		: (?string|undefined),
 *		_value 		: (Array|?string|undefined),
 *		_parent 	: (Object|undefined),
 *		_row 		: (fz.element.table.Row|undefined)
 * }}
 */
fz.element.table.Scope;

/**
 * @constructor
 * @ngInject
 * @param {!fz.element.table.Scope} $scope
 */
fz.element.table.Controller = function($scope)
{
	if (!$scope.columnList)
		throw 'fz-table necesita al menos tener defenida la lista de columnas';

	for (var i = 0, column; column = $scope.columnList[i]; ++i)
	{
		if (!column.style)
			column.style = fz.element.table.Controller._getColumnClass;

		if (fz.element.table.ColumnType.SELECT === column.type && !angular.isArray(column.model))
			column.model = [];

		if (fz.element.table.ColumnType.ACTION === column.type && column.list)
		{
			for (var j = 0, action; action = /** @type {!fz.element.table.Action} */ (column.list[j]); ++j)
			{
				if (!action.callback)
				{
					console.info('La acción "%s" será eliminada ya que no tiene asociado un callback en el contexto actual', action.name);
					column.list.splice(j, 1);
					--j;
				}
			}
		}
	}

	Object.defineProperties($scope,
	{
		actionClass :
		{
			get : fz.element.table.Controller._getActionClass
		},
		cellClass :
		{
			get : fz.element.table.Controller._getCellClass
		},
		listClass :
		{
			get : fz.element.table.Controller._getListClass
		},
		rowClass :
		{
			get : fz.element.table.Controller._getRowClass
		}
	});

	$scope.initList = fz.element.table.Controller._initListScope;
	$scope.isArray = angular.isArray;
}

/**
 * @private
 * @return {string}
 * @this {fz.element.table.Scope}
 */
fz.element.table.Controller._getActionClass = function()
{
	return (this.clazz || '') + ' action ' + this._action.name;
}

/**
 * @private
 * @return {string}
 * @this {fz.element.table.Scope}
 */
fz.element.table.Controller._getCellClass = function()
{
	return (this.clazz || '') + ' ' +
		this._column.style(this._column.type !== 'select' ? this._row[this._column.name] : this._row);
}

/**
 * @private
 * @param {*} cell
 * @return {string}
 * @this {fz.element.table.Column}
 */
fz.element.table.Controller._getColumnClass = function(cell)
{
	return this.name +
		(this.type ? ' ' + this.type : '');
}

/**
 * @private
 * @return {string}
 * @this {fz.element.table.Scope}
 */
fz.element.table.Controller._getRowClass = function()
{
	return (this.$even ? 'even ' : 'odd ') +
		(this.clazz || '');
}

/**
 * @private
 * @return {string}
 * @this {fz.element.table.Scope}
 */
fz.element.table.Controller._getListClass = function()
{
	return (this._parent ? 'inner ' : 'outer ') +
		(this.clazz + ' ' || '') +
		this._column.style(this._row[this._column.name]) +
		(this._item ? ' ' + this._key : '');
}

/**
 * @private
 * @this {fz.element.table.Scope}
 */
fz.element.table.Controller._initListScope = function()
{
	this._parent = this._item;
	this._itemList = (this._item ? this._item : this._row[this._column.name]);
}

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @return {!angular.Directive}
 */
fz.element.table.directive = function(fzConfig)
{
	var scope =
	{
		/**
		 * @expose
		 * @type {string}
		 */
		clazz		: fz.element.Attrib.CLASS,

		/**
		 * @expose
		 * @type {string}
		 */
		columnList	: fz.element.Attrib.COLUMN_LIST,

		/**
		 * @expose
		 * @type {string}
		 */
		rowList		: fz.element.Attrib.ROW_LIST,

		/**
		 * @expose
		 * @type {string}
		 */
		title		: fz.element.Attrib.TITLE
	};

	return /** @type {!angular.Directive} */(
	{
		controller	: fz.element.table.Controller,
		restrict	: 'AE',
		scope		: scope,
		templateUrl	: fzConfig.get('template.pool.url') + '/fz-table.html',
		link		: function (scope, element, attrs)
		{
			element.addClass('fz-table');
			element.addClass(attrs.fzClass);
		}
	});
}

/**
 * @type {!angular.Module}
 */
fz.element.table.module = angular.module('fz.element.table',
[
	fz.config.module.name
]);

fz.element.table.module.directive(
	'fzTable',
	fz.element.table.directive
);
