goog.require('fz');
goog.require('fz.config');
goog.require('fz.element.index');

goog.provide('fz.element.breadcrumb');

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @return {!angular.Directive}
 */
fz.element.breadcrumb.directive = function(fzConfig)
{
	return /** @type {!angular.Directive} */ (
	{
		controller	: fz.element.index.Controller,
		restrict	: 'AE',
		scope		: fz.element.index.scope,
		templateUrl	: fzConfig.get('template.pool.url') + '/fz-breadcrumb.html',
		link		: function (scope, element, attrs)
		{
			element.addClass('fz-breadcrumb');
			element.addClass(attrs.fzClass);
		}
	});
}

/**
 * @type {!angular.Module}
 */
fz.element.breadcrumb.module = angular.module('fz.element.breadcrumb',
[
	fz.config.module.name
]);
fz.element.breadcrumb.module.directive(
	'fzBreadcrumb',
	fz.element.breadcrumb.directive
);
