goog.provide('fz.element');

/**
 * @enum {string}
 */
fz.element.Attrib =
{
	CLASS			: '@fzClass',
	COLUMN_LIST		: '=fzColumns',
	DESCRIPTION		: '@fzDescription',
	DIAGNOSIS_LIST	: '=fzDiaglist',
	FIELD_LIST		: '=fzFields',
	FIELD_MAP		: '=fzFields',
	ITEM_LIST		: '=fzItems',
	MAIN_MENU		: '=fzNav',
	MODEL			: '=fzModel',
	NAME			: '@fzName',
	REQUIRED		: '=fzRequired',
	RELATED_MENU	: '=fzAside',
	ROW_LIST		: '=fzRows',
	TITLE			: '@fzTitle'
}
