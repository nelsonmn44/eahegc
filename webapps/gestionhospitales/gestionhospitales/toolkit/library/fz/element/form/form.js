goog.require('fz');
goog.require('fz.config');
goog.require('fz.element');

goog.provide('fz.element.form');
goog.provide('fz.element.form.helper');

/**
 * @enum {string}
 */
fz.element.form.FieldType =
{
	DATE		: 'date',
	DIAGNOSIS	: 'diagnosis',
	FIELDGROUP	: 'fieldgroup',
	SECTION		: 'section',
	TIME		: 'time'
}

/**
 * @typedef {{
 *		title : (?string|undefined),
 *		value : ?(string|number|boolean|undefined)
 * }}
 */
fz.element.form.DataItem;

/**
 * @typedef {(Object|?function(*, number)|?string|undefined)}
 */
fz.element.form.Filter;

/**
 * @typedef {{
 *		name		: string,
 *		title		: (?string|undefined),
 *		type		: (?string|undefined),
 *		onType		: (?string|undefined),
 *		data		: (Array.<!fz.element.form.DataItem>|undefined),
 *		disabled	: (?boolean|undefined),
 *		filter		: (fz.element.form.Filter|undefined),
 *		readonly	: (?boolean|undefined),
 *		required	: (?boolean|undefined),
 *		value		: *
 * }}
 */
fz.element.form.Field;

/**
 * @typedef {{
 *		control 	: !angular.FormController,
 *		dataMap		: (Object.<string, Array.<!fz.element.form.DataItem>>|undefined),
 *		fieldList	: (Array.<!fz.element.form.Field>|undefined),
 *		filterMap	: (Object.<string, !fz.element.form.Filter>|undefined),
 *		valueMap	: (Object.<string, *>|undefined)
 * }}
 */
fz.element.form.Form;

/**
 * @typedef {{
 *		clazz		: (?string|undefined),
 *		fieldList	: (Array.<!fz.element.form.Field>|undefined),
 *		model		: (Object.<string, *>|undefined),
 *		aheadLabel	: function((string|number)):?(string|number|undefined),
 *		tagClass	: function():string,
 *		_field		: (fz.element.form.Field|undefined),
 *		_parent		: (Object|undefined)
 * }}
 */
fz.element.form.Scope;

/**
 * @constructor
 * @ngInject
 * @param {!fz.element.form.Scope} $scope
 */
fz.element.form.Controller = function($scope)
{
	Object.defineProperty($scope, 'tagClass',
	{
		/**
		 * @private
		 * @return {string}
		 * @this {fz.element.form.Scope}
		 */
		get : function()
		{
			return (this._parent ? 'inner ' : 'outer ') +
				(this.clazz || '') +
				(this._field ? ' ' + this._field.name : '') +
				(this._field && this._field.disabled ? ' disabled' : '') +
				(this._field && this._field.readonly ? ' readonly' : '') +
				(this._field && this._field.required ? ' required' : '');
		}
	});

	$scope.aheadLabel = function(model)
	{
		for (var i = 0, item; item = this._field.data[i]; ++i)
		{
			if (model == item.value)
				return item.title;
		}
	}
}

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @return {!angular.Directive}
 */
fz.element.form.directive = function(fzConfig)
{
	var scope =
	{
		/**
		 * @expose
		 * @type {string}
		 */
		clazz		: fz.element.Attrib.CLASS,

		/**
		 * @expose
		 * @type {string}
		 */
		fieldList	: fz.element.Attrib.FIELD_LIST,

		/**
		 * @expose
		 * @type {string}
		 */
		model		: fz.element.Attrib.MODEL
	};

	return /** @type {!angular.Directive} */ (
	{
		controller	: fz.element.form.Controller,
		restrict	: 'AE',
		scope		: scope,
		templateUrl	: fzConfig.get('template.pool.url') + '/fz-form.html',
		link		: function (scope, element, attrs)
		{
			element.addClass('fz-form');
			element.addClass(attrs.fzClass);
		}
	});
}

/**
 * @type {!angular.Module}
 */
fz.element.form.module = angular.module('fz.element.form',
[
	fz.Cnt.UI_BOOTSTRAP,
	fz.config.module.name
]);

fz.element.form.module.directive(
	'fzForm',
	fz.element.form.directive
);

/**
 * @param {(fz.element.form.Form|undefined)} form
 */
fz.element.form.helper.init = function(form)
{
	if (!form)
	{
		console.warn('Procesamiento de formulario abortado debido a datos vacíos');
		return;
	}
	else if (!form.fieldList)
	{
		console.warn('Procesamiento de formulario abortado debido a lista de campos vacío');
		return;
	}

	/**
	 * @type {function(!fz.element.form.Field, *)}
	 */
	var initField = function(field, valueMap)
	{
		if (valueMap[field.name])
		{
			switch (field.onType || field.type)
			{
				case fz.element.form.FieldType.DATE :
				{
					var date = new Date(valueMap[field.name]);
					date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
					valueMap[field.name] = date;
					break;
				}
				case fz.element.form.FieldType.TIME :
				{
					var time = valueMap[field.name].split(':');
					valueMap[field.name] = new Date(1970, 0, 1);
					valueMap[field.name].setHours(time[0], time[1], time[2]);
					break;
				}
				case fz.element.form.FieldType.SECTION :
				{
					for (var i = 0, subField; subField = field.value[i]; ++i)
						initField(subField, valueMap[field.name]);
				}
			}
		}

		if (field.data)
		{
			if (form.dataMap)
			{
				var dataList = form.dataMap[/** @type {?} */ (field.data)];
				if (dataList)
					field.data = dataList;
				else
					console.warn('No se encuentra el origen de datos "%s" para el campo "%s"', field.data, field.name);
			}
			else
				console.warn('Se declaró el uso del origen de datos "%s", pero el mapa de origenes de datos está vacío', field.data);
		}

		if (field.filter)
		{
			if (form.filterMap)
			{
				var filter = form.filterMap[/** @type {?} */ (field.filter)];
				if (filter)
				{
					if (angular.isFunction(filter))
						filter = angular.bind(field, /** @type {!Function} */ (filter));

					field.filter = filter;
				}
				else
					console.warn('No se encuentra el filtro "%s" para el campo "%s"', field.filter, field.name);
			}
			else
				console.warn('Se declaró el uso del filtro "%s", pero el mapa de filtros está vacío', field.filter);
		}
	}

	for (var i = 0, field; field = form.fieldList[i]; ++i)
		initField(field, form.valueMap);
}

/**
 * @param {(fz.element.form.Form|undefined)} form
 * @return {(Object.<string, *>|undefined)}
 */
fz.element.form.helper.format = function(form)
{
	if (!form)
	{
		console.warn('Procesamiento de formulario abortado debido a datos vacíos');
		return;
	}
	else if (!form.fieldList)
	{
		console.warn('Procesamiento de formulario abortado debido a lista de campos vacío');
		return;
	}
	else if (!form.valueMap)
	{
		console.warn('Procesamiento de formulario abortado debido a mapa de valores vacío');
		return;
	}

	/**
	 * @type {function(!fz.element.form.Field, *)}
	 */
	var formatValue = function(field, valueMap)
	{
		if (valueMap[field.name])
		{
			switch (field.onType || field.type)
			{
				case fz.element.form.FieldType.DATE :
				{
					/**
					 * @type {!Date}
					 */
					var date = /** @type {!Date} */ (valueMap[field.name]);

					valueMap[field.name] =
						date.getFullYear() + '-' + (1 + date.getMonth()) + '-' + date.getDate();

					break;
				}
				case fz.element.form.FieldType.TIME :
				{
					/**
					 * @type {!Date}
					 */
					var time = /** @type {!Date} */ (valueMap[field.name]);

					valueMap[field.name] =
						time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds();

					break;
				}
				case fz.element.form.FieldType.SECTION :
				{
					for (var i = 0, subField; subField = field.value[i]; ++i)
						formatValue(subField, valueMap[field.name]);
				}
			}
		}
	}

	for (var i = 0, field; field = form.fieldList[i]; ++i)
		formatValue(field, form.valueMap);
}

