goog.require('fz.element');

goog.provide('fz.element.index');

/**
 * @enum {string}
 */
fz.element.index.Trait =
{
	ACTION		: 'action',
	DUMMY		: 'dummy',
	SEPARATOR	: 'separator',
	STATE		: 'state',
	HREF		: 'href'
}

/**
 * @typedef {{
 *		name		: (?string|undefined),
 *		title		: (?string|undefined),
 *		target		: (?string|Function|undefined),
 *		trait		: !fz.element.index.Trait,
 *		current		: (?boolean|undefined),
 *		disabled	: (?boolean|undefined),
 *		selected	: (?boolean|undefined)
 *	}}
 */
fz.element.index.Item;

/**
 * @todo: Eliminar casting
 * @type {!fz.element.index.Item}
 */
fz.element.index.Separator = /** @type {!fz.element.index.Item} */ (
{
	trait : fz.element.index.Trait.SEPARATOR,
});

/**
 * @constructor
 * @ngInject
 * @param {!ui.router.$state} $state
 * @param {!angular.Scope} $scope
 */
fz.element.index.Controller = function($state, $scope)
{
	Object.defineProperty($scope, 'itemType',
	{
		get : fz.element.index.Controller._getItemType
	});
	Object.defineProperty($scope, 'tagClass',
	{
		get : fz.element.index.Controller._getTagClass
	});
}

/**
 * @private
 * @return {string}
 * @this {{_item : !fz.element.index.Item}}
 */
fz.element.index.Controller._getItemType = function()
{
	if (this._item.current || this._item.disabled || !this._item.trait)
		return fz.element.index.Trait.DUMMY;
	else
		return this._item.trait;
}

/**
 * @private
 * @return {string}
 * @this {{clazz : string, _item : !fz.element.index.Item}}
 */
fz.element.index.Controller._getTagClass = function()
{
	return (this._item.disabled ? 'disabled ' : '') +
		(this._item.selected ? 'selected ' : '') +
		this.clazz + ' ' +
		(this._item.trait !== fz.element.index.Trait.SEPARATOR ? this._item.name : this._item.trait);
}

fz.element.index.scope =
{
	/**
	 * @expose
	 * @type {string}
	 */
	clazz		: fz.element.Attrib.CLASS,

	/**
	 * @expose
	 * @type {string}
	 */
	itemList	: fz.element.Attrib.ITEM_LIST,

	/**
	 * @expose
	 * @type {string}
	 */
	title		: fz.element.Attrib.TITLE
};
