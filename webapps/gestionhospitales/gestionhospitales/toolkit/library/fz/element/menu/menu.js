goog.require('fz');
goog.require('fz.element.index');
goog.require('fz.node');

goog.provide('fz.element.menu');
goog.provide('fz.element.menu.helper');

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @return {!angular.Directive}
 */
fz.element.menu.directive = function(fzConfig)
{
	return /** @type {!angular.Directive} */(
	{
		controller	: fz.element.index.Controller,
		restrict	: 'AE',
		scope		: fz.element.index.scope,
		templateUrl	: fzConfig.get('template.pool.url') + '/fz-menu.html',
		link		: function (scope, element, attrs)
		{
			element.addClass('fz-menu');
			element.addClass(attrs.fzClass);
		}
	});
}

/**
 * @type {!angular.Module}
 */
fz.element.menu.module = angular.module('fz.element.menu',
[
	fz.Cnt.UI_ROUTER,
	fz.config.module.name
]);

fz.element.menu.module.directive(
	'fzMenu',
	fz.element.menu.directive
);

/**
 * Utiliza los items de configuración de nodo como items de menú, ya que la
 * interfáz de ambos es similar. Siempre hay que tener en cuenta este factor a
 * la hora de hacer modificaciones en los items de menú, ya que internamente
 * se estaría modificando también la configuración del nodo, siendo esta última
 * mucho más estática que la primera.
 *
 * @param {(Array.<(!fz.node.Config|string)>|undefined)} source
 * @param {Object} context
 * @return {(!Array.<!fz.element.index.Item>|undefined)}
 */
fz.element.menu.helper.init = function(source, context)
{
	if (!source)
		return;

	for (var i = 0, node; node = source[i]; ++i)
	{
		var item = /** @type {!fz.element.index.Item} */ (source[i]);
		if (node.action && context)
		{
			if (context[node.action])
				item.target	= angular.bind(context, context[node.action]);
			else
				console.warn(
					'La función "%s" para la acción "%s" no existe en el contexto "%o"',
					node.action,
					node.name,
					context
				);

			item.trait	= fz.element.index.Trait.ACTION;
		}
		else if (node.state)
		{
			item.target	= node.state.name;
			item.trait	= fz.element.index.Trait.STATE;
		}
		else if (node.href)
		{
			item.target	= node.href;
			item.trait	= fz.element.index.Trait.HREF;
		}
		else if (fz.node.Dummy === node)
			item.trait	= fz.element.index.Trait.SEPARATOR;
	}

	return /** @type {(!Array.<!fz.element.index.Item>|undefined)} */ (source);
}
