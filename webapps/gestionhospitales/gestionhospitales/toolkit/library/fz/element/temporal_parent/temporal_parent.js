goog.provide('fz.element.temporalParent');

/**
 * @ngInject
 * @return {!angular.Directive}
 */
fz.element.temporalParent.directive = function()
{
	return /** @type {!angular.Directive} */ (
	{
		restrict	: 'AE',
		link		: function (scope, element, attrs)
		{
			element.replaceWith(element.children());
		}
	});
}

/**
 * @type {!angular.Module}
 */
fz.element.temporalParent.module = angular.module('fz.element.temporalParent', []);

fz.element.temporalParent.module.directive(
	'fzTemporalParent',
	fz.element.temporalParent.directive
);
