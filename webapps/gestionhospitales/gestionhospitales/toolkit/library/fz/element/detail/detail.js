goog.require('fz.element.temporalParent');

goog.provide('fz.element.detail');

/**
 * @typedef {{
 *		clazz		: (?string|undefined),
 *		init		: function(),
 *		itemList	: (Array.<!Object>|undefined),
 *		title		: (?string|undefined),
 *		inner		: function():boolean,
 *		tagClass	: function():string,
 *		_item		: (Object|undefined),
 *		_itemList	: (Array.<!Object>|undefined),
 *		_parent		: (Object|undefined)
 * }}
 */
fz.element.detail.Scope;

/**
 * @constructor
 * @ngInject
 * @param {!fz.element.detail.Scope} $scope
 */
fz.element.detail.Controller = function($scope)
{
	Object.defineProperties($scope,
	{
		inner :
		{
			/**
			 * @this {fz.element.detail.Scope}
			 */
			get : function()
			{
				return this._item && angular.isArray(this._item.value);
			}
		},
		tagClass :
		{
			/**
			 * @this {fz.element.detail.Scope}
			 */
			get : function()
			{
				return (this._parent ? 'inner ' : 'outer ') +
					(this.clazz || '') +
					(this._item ? ' ' + this._item.name : '');
			}
		}
	});

	$scope.init = function()
	{
		this._parent = this._item;
		this._itemList = this._item && angular.isArray(this._item.value) ? this._item.value : this.itemList;
	}
}

/**
 * @ngInject
 * @param {!fz.config.Config} fzConfig
 * @return {!angular.Directive}
 */
fz.element.detail.directive = function(fzConfig)
{
	var scope =
	{
		/**
		 * @expose
		 * @type {string}
		 */
		clazz		: fz.element.Attrib.CLASS,

		/**
		 * @expose
		 * @type {string}
		 */
		itemList	: fz.element.Attrib.ITEM_LIST,

		/**
		 * @expose
		 * @type {string}
		 */
		title		: fz.element.Attrib.TITLE
	};

	return /** @type {!angular.Directive} */(
	{
		controller	: fz.element.detail.Controller,
		restrict	: 'AE',
		scope		: scope,
		templateUrl	: fzConfig.get('template.pool.url') + '/fz-detail.html',
		link		: function (scope, element, attrs)
		{
			element.addClass('fz-detail');
			element.addClass(attrs.fzClass);
		}
	});
}

/**
 * @type {!angular.Module}
 */
fz.element.detail.module = angular.module('fz.element.detail',
[
	fz.config.module.name,
	fz.element.temporalParent.module.name,
]);

fz.element.detail.module.directive(
	'fzDetail',
	fz.element.detail.directive
);
