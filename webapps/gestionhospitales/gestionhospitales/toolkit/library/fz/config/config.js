goog.provide('fz.config');

/**
 * @typedef {Object.<string, (Object|?string|undefined)>}
 */
fz.config.Settings;

/**
 * @constructor
 * @param {!fz.config.Settings} settings
 */
fz.config.Config = function(settings)
{
	/**
	 * @protected
 	 * @type {!fz.config.Settings}
	 */
	this.settings = settings;
}

/**
 * @param {(?string|undefined)} key
 * @param {(Object|?string)=} unfault
 * @return {(Object|?string|undefined)}
 */
fz.config.Config.prototype.get = function(key, unfault)
{
	if (!this.settings.hasOwnProperty(key))
		return unfault;
	else
		return this.settings[/** @type {string} */ (key)];
}

/**
 * @param {(?string|undefined)} name
 * @return {Object|undefined}
 */
fz.config.Config.prototype.getNode = function(name)
{
	return this.settings['node.map'][name];
}

/**
 * @param {(fz.config.Settings|undefined)} settings
 */
fz.config.Config.prototype.merge = function(settings)
{
	if (settings)
		angular.extend(this.settings, settings);
}

/**
 * @param {(?string|undefined)} key
 * @param {(Object|?string|undefined)} value
 */
fz.config.Config.prototype.set = function(key, value)
{
	if (!key)
		return;

	return this.settings[key] = value;
}

/**
 * @private
 * @type {!fz.config.Settings}
 */
fz.config._settings =
{
	'template.default'	: '<ui-view></ui-view>',
	'template.pool.url'	: ''
}

/**
 * @type {!angular.Module}
 */
fz.config.module = angular.module('fz.config', []);

fz.config.module.constant(
	'fzConfig', new fz.config.Config(fz.config._settings)
);
