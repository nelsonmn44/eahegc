goog.require('fz');
goog.require('fz.auth');
goog.require('fz.element.index');
goog.require('fz.element.menu.helper');
goog.require('fz.node');

goog.provide('fz.site.Controller');

/**
 * @enum {number}
 */
fz.site.Trait =
{
	FRAME	: 0,
	PANEL	: 1,
	PLAIN	: 2,
	CUSTOM	: 3
}

/**
 * @abstract
 * @constructor
 * @ngInject
 * @param {!fz.node.Config} config
 * @param {!fz.site.Trait} traits
 * @param {!fz.auth.Auth} fzAuth
 * @param {!fz.node.Node} fzNode
 * @param {!angular.Scope} $scope
 * @param {!angular.$window} $window
 */
fz.site.Controller = function(config, traits, fzAuth, fzNode, $scope, $window)
{
	if (!config)
		throw 'No se puede crear un site.Controller sin una configuración de nodo válida';

	/**
	 * @protected
	 * @type {!fz.node.Config}
	 */
	this.config = config;

	/**
	 * @protected
	 * @type {!fz.site.Trait}
	 */
	this.traitFlags = traits;

	this.fzAuth 	= fzAuth;

	this.fzNode 	= fzNode;

	/**
	 * @protected
	 * @type {!angular.Scope}
	 */
	this.$scope = $scope;

	this.$window = $window;

	/**
	 * @type {!fz.site.Controller}
	 */
	$scope.site = this;

	/**
	 * @protected
	 * @type {(Array.<!fz.element.index.Item>|undefined)}
	 */
	this.mainItemList;

	/**
	 * @protected
	 * @type {(Array.<!fz.element.index.Item>|undefined)}
	 */
	this.relatedItemList;

	angular.bind($scope, this._initScope)();
	console.info('Instancia de site.Controller creada para el sitio "%s"', this.name);
}

/**
 * @private
 * @this {!angular.Scope}
 */
fz.site.Controller.prototype._initScope = function()
{
	this.box.name		= this.site.name;
	this.box.error		=
	this.box.warning	= fz.VOID;

	switch (this.site.traits)
	{
		case fz.site.Trait.FRAME :
		{
			this.box.title			= this.site.title;
			this.box.description	= this.site.description;
			break;
		}
		case fz.site.Trait.PLAIN :
		{
			this.box.title			=
			this.box.description	= fz.VOID;
		}
	}
}

Object.defineProperties(fz.site.Controller.prototype,
{
	name :
	{
		/**
	 	 * @return {string}
	 	 * @this {fz.site.Controller}
	 	 */
		get : function()
		{
			return this.config.name;
		}
	},
	title :
	{
		/**
	 	 * @return {(?string|undefined)}
	 	 * @this {fz.site.Controller}
	 	 */
		get : function()
		{
			return this.config.title;
		}
	},
	description :
	{
		/**
	 	 * @return {(?string|undefined)}
	 	 * @this {fz.site.Controller}
	 	 */
		get : function()
		{
			return this.config.description;
		}
	},
	traits :
	{
		/**
	 	 * @return {!fz.site.Trait}
	 	 * @this {fz.site.Controller}
	 	 */
		get : function()
		{
			return this.traitFlags;
		}
	},
	mainMenu :
	{
		/**
	 	 * @return {(Array.<!fz.element.index.Item>|undefined)}
	 	 * @this {fz.site.Controller}
	 	 */
		get : function()
		{
			return this.mainItemList;
		}
	},
	relatedMenu :
	{
		/**
	 	 * @return {(Array.<!fz.element.index.Item>|undefined)}
	 	 * @this {fz.site.Controller}
	 	 */
		get : function()
		{
			return this.relatedItemList;
		}
	}
});

/**
 * @protected
 */
fz.site.Controller.prototype.initMenu = function()
{
	this.mainItemList		= fz.element.menu.helper.init(
		this.config.mainMenu,
		this.$scope
	);
	this.relatedItemList	= fz.element.menu.helper.init(
		this.config.relatedMenu,
		this.$scope
	);

	switch (this.traitFlags)
	{
		case fz.site.Trait.FRAME :
			this.$scope.box.mainMenu	= this.mainItemList;
		case fz.site.Trait.PANEL :
		{
			this.$scope.box.relatedMenu	= this.relatedItemList;
			break;
		}
		case fz.site.Trait.PLAIN :
		{
			this.$scope.box.mainMenu	=
			this.$scope.box.relatedMenu	= fz.VOID;
		}
	}
}

/**
 * @protected
 * @type {!fz.auth.Auth}
 */
fz.site.Controller.prototype.auth;

/**
 * @protected
 * @type {!fz.node.Node}
 */
fz.site.Controller.prototype.fzNode;

/**
 * @protected
 * @type {!angular.$window}
 */
fz.site.Controller.prototype.$window;

/**
 * @protected
 * @param {(?string|undefined)} url
 */
fz.site.Controller.prototype.leaveTo = function(url)
{
	if (url)
		this.$window.location.href = url;
	else
		console.warn('URL de redirección inválida');
}
