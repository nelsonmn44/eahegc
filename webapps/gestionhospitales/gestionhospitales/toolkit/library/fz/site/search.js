goog.require('fz');

goog.provide('fz.site.Search');

/**
 * @constructor
 * @param {function(?(string|number)=)=} runCallback
 * @param {function()=} cleanCallback
 * @param {?(string|number)=} model
 */
fz.site.Search = function(runCallback, cleanCallback, model)
{
	/**
	 * @private
	 * @type {(function(?(string|number)=)|undefined)}
	 */
	this._run = runCallback;

	/**
	 * @private
	 * @type {(function()|undefined)}
	 */
	this._clean = cleanCallback;

	/**
	 * @private
	 * @type {boolean}
	 */
	this._dirty = false;

	/**
	 * @private
	 * @type {boolean}
	 */
	this._updated = true;

	/**
	 * @private
	 * @type {?(string|number|undefined)}
	 */
	this._model = model;

	/**
	 * @type {(angular.FormController|undefined)}
	 */
	this.form;
}

Object.defineProperties(fz.site.Search.prototype,
{
	dirty :
	{
		/**
	 	 * @return {boolean}
	 	 * @this {fz.site.Search}
	 	 */
		get : function()
		{
			return this._dirty;
		}
	},
	model :
	{
		/**
	 	 * @return {?(string|number|undefined)}
	 	 * @this {fz.site.Search}
	 	 */
		get : function()
		{
			return this._model;
		},

		/**
	 	 * @param {?(string|number)=} model
	 	 * @this {fz.site.Search}
	 	 */
		set : function(model)
		{
			this._model		= model;
			this._updated	= false;
		}
	},
	updated :
	{
		/**
	 	 * @return {boolean}
	 	 * @this {fz.site.Search}
	 	 */
		get : function()
		{
			return this._updated;
		}
	}
});

/**
 * @param {*} event
 */
fz.site.Search.prototype.command = function(event)
{
	if (fz.Key.ENTER === event.keyCode && this._model)
		this.run();
}

/**
 *
 */
fz.site.Search.prototype.run = function()
{
	this._run && this._run(this._model);
	this._dirty		= true;
	this._updated	= true;
}

/**
 *
 */
fz.site.Search.prototype.clean = function()
{
	this._clean && this._clean();
	this._dirty		= false;
	this._updated	= true;
	this._model		= fz.VOID;
}
