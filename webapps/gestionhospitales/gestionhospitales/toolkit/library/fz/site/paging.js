goog.require('fz');

goog.provide('fz.site.Paging');

/**
 * @constructor
 * @param {number} size
 * @param {function(number)=} previousCallback
 * @param {function(number)=} nextCallback
 * @param {number=} index
 */
fz.site.Paging = function(size, previousCallback, nextCallback, index)
{
	/**
	 * @private
	 * @type {number}
	 */
	this._size = size;

	/**
	 * @private
	 * @type {(function(number)|undefined)}
	 */
	this._previous = previousCallback;

	/**
	 * @private
	 * @type {(function(number)|undefined)}
	 */
	this._next = nextCallback;

	/**
	 * @private
	 * @type {number}
	 */
	this._index = index || 0;

	/**
	 * @private
	 * @type {(number|undefined)}
	 */
	this._length;
}

Object.defineProperties(fz.site.Paging.prototype,
{
	hasNext :
	{
		/**
	 	 * @return {boolean}
	 	 * @this {fz.site.Paging}
	 	 */
		get : function()
		{
			return 0 !== this._length && this._size === this._length;
		}
	},
	hasPrevious :
	{
		/**
	 	 * @return {boolean}
	 	 * @this {fz.site.Paging}
	 	 */
		get : function()
		{
			return 0 < this._index;
		}
	},
	index :
	{
		/**
	 	 * @return {number}
	 	 * @this {fz.site.Paging}
	 	 */
		get : function()
		{
			return this._index;
		}
	}
});

/**
 *
 */
fz.site.Paging.prototype.previous = function()
{
	if (0 < this._index)
	{
		--this._index;
		this._previous && this._previous(this._index);
	}
}

/**
 *
 */
fz.site.Paging.prototype.next = function()
{
	++this._index;
	this._next && this._next(this._index);
}

/**
 * @param {number} length
 * @param {number=} index
 */
fz.site.Paging.prototype.update = function(length, index)
{
	this._length	= length;
	this._index		= /** @type {number} */ (fz.isVoid(index) ? this._index : index);
}
