goog.require('fz.node');

goog.provide('fz.site.Action');

/**
 * @constructor
 * @param {!Object.<string, !Object>} staticMap
 * @param {(?Object.<string, !fz.node.Config>|undefined)} dinamicMap
 */
fz.site.Action = function(staticMap, dinamicMap)
{
	/**
	 * @private
	 * @type {!Object.<string, !Object>}
	 */
	this._staticMap		= staticMap;

	/**
	 * @private
	 * @type {(?Object.<string, !fz.node.Config>|undefined)}
	 */
	this._dinamicMap	= dinamicMap;
}

/**
 * @param {string} name
 * @return {!fz.node.Config}
 */
fz.site.Action.prototype.get = function(name)
{
	if (!this._dinamicMap[name])
		return {name : name, disabled : true};
	else
		return this._dinamicMap[name];
}

/**
 * @param {!Object} context
 * @return {!fz.site.Action}
 */
fz.site.Action.prototype.filter = function(context)
{
	for (var i = 0, action; action = Object.keys(this._staticMap)[i]; ++i)
	{
		if (!this._dinamicMap || !this._dinamicMap[action])
		{
			var callback = 'do' + action[0].toUpperCase() + action.slice(1);
			if (context[callback])
				delete context[callback];
			else
				console.warn(
					'La función "%s" para la acción "%s" no existe en el contexto "%o"',
					callback,
					action,
					context
				);
		}
	}

	return this;
}
