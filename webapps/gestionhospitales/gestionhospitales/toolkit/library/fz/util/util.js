goog.provide('fz.util');

/**
 * @param {string} string
 * @return {string}
 */
fz.util.toRoute = function(string)
{
	return string.replace(/([A-Z]|\.)/g, function(match)
	{
		if ('.' === match)
			return '-';
		else
			return '_' + match.toLowerCase();
	});
}

/**
 * @param {(Object.<string, *>|undefined)} map
 * @return {(?string|undefined)}
 */
fz.util.toQueryString = function(map)
{
	if (!map)
		return;

	var query = '';
	for (var i = 0, key; key = Object.keys(map)[i]; ++i)
		query += (0 === i ? '?' : '&') + key + '='+ map[key];

	if ('' !== query)
		return query;
}
