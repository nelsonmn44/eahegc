goog.require('fz.config');

goog.provide('fz.auth');

/**
 * @ngInject
 * @constructor
 * @param {!fz.config.Config} fzConfig
 */
fz.auth.Auth = function(fzConfig)
{
	/**
	 * @param {(?string|undefined)} permission
	 * @return {boolean}
	 */
	this.check = function(permission)
	{
		var bounds = fzConfig.get('bounds');
		if (bounds['*'])
			return true;

		return bounds && bounds[permission] ? true : false;
	}
}

/**
 * @type {!angular.Module}
 */
fz.auth.module = angular.module('fz.auth',
[
	fz.config.module.name
]);

fz.auth.module.service('fzAuth', fz.auth.Auth);
