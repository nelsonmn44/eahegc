goog.provide('fz.state');

/**
 * @typedef {{
 *		abstract	: (?boolean|undefined),
 *		controller	: Function,
 *		params		: (Object.<string, string>|undefined),
 *		parent		: (fz.state.Config|string|undefined),
 *		template	: (?string|Function),
 *		templateUrl	: (?string|undefined),
 *		url			: (?string|undefined)
 *	}}
 */
fz.state.Config;
