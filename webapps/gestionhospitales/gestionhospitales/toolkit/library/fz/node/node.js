goog.require('fz');
goog.require('fz.state');
goog.require('fz.util');

goog.provide('fz.node');

/**
 * @typedef {{
 *		name		: string,
 *		title		: (?string|undefined),
 *		description	: (?string|undefined),
 *		disabled	: (?boolean|undefined),
 *		actionMap	: (Object.<string, !fz.node.Config>|undefined),
 *		action		: (?string|undefined),
 *		href		: (?string|undefined),
 *		state		: (fz.state.Config|undefined),
 *		mainMenu	: (Array.<(!fz.node.Config|string)>|undefined),
 *		redirectTo	: (fz.node.Config|?string|undefined),
 *		relatedMenu	: (Array.<!fz.node.Config|string>|undefined)
 *	}}
 */
fz.node.Config;

/**
 * @type {!fz.node.Config}
 */
fz.node.Dummy = /** @type {!fz.node.Config} */ (
{
	name : '-'
});

/**
 * @typedef {{
 *		activate	: function((?string|undefined), Object=),
 *		get			: function((?string|undefined)) : (!fz.node.Config|undefined),
 *		paramMap	: function():(Object.<string, string>|undefined)
 *	}}
 */
fz.node.Node;

/**
 * @constructor
 * @ngInject
 * @param {!ui.router.$stateProvider} $stateProvider
 */
fz.node.NodeProvider = function($stateProvider)
{
	/**
	 * @type {!Object.<string, !fz.node.Config>}
	 */
	this._nodeMap = {};

	/**
	 * @private
	 * @type {!ui.router.$stateProvider}
	 */
	this._$stateProvider = $stateProvider;
}

/**
 * @ngInject
 * @param {!ui.router.$state} $state
 * @param {!angular.$window} $window
 * @return {!fz.node.Node}
 */
fz.node.NodeProvider.prototype.$get = function($state, $window)
{
	if (this._dirty)
	{
		console.warn('No se realizó la revisión del mapa de nodos previa ejecución de la aplicación. Para evitar posibles malfunciones, se realizará ahora');
		this._checkTree();
	}

	/**
	 * @private
	 * @type {!ui.router.$state}
	 */
	this._$state = $state;

	/**
	 * @private
	 * @type {!angular.$window}
	 */
	this._$window = $window;

	return /** @type {!fz.node.Node} */ (this);
}

/**
 * @param {!fz.node.Config} node
 * @param {!Object.<string, !fz.node.Config>} actionMap
 * @private
 */
fz.node.NodeProvider.prototype._checkActions = function(node, actionMap)
{
	for (var i = 0, action; action = Object.keys(actionMap)[i]; ++i)
	{
		actionMap[action]['name']	= action;
		actionMap[action]['action']	= actionMap[action]['action'] ||
			'do' + action[0].toUpperCase() + action.slice(1);
	}
}

/**
 * @param {!fz.node.Config} node
 * @param {!Array.<(!fz.node.Config|string)>} itemList
 * @private
 */
fz.node.NodeProvider.prototype._expandMenu = function(node, itemList)
{
	for (var i = 0, item; item = /** @type {string} */ (itemList[i]); ++i)
	{
		if (fz.node.Dummy.name === item)
			itemList[i] = fz.node.Dummy;
		else if(':' === item[0])
		{
			var name = item.substring(1);
			var action = (node.actionMap ? node.actionMap[name] : fz.VOID);

			if (action)
				itemList[i] = action;
			else
			{
				console.warn('El item de menu "%s" del nodo "%s" será eliminado debido a que no existe dentro del mapa de acciones del nodo',
					item,
					node.name
				);
				itemList.splice(i, 1);
				--i;
			}
		}
		else if (this._nodeMap[item])
			itemList[i] = this._nodeMap[item];
		else
		{
			console.warn('El item de menu "%s" será eliminado ya que no existe dentro del mapa de nodos', item);
			itemList.splice(i, 1);
			--i;
		}
	}
}

/**
 * @param {!fz.node.Config} node
 * @private
 */
fz.node.NodeProvider.prototype._expandRedirection = function(node)
{
	if (this._nodeMap[/** @type {string} */ (node.redirectTo)])
		node.redirectTo = this._nodeMap[/** @type {string} */ (node.redirectTo)];
	else
	{
		console.warn('La redirección "%s" del nodo "%s" será eliminada ya que no existe dentro del mapa de nodos', node.redirectTo, node.name);
		delete node.redirectTo;
	}
}

/**
 * @private
 */
fz.node.NodeProvider.prototype._checkTree = function()
{
	for (var i = 0, node; node = Object.keys(this._nodeMap)[i]; ++i)
	{
		if (this._nodeMap[node].actionMap)
			this._checkActions(
				this._nodeMap[node],
				/** @type {!Object.<string, !fz.node.Config>} */ (this._nodeMap[node].actionMap)
			);

		if (this._nodeMap[node].mainMenu)
			this._expandMenu(
				this._nodeMap[node],
				/** @type {!Array.<(!fz.node.Config|string)>} */ (this._nodeMap[node].mainMenu)
			);

		if (this._nodeMap[node].relatedMenu)
			this._expandMenu(
				this._nodeMap[node],
				/** @type {!Array.<(!fz.node.Config|string)>} */ (this._nodeMap[node].relatedMenu)
			);

		if (this._nodeMap[node].redirectTo)
			this._expandRedirection(this._nodeMap[node]);
	}
}

/**
 * @private
 * @type {boolean}
 */
fz.node.NodeProvider.prototype._dirty = false;

/**
 * @param {(?string|undefined)} targetName
 * @param {Object=} paramMap
 */
fz.node.NodeProvider.prototype.activate = function(targetName, paramMap)
{
	if (!targetName)
		return;

	var target = this._nodeMap[targetName];
	if (target)
		if (target.state)
			this._$state.go(target.state, paramMap);
		else if (target.href)
			this._$window.location.href = target.href;
	else
		console.warn('No se encontró el nodo "%s" dentro del mapa de nodos', targetName);
}

/**
 * @return true si el ciclo de chequeo fue ejecutado
 */
fz.node.NodeProvider.prototype.check = function()
{
	var dirty = this._dirty;
	if (this._dirty)
	{
		this._checkTree();
		this._dirty = false;
	}

	return dirty !== this._dirty;
}

Object.defineProperty(fz.node.NodeProvider.prototype, 'paramMap',
{
	/**
	 * @return {(Object.<string, *>|undefined)}
	 * @this {fz.node.NodeProvider}
	 */
	get : function()
	{
		return this._$state.params;
	}
});

/**
 * @param {(fz.node.Config|undefined)} target
 * @param {(Object|undefined)} source
 */
fz.node.NodeProvider.prototype.addMerged = function(target, source)
{
	if (!target || !source)
	{
		target && console.info('El nodo "%s" no será incluido debido a que no se encuentra en la configuración importada', target.name);
		return;
	}

	/**
	 * Lista de campos personalizables desde fuera
	 */
	var fieldList =
	[
		'title',
		'description',
		'disabled',
		'href',
		'mainMenu',
		'redirectTo',
		'relatedMenu',
		'actionMap'
	];

	for (var i = 0, field; field = fieldList[i]; ++i)
	{
		if (source[field])
			target[field] = source[field];
	}

	if (target.state)
	{
		target.state.node = target;

		if (angular.isString(target.state.parent))
			target.state.parent = fz.util.toRoute(
				/** @type {string} */ (target.state.parent)
		);

		this._$stateProvider.state(
			fz.util.toRoute(target.name),
			/** @type {!Object} */ (target.state)
		);
	}

	this._nodeMap[target.name] = target;
	this._dirty = true;
}

/**
 * @param {(?string|undefined)} name
 * @return {(!fz.node.Config|undefined)}
 */
fz.node.NodeProvider.prototype.get = function(name)
{
	if (name)
		return this._nodeMap[name];
}

/**
 * @param {(Object.<string, !fz.node.Config>|undefined)} nodeMap
 */
fz.node.NodeProvider.prototype.set = function(nodeMap)
{
	if (nodeMap)
	{
		for (var i = 0, name; name = Object.keys(nodeMap)[i]; ++i)
			nodeMap[name].name = name;

		this._nodeMap = nodeMap;
		this._dirty = true;
	}
}

/**
 * @type {!angular.Module}
 */
fz.node.module = angular.module('fz.node', [fz.Cnt.UI_ROUTER]);

fz.node.module.provider('fzNode', fz.node.NodeProvider);
