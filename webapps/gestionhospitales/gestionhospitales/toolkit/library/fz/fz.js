goog.provide('fz');

/**
 * @const
 */
fz.VOID = undefined;

/**
 * @enum {string}
 */
fz.Cnt =
{
	NONE			: 'none',
	UI_BOOTSTRAP	: 'ui.bootstrap',
	UI_ROUTER		: 'ui.router'
}

/**
 * @enum {number}
 */
fz.Key =
{
	ENTER			: 13
}

/**
 * @param {Function} child
 * @param {Function} parent
 */
fz.inherit = function(child, parent)
{
	child.prototype = Object.create(parent.prototype);

	/**
	 * @override
	 */
	child.prototype.constructor = child;
}

/**
 * @param {*} value
 * @return {boolean}
 */
fz.isVoid = function(value)
{
	return 'undefined' === typeof value || 'null' === value;
}

/**
 * @typedef {{
 *		id			: number,
 *		username	: string,
 *		name		: (?string|undefined),
 *		lastname	: (?string|undefined)
 *	}}
 */
fz.scope.User;

/**
 * @typedef {{
 *		name			: (?string|undefined),
 *		title			: (?string|undefined),
 *		description		: (?string|undefined),
 *		mainMenu		: (Array.<!fz.element.index.Item>|undefined),
 *		relatedMenu		: (Array.<!fz.element.index.Item>|undefined),
 *		showBreadcrumb	: boolean,
 *		showMainMenu	: boolean,
 *		showRelatedMenu	: boolean,
 *		showTitle		: boolean,
 *		error			: Array.<string>,
 *		warning			: Array.<string>
 * }}
 */
fz.scope.Box;

/**
 * @typedef {{
 *		name			: (?string|undefined),
 *		title			: (?string|undefined)
 * }}
 */
fz.scope.SystemInfo;
